getLocationFromBrowser = () => {

	return new Promise((resolve, reject) => {

		try{

			if(navigator.geolocation){

				navigator.geolocation.getCurrentPosition((position) => {

					let {latitude, longitude} = position.coords;

					resolve({latitude : latitude, longitude : longitude});

				}, (error) => {

					reject({latitude : 0, longitude : 0});
				})
			}

		}catch{

			reject({latitude : 0, longitude : 0, 'source' : 'address'});
		}
		
	})
}