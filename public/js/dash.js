$('#example').DataTable({
	// dom: 'Bfrtip',
	buttons: [
	'copy', 'csv', 'excel', 'pdf', 'print'
	]
});

$('#example1').DataTable({
	// dom: 'Bfrtip',
	buttons: [
	'copy', 'csv', 'excel', 'pdf', 'print'
	]
});

$('.menu ul ul').hide();

var myDropzone = new Dropzone("#dropzone", { url: "/file/post"});

$(function() {
	$('.business-gallery').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 3,
		arrows:true,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
				infinite: true,
				dots: true
			}
		},
		{
			breakpoint: 991,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2
			}
		},
		{
			breakpoint: 640,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});
});

// Scroll Top
var showToTop = 200;
$(window).scroll(function() {
	var scroll = getCurrentScroller();
	if ( scroll >= showToTop ) {
		$('.top').show().addClass('animated slideInUp');

	}
	else {
		$('.top').hide(500);
	}
});
function getCurrentScroller() {
	return window.pageYOffset || document.documentElement.scrollTop;
}

//Date Time
$(function() {

	$('.opening-hourss li').each(function() {
		_this = $(this);
		_this.find('#dayoff').click(function(event) {
			_this.find('.form-control').val('Closed');
		});
	});


	$('#datetimepicker1').datetimepicker({
		format: 'LT'
	});

	$('#magicsuggest').magicSuggest({
		allowFreeEntries: false,
		data: ['Business', 'Freelancing', 'Consulting'],
		expandOnFocus: true
	});
});
		//Country
		populateCountries("country", "state");


// https://github.com/wimagguc/jquery-latitude-longitude-picker-gmaps
// https://www.jqueryscript.net/demo/Location-Picker-Place-Autocomplete-Plugin-For-Google-Maps-Location-Picker/

		$(document).ready(function() {
    // Copy the init code from "jquery-gmaps-latlon-picker.js" and extend it here
    $(".gllpLatlonPicker").each(function() {
    	$obj = $(document).gMapsLatLonPicker();

    	$obj.params.strings.markerText = "Drag this Marker (example edit)";

    	$obj.params.displayError = function(message) {
        console.log("MAPS ERROR: " + message); // instead of alert()
    };

    $obj.init( $(this) );
});
});
