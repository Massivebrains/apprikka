// JS Functions

$(function() {

    // Homepage Newsletter Modal
    // $('#newsletter').modal('show');

    // Homepage Business Carousel
    $('#businesses').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        arrows:true,
        responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 640,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });


// Homepage Deals Carousel

$('#deals').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:true,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 640,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});


// Homepage Events Carousel
$('#events').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:true,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 640,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});


// Homepage Articles Carousel
$('.articles-carousel').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:true,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 640,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});


// Homepage Reviews Carousel
$('.reviews-carouse').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:true,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 640,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});

// Recent Items Carousel
$('#recent').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:true,
    dots:false,
    autoplay: true,
    autoplaySpeed: 3000,
    responsive: [
    {
        breakpoint: 1199,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 992,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 640,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});

// Gallery Carousel
$('.business-gallery').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows:true,
    responsive: [
    {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
        }
    },
    {
        breakpoint: 991,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    },
    {
        breakpoint: 640,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
    ]
});

$('#features .business-gallery .slick-next').html('→');
$('#features .business-gallery .slick-prev').html('←');


// Business Gallery Magnific Popup
$('.business-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery:{
        enabled:true
    }
});

// Tooltips
$('.tips').tipsy();

});

// Dashboard Dropdown Menus
$(".has-dropdown a.active").closest('ul').css('display', 'block');
$('.menu ul ul').hide();

// Dashboard Mobile Menu Toggle
$('.toggle-menu button').click(function(event) {
    $('.navigation .menu').slideToggle();
});

// Scroll Top
var showToTop = 200;
$(window).scroll(function() {
    var scroll = getCurrentScroller();
    if ( scroll >= showToTop ) {
        $('.top').show().addClass('animated slideInUp');

    }
    else {
        $('.top').hide(500);
    }
});
function getCurrentScroller() {
    return window.pageYOffset || document.documentElement.scrollTop;
}

// Deals Main Slider
$('.deal-slider').slick();

// Business Gallery Magnific Popup
$('.deal-slider').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery:{
        enabled:true
    }
});

// Dashboard Sidebar Widget Menu

$('.widget-menu').hide();
$('.widget-menu.visible-default').show();
$('.main-sidebar .sidebar-widgets').each(function() {
    var _this = $(this);
    _this.find('.widget-title').click(function(event) {
        _this.find('.widget-menu').slideToggle();
    });
}); 


$('.share-spot').click(function(event) {

    setTimeout(function() {
        $('.share-spot').find('.share-this').show().addClass('animated fadeInDown');
    }, 100);

    setTimeout(function() {

        $('.share-spot').find('.spots-meta').css('display', 'none');

    }, 100);
});

$('.share-spot').find('.close-share').click(function(event) {
    setTimeout(function() {
        $('.share-spot').find('.spots-meta').show().addClass('animated fadeInUp');
    }, 100);
    setTimeout(function() {
        $('.share-spot').find('.share-this').css('display', 'none');
    }, 100);
}); 

// Share on Homepage
$('.spotlight-carousel .carousel-item').each(function() {
    var _this = $(this);
    _this.find('.share-spot').click(function(event) {
        setTimeout(function() {
            _this.find('.share-this').show().addClass('animated fadeInDown');
        }, 100);
        setTimeout(function() {
            _this.find('.spots-meta').css('display', 'none');
        }, 100);
    });

    _this.find('.close-share').click(function(event) {
        setTimeout(function() {
            _this.find('.spots-meta').show().addClass('animated fadeInUp');
        }, 100);
        setTimeout(function() {
            _this.find('.share-this').css('display', 'none');
        }, 100);
    }); 
});

// Save on Homepage
$('.spotlight-carousel .carousel-item').each(function() {
    var _this = $(this);
    $('.unsave-spot, .saved-notify, .unsaved-notify').hide();
    _this.find('.save-spot').click(function(event) {
        _this.find('.save-spot').hide();
        _this.find('.unsave-spot').show();
        _this.find('.saved-notify').removeClass('slideOutDown').show();
        setTimeout(function() {
            _this.find('.saved-notify').addClass('animated slideOutDown');
        }, 1800);
        setTimeout(function() {
            _this.find('.saved-notify').hide();
        }, 2200);
    });

    _this.find('.unsave-spot').click(function(event) {
        _this.find('.save-spot').show();
        _this.find('.unsave-spot').hide();
        _this.find('.unsaved-notify').removeClass('slideOutDown').show();
        setTimeout(function() {
            _this.find('.unsaved-notify').addClass('animated slideOutDown');
        }, 1800);
        setTimeout(function() {
            _this.find('.unsaved-notify').hide();
        }, 2200);
    });
});

// Save on Categories Page
$('.general-spotlight .carousel-item').each(function() {
    var _this = $(this);
    $('.unsave-spot, .saved-notify, .unsaved-notify').hide();
    _this.find('.save-spot').click(function(event) {
        _this.find('.save-spot').hide();
        _this.find('.unsave-spot').show();
        _this.find('.saved-notify').removeClass('slideOutDown').show();
        setTimeout(function() {
            _this.find('.saved-notify').addClass('animated slideOutDown');
        }, 1800);
        setTimeout(function() {
            _this.find('.saved-notify').hide();
        }, 2200);
    });

    _this.find('.unsave-spot').click(function(event) {
        _this.find('.save-spot').show();
        _this.find('.unsave-spot').hide();
        _this.find('.unsaved-notify').removeClass('slideOutDown').show();
        setTimeout(function() {
            _this.find('.unsaved-notify').addClass('animated slideOutDown');
        }, 1800);
        setTimeout(function() {
            _this.find('.unsaved-notify').hide();
        }, 2200);
    });
});


// Smooth Scroll Anchor
$('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
            && 
            location.hostname == this.hostname
            ) {
          // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
            } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
            };
        });
        }
    }
});


// Write a review
$('.btn-review').click(function(event) {
    $('.comment-box').slideToggle();
});


// Filter results

$(function() {
    $('.filter-services').hide();
});

$('.filter-results p').click(function(event) {
    $('.filter-services').slideToggle();
});


// Sticky Menu
$(function(){

    var shrinkTopHeader = 1000;
    $(window).scroll(function() {
        var scroll = getCurrentScroller();
        if ( scroll >= shrinkTopHeader ) {
            $('.menu-sticky').addClass('fixed');

        }
        else {
            $('.menu-sticky').removeClass('fixed');
        }
    });
    function getCurrentScroller() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
});




// Local Storage Image
// https://stackoverflow.com/questions/29933027/refreshing-page-with-a-new-image
window.onload = function () {
    var images = [
    'images/fashion.jpg',
    'images/evt.jpg',
    'images/lagos.png'
    ];

    localStorage['backgroundIndex'] = !localStorage['backgroundIndex']?0:+localStorage['backgroundIndex']+2>images.length?0:+localStorage['backgroundIndex']+1;

    let heroSection = document.getElementById('hero-section');

    if(heroSection != null){

        heroSection.style.backgroundImage =  'url(' + BASE_PATH + images[+localStorage['backgroundIndex']] + ')';
    }
}

$(function(){
    var showToTop = 200;
    $(window).scroll(function() {
        var scroll = getCurrentScroller();
        if ( scroll >= showToTop ) {
            $('.top').show().addClass('animated slideInUp');

        }
        else {
            $('.top').hide(500);
        }
    });
    function getCurrentScroller() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
});


$('.sharing-icons').hide();

$('.share-business').click(e => {

    $('.head-tool').hide();
    $('.sharing-icons').show().addClass('animated fadeInUp');
});

$('.close-share-tools').click(e => {

    $('.head-tool').show();
    $('.sharing-icons').hide();
});

$(".menu .arrow").click(function () {

    $(".has-dropdown ul").slideUp(400);

    if (!$(this).next().is(":visible")) {

        $(this).next().slideDown(400)
    }
});



$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });

    return o;
}


$('.countdown').each(function(index, value){

    let date = $(this).data('date');

    $(this).countdown(date, function(event){

        $(this).text(event.strftime('%D Days %H Hours : %M Min :%S'));
    })
});

