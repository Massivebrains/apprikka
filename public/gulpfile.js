let gulp 		= require('gulp');
let concat 		= require('gulp-concat');
let minify 		= require('gulp-minify');
let cleanCSS 	= require('gulp-clean-css');

gulp.task('default', function(){

	/* CSS */
	
	gulp.src([

		'./css/normalize.css',
		'./bootstrap/css/bootstrap.min.css',
		'./css/slick.css',
		'./css/slick-theme.css',
		'./css/offcanvas.css',
		'./css/style.css',
		'./css/datatable-bootstrap.min.css',
		'./css/merchant-auth.css'
	])
	.pipe(concat('auth-merchant.css'))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(gulp.dest('./dist/css'));

	gulp.src([

		'./css/normalize.css',
		'./bootstrap/css/bootstrap.min.css',
		'./css/offcanvas.css',
		'./css/style.css'
	])
	.pipe(concat('auth.css'))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(gulp.dest('./dist/css'));

	gulp.src([

		'./css/animate.css',
		'./bootstrap/css/bootstrap.min.css',
		'./css/slick.css',
		'./css/datatable-bootstrap.min.css',
		'./css/star-rating.css',
		'./css/style.css',
		'./js/select2/css/select2.min.css'
	])
	.pipe(concat('frontend.css'))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(gulp.dest('./dist/css'));

	gulp.src([

		'./bootstrap/css/bootstrap.min.css',
		'./css/slick.css',
		'./css/animate.css',
		'./css/slick-theme.css',
		'./css/offcanvas.css',
		'./css/style.css'
	])
	.pipe(concat('home.css'))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(gulp.dest('./dist/css'));

	gulp.src([

		'./css/normalize.css',
		'./bootstrap/css/bootstrap.min.css',
		'./css/offcanvas.css',
		'./css/style.css',
		'./css/animate.css',
		'./css/slick.css',
		'./css/datatable-bootstrap.min.css',
		'./js/toastr/toastr.css',
		'./js/select2/css/select2.min.css',
		'./css/select2-bootstrap.min.css',
		'./css/magicsuggest.css',
		'./css/datepicker.css',
		'./js/datetimepicker/css/bootstrap-datetimepicker.min.css',
		'./js/timepicker/jquery.timepicker.min.css',
		'./css/dropzone.css',
		'./dropify/css/dropify.min.css',
		'./js/tooltip/css/tooltipster.bundle.min.css',
		'./js/tooltip/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css',
		'./js/trumbowyg/ui/trumbowyg.css',
		'./js/sweetalert/sweetalert2.min.css'
	])
	.pipe(concat('merchant.css'))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(gulp.dest('./dist/css'));

	/* Javascript */

	gulp.src([

		'./bootstrap/js/bootstrap.min.js',
		'./js/slick.js',
		'./js/toastr.js',
		'./js/tipsy.js',
		'./js/datatable.min.js',
		'./js/datatable-bootstrap.min.js',
		'./js/toastr/toastr.min.js',
		'./js/select2/js/select2.min.js',
		'./js/magnific.js',
		'./js/magicsuggest.js',
		'./js/timepicker/jquery.timepicker.min.js',
		'./js/moment.js',
		'./js/datetimepicker/js/bootstrap-datetimepicker.min.js',
		'./js/datepicker.js',
		'./js/dropzone.js',
		'./dropify/js/dropify.min.js',
		'./js/tooltip/js/tooltipster.bundle.min.js',
		'./js/jinplace-1.2.1.min.js',
		'./js/trumbowyg/trumbowyg.min.js',
		'./js/sweetalert/sweetalert2.all.min.js',
		'./js/main.js'
	])
	.pipe(concat('merchant.js'))
	.pipe(minify())
	.pipe(gulp.dest('./dist/js'));

	gulp.src([

		'./bootstrap/js/bootstrap.min.js',
		'./js/slick.js',
		'./js/countdown.js',
		'./js/star-rating.js',
		'./js/magnific.js',
		'./js/toastr.js',
		'./js/tipsy.js',
		'./js/datatable.min.js',
		'./js/datatable-bootstrap.min.js',
		'./js/select2/js/select2.min.js',
		'./js/bootstrap3-typeahead.min.js',
		'./js/main.js'
	])
	.pipe(concat('frontend.js'))
	.pipe(minify())
	.pipe(gulp.dest('./dist/js'));

	gulp.src([

		'./js/jquery.min.js',
		'./bootstrap/js/bootstrap.min.js',
		'./js/slick.js',
		'./js/rotate-text.js',
		'./js/countdown.js',
		'./js/offcanvas.js',
		'./js/magnific.js',
		'./js/toastr.js',
		'./js/tipsy.js',
		'./js/main.js'
	])
	.pipe(concat('home.js'))
	.pipe(minify())
	.pipe(gulp.dest('./dist/js'));
});