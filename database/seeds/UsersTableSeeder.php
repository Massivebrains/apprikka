<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();

		for($i = 0; $i<= 100; $i++){

			User::create([

				'first_name'		=> $faker->name,
				'last_name'			=> $faker->name,
				'email'				=> $faker->unique()->email,
				'phone'				=> $faker->phoneNumber,
				'type'				=> 'user',
				'status'			=> 'inactive',
				'password'			=> bcrypt('password'),
				'category_id'		=> rand(1, 10),
				'subcategory_id'	=> rand(1, 10),
				'country_id'		=> rand(1, 100),
				'state_id'			=> rand(1, 1000),
				'city_id'			=> rand(1, 1000),
				'address_1'			=> $faker->address,
				'address_2'			=> $faker->address,
				'website'			=> $faker->url,
				'info'				=> $faker->text

			]);
		}
	}
}
