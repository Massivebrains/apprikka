<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionsTable extends Migration
{
    public function up()
    {
        Schema::create('options', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('product_id')->nullable();
            $table->string('name')->nullable();
            $table->float('price')->default(0);
            $table->float('quantity')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::dropIfExists('options');
    }
}
