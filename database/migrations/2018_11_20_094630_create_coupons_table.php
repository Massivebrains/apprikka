<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('merchant_id')->nullable()->default(0)->comment('user_id of the merchant who owns this coupon. Zero for none.');
            $table->integer('user_id')->nullable()->default(0)->comment('user_id of customer allowed to use this coupon. Zero for any');
            $table->integer('order_id')->nullable()->default(0);
            $table->text('name')->nullable();
            $table->string('code');
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->enum('type', ['percent', 'flat'])->nullable();
            $table->float('value')->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->boolean('used')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }


    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
