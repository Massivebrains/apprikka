<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{

    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('order_id');
            $table->integer('product_id')->nullable();
            $table->integer('merchant_id')->nullable();
            $table->integer('option_id')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('option_name')->nullable();
            $table->enum('status', ['cart', 'pending', 'completed']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
