<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->text('slug')->nullable();
            $table->enum('type', ['customer', 'merchant', 'admin'])->default('user');
            $table->enum('status', ['inactive', 'active', 'disabled'])->default('inactive');
            $table->string('password');
            $table->integer('place_id')->default(0);
            $table->integer('country_id')->default(0);
            $table->integer('state_id')->default(0);
            $table->integer('city_id')->default(0);
            $table->text('address')->nullable();
            $table->string('website')->nullable();
            $table->text('info')->nullable();
            $table->string('image')->nullable();
            $table->string('office_address')->nullable();
            $table->string('office_email')->nullable();
            $table->string('office_phone')->nullable();
            $table->integer('step')->nullable()->default(0);
            $table->string('document')->nullable();
            $table->string('bank')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_name')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
