<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{

    public function up()
    {
        Schema::create('places', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('google_place_id')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->text('viewport')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('places');
    }
}
