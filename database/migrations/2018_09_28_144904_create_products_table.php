<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->enum('type', ['business', 'deal', 'event', 'group'])->nullable();
            $table->string('name');
            $table->string('slug');
            $table->integer('category_id')->default(0)->nullable();
            $table->integer('subcategory_id')->default(0)->nullable();
            $table->integer('country_id')->default(156);
            $table->integer('state_id')->default(0);
            $table->integer('place_id')->default(0);
            $table->text('address')->nullable();
            $table->string('website')->nullable();
            $table->text('description')->nullable();
            $table->string('logo')->nullable();
            $table->string('banner')->nullable();
            $table->text('images')->nullable();
            $table->string('personnel')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->integer('featured')->default(0)->nullable();
            $table->datetime('last_viewed_at')->nullable();

            //Deal
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->float('regular_price')->default(0)->nullable();
            $table->float('sale_price')->default(0)->nullable();

            //Event
            $table->enum('duration', ['one', 'multiple'])->nullable()->default('one');
            $table->text('start_time')->nullable();
            $table->text('end_time')->nullable();
            $table->enum('price_type', ['free', 'paid'])->nullable()->default('free');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('business_hours', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('product_id');
            $table->string('day')->nullable();
            $table->string('opens')->nullable();
            $table->string('closes')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('business_hours');
    }
}
