<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('order_reference')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->float('subtotal')->default(0)->nullable();
            $table->float('discount')->default(0)->nullable();
            $table->float('total')->default(0)->nullable();
            $table->enum('status', ['cart', 'paid', 'completed', 'cancelled'])->default('cart')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('country_id')->nullable();
            $table->string('state_id')->nullable();
            $table->string('ip_address')->nullable();
            $table->string('device')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
