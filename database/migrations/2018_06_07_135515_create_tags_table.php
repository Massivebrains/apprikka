<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{

    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {

            $table->increments('id');
            $table->string('tag')->nullable();
            $table->timestamps();
        });

        Schema::create('user_tags', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id');
            $table->integer('tag_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
