<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayoutsTable extends Migration
{

    public function up()
    {
        Schema::create('payouts', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('reference')->nullable();
            $table->enum('status', ['pending', 'paid', 'cancelled'])->default('pending')->nullable();
            $table->datetime('payment_date')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('payouts');
    }
}
