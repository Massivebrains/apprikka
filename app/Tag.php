<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];
}
