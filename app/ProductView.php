<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductView extends Model
{
    protected $table 	= 'views';
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];
    
    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
