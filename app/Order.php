<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Order extends Model
{
    use SoftDeletes;

    protected $hidden   = ['updated_at', 'deleted_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function option()
    {
        return $this->belongsTo('App\EventOption');
    }

    public function transaction()
    {
    	return $this->belongsTo('App\Transaction')->withDefault(function(){

            return new \App\Transaction;
        });
    }

    public function order_details()
    {
        return $this->hasMany('App\OrderDetail');
    }

    public function coupon()
    {
        return $this->hasOne('App\Coupon');
    }

    public static function getMerchantOrders($merchant_id = 0)
    {
        return DB::table('order_details')
        ->join('orders', 'order_details.order_id', '=', 'orders.id')
        ->where('orders.deleted_at', null)
        ->where('orders.status', '!=', 'cart')
        ->where('order_details.merchant_id', $merchant_id)
        ->get();
    }

    public static function getTotal(Order $order)
    {
        if(_isAdmin())
            return $order->total;

        $sum = 0;

        foreach($order->order_details as $row){

            $sum+= $row->price;
        }

        return $sum;
    }
}
