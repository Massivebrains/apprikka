<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    protected $hidden   = ['updated_at', 'deleted_at'];
    protected $guarded  = ['updated_at'];

    public function merchant()
    {
    	return $this->belongsTo('App\User', 'merchant_id')->withDefault(function(){

    		return new \App\User();
    	});
    }

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

    		return new \App\User();
    	});
    }

    public function product()
    {
    	return $this->belongsTo('App\Product')->withDefault(function(){

    		return new \App\Product();
    	});
    }
}
