<?php

namespace App\Listeners;

use App\Events\ProductViewed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Product;
use App\ProductView;
use Auth;
use DB;

class ProductViewedListener
{

    public function __construct()
    {
        //
    }

    public function handle(ProductViewed $event)
    {
        $product    = $event->product;
        $ip         = request()->ip();
        $device     = request()->header('User-Agent');
        $user_id    = Auth::check() ? Auth::user()->id : 0;

        if($user_id != 0){

            $productView = ProductView::where(['product_id'=> $product->id,'user_id' => $user_id])->first();

        }else{

            $productView = ProductView::where(['product_id' => $product->id, 'ip_address' => $ip ])->first();
        }

        if($productView){

            $productView->update([

                'views'         => DB::raw('views+1'),
                'ip_address'    => $ip,
                'device'        => $device
            ]);

        }else{

            $productView = ProductView::create([

                'product_id'    => $product->id,
                'user_id'       => $user_id,
                'views'         => 1,
                'ip_address'    => $ip,
                'device'        => $device
            ]);
        }

        $product->update(['last_viewed_at' => $productView->updated_at]);

        return true;
        
    }
}
