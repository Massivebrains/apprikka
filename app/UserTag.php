<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTag extends Model
{
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['updated_at'];
}
