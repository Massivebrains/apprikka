<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $hidden   = ['updated_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function scopeVisible($query)
    {
        return $query->where(['status' => 'visible']);
    }

    public function scopeHidden($query)
    {
        return $query->where(['status' => 'hidden']);
    }

    public static function userReviews($user_id)
    {
        return self::whereHas('product', function($query) use($user_id){

            return $query->where(['user_id' => $user_id]);

        })->get();
    }
}
