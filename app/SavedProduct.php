<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedProduct extends Model
{
    protected $hidden   = ['updated_at', 'deleted_at'];
    protected $guarded  = ['updated_at'];

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }
}
