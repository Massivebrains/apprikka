<?php

namespace App\Http\Controllers;

require_once(base_path('app/Paystack/Paystack.php'));

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\Transaction;

class TransactionController extends Controller
{
    public function requery($transaction_reference = '', $order_reference = '')
    {
        $order = Order::where(['order_reference' => $order_reference])->first();

        if(!$order){

            return response()->json(['status' => 0]);
        }

        $private_key = env('PAYSTACK_STATUS') == 'test' ? env('PAYSTACK_TEST_SECRET') : env('PAYSTACK_LIVE_SECRET');

        $paystack = new \Paystack($private_key);

        try{

            $paystackTransaction = $paystack->transaction->verify(['reference' => $transaction_reference]);

            if((bool)$paystackTransaction->status == true){

                $transaction = Transaction::create([

                    'user_id'               => $order->user_id,
                    'order_reference'       => $order->order_reference,
                    'amount'                => $order->total,
                    'transaction_date'      => date('Y-m-d H:i:s'),
                    'status'                => $paystackTransaction->data->status,
                    'reference'             => $paystackTransaction->data->reference
                ]);

                Order::where(['id' => $order->id])->update([

                    'transaction_id' => $transaction->id,
                    'status'         => 'paid',

                ]);

                OrderDetail::where(['order_id' => $order->id])->update(['status' => 'paid']);

                return response()->json([

                    'status'                => 1,
                    'order_reference'       => $order_reference,
                    'transaction_reference' => $transaction->reference

                ]);

            }

            return response()->json(['status' => 0]);

        }catch(Exception $e){

            return response()->json(['status' => 0]);
        }
        
    }

    public function successful($order_reference = '')
    {      
        $order = Order::where(['order_reference' => $order_reference])->first();

        if(!$order)
            return redirect('/');

        if($order->total == 0){

            Order::where(['id' => $order->id])->update(['status' => 'paid']);
            OrderDetail::where(['order_id' => $order->id])->update(['status' => 'paid']);
        }

        $body       = view('emails.payment-succesful')->with('order', $order)->render();
        $subject    = 'Payment Completed';

        _sendEmail($order->user->email, $subject, $body);

        $body      = view('emails.order-succesful')->with('order', $order)->render();
        $subject    = "Your Apprikaa Order {$order->order_reference} has been confirmed";

        _sendEmail($order->user->email, $subject, $body);

        session()->flash('message', 'Your payment was succesful!');

        return redirect('user');

    }
}
