<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\SavedProduct;
use App\User;
use App\Category;

class ProductsController extends Controller
{
    public function save($id = 0)
    {
        if(!Auth::check())
            return response()->json(['status' => false, 'data' => 'Please login or create an account to save items to your account.']);
        
        $product   = Product::find($id);

        if(!$product)
            return response()->json(['status' => false, 'data' => 'We could not save this item for you.']);

        $data   = ['product_id' => $product->id, 'user_id' => Auth::user()->id];
        $status = 'unsaved';

        if(SavedProduct::where($data)->count() > 0){

            SavedProduct::where($data)->delete();

        }else{

            SavedProduct::create($data);
            $status = 'saved';
        }    

        $type = ucfirst($product->type);

        return response()->json([

            'status'    => true, 
            'data'      => [

                'message' => "$type has been $status for you.",
                'saved'   => $status == 'saved' ? 1 : 0
            ]
        ]);
    }

    public function merchant($slug = '')
    {
        $user = User::where(['slug' => $slug])->first();

        if(!$user || !request('type') || !in_array(request('type'), ['deals', 'events'])) return abort('404');

        $type = request('type') == 'deals' ? 'deal' : 'event';

        $data['categories']         = Category::where(['type' => $type])->orderBy('name', 'asc')->get();
        $data['user']               = $user;
        $data['type']               = request('type');
        $data['products']           = Product::active()->where(['type' => $type])->by($user->id)->orderBy('featured', 'desc')->paginate(20);
        $data['recently_viewed']    = Product::active()->where(['type' => $type])->by($user->id)->recentlyViewed()->orderBy('id', 'random')->take(10)->get();

        return view('products.merchant', $data);
    }
}
