<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\SavedProduct;
use App\User;
use App\Review;
use Auth;
use App\Events\ProductViewed;

class GroupsController extends Controller
{
    public function index($category_slug = '')
    {
        $where = [];

        if($category_slug != ''){

            $category = Category::groups()->where(['slug' => $category_slug])->first();

            if($category) $where = ['category_id' => $category->id];
        }

        $products = Product::groups()->active();

        switch(request('sort')){

            case 'recently-viewed'  : $products->recentlyViewed(); break;
            case 'newest'           : $products->orderBy('created_at', 'desc'); break;
            case 'oldest'           : $products->orderBy('created_at', 'asc'); break;
            default                 : $products->where($where)->orderBy('featured', 'desc'); break;
        }

        $data['categories']         = Category::groups()->orderBy('name', 'asc')->get();
        $data['products']           = $products->paginate(20);
        $data['recently_viewed']    = Product::groups()->active()->recentlyViewed()->orderBy('id', 'random')->take(10)->get();
                
        return view('groups.index', $data);
    }

    public function group(Request $request, $slug = '')
    {
        $product = Product::where(['slug' => $slug])->with(['category', 'user'])->first();

        if(!$product)
            return redirect('groups');

        event(new ProductViewed($product));
        
        $data['product']            = $product;
        $data['recently_viewed']    = Product::groups()->active()->recentlyViewed()->orderBy('id', 'random')->take(10)->get();
        $data['reviews']            = Review::visible()->where(['product_id' => $product->id])->where('review', '>', 0)->get();
        
        return view('groups.group.index', $data);
    }
}
