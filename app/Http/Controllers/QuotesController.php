<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Product;
use App\Quote;

class QuotesController extends Controller
{
    public function requestQuote(Request $request)
	{
		// if(!Auth::check())
		// 	return response()->json(['status' => false, 'data' => 'auth']);

		$user_id = Auth::check() ? Auth::user()->id : 0;

		$quote = Quote::where([

            'email'         => request('email'),
            'phone'         => request('phone'), 
			'product_id' 	=> request('product_id')

		])->first();

		if($quote)
			return response()->json(['status' => false, 'data' => 'You have requested a quote for this business and your request is currently being attended to.']);

		$product = Product::find(request('product_id'));

		if(!$product)
			return response()->json(['status' => false, 'data' => 'We could not initiate this request. Please contact support']);

		$quote = Quote::create([

			'user_id' 			=> $user_id,
			'product_id'		=> request('product_id'),
			'name'				=> request('name'),
			'email'				=> request('email'),
			'phone'				=> request('phone'),
			'title'				=> request('title'),
			'description'		=> request('description')
		]);

		$body       = "<h2>Hello {$product->user->first_name},</h2>";
		$body       .= "<p>Someone has recently requested for your quote on <strong>{$product->name}</strong>. Login to your account on Apprikaa for more details.";

		$email      = view('layouts.emails')->with('body', $body)->render();

		_sendEmail($product->user->email, 'You have a new Quote Request!', $email);

		return response()->json(['status' => true, 'data' => 'Thank you for your quote request. You would be contacted within 24 hours.']);
	}
}
