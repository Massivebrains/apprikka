<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CronController extends Controller
{
    public function expiredDeals()
    {
    	$deals = Product::deals()->active()->where('end_date', '<', date('Y-m-d H:i:s'))->get();

    	foreach($deals as $row){

    		Product::where(['id' => $row->id])->update(['status' => 'inactive']);
    	}
    }
}
