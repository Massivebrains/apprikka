<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\BusinessHour;
use App\SavedProduct;
use App\User;
use App\Review;
use Auth;
use App\Events\ProductViewed;

class BusinessesController extends Controller
{
    public function index($category_slug = '')
    {
        $where = [];

        if($category_slug != ''){

            $category = Category::businesses()->where(['slug' => $category_slug])->first();

            if($category) $where = ['category_id' => $category->id];
        }

        $products = Product::businesses()->active();

        switch(request('sort')){

            case 'recently-viewed'  : $products->recentlyViewed(); break;
            case 'newest'           : $products->orderBy('created_at', 'desc'); break;
            case 'oldest'           : $products->orderBy('created_at', 'asc'); break;
            default                 : $products->where($where)->orderBy('featured', 'desc'); break;
        }

        $data['categories']         = Category::businesses()->orderBy('name', 'asc')->get();
        $data['products']           = $products->paginate(20);
        $data['recently_viewed']    = Product::businesses()->active()->recentlyViewed()->orderBy('id', 'random')->take(10)->get();
                
        return view('businesses.index', $data);
    }

    public function business(Request $request, $slug = '')
    {
        $product = Product::where(['slug' => $slug])->with(['category', 'user', 'days'])->first();

        if(!$product)
            return redirect('businesses');

        event(new ProductViewed($product));
        
        $data['product']            = $product;
        $data['recently_viewed']    = Product::businesses()->active()->recentlyViewed()->orderBy('id', 'random')->take(10)->get();
        $data['reviews']            = Review::visible()->where(['product_id' => $product->id])->where('review', '>', 0)->get();
        
        return view('businesses.business.index', $data);
    }

    public function businessesAjax()
    {
        $businesses = Product::where(['type' => 'business'])->get();

        $data = [];

        foreach($businesses as $row){

            $data[] = ['id' => $row->id, 'name' => $row->name];
        }

        return response()->json($data);
    }
}
