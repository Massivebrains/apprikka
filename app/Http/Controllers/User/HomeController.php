<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\SavedProduct;
use App\Quote;
use Auth;

class HomeController extends Controller
{
    public function index()
    {   
        $data['active'] = 'dashboard';
    	$data['title']	= 'User Dashboard';

        $data['saved_businesses'] = SavedProduct::where(['user_id' => Auth::user()->id])->whereHas('product', function($query){

            return $query->where(['type' => 'business']);

        })->count();

        $data['saved_deals'] = SavedProduct::where(['user_id' => Auth::user()->id])->whereHas('product', function($query){

            return $query->where(['type' => 'deal']);

        })->count();

        $data['saved_events'] = SavedProduct::where(['user_id' => Auth::user()->id])->whereHas('product', function($query){

            return $query->where(['type' => 'event']);

        })->count();
    	
        $data['orders'] = Order::where(['user_id' => Auth::user()->id])->where('status', '!=', 'cart')->count();
        $data['quotes'] = Quote::where(['user_id' => Auth::user()->id])->count();

    	return view('user.dashboard', $data);
    }

    public function saved()
    {
    	$saved = SavedProduct::where(['user_id' => Auth::user()->id])->get();

        $data['saved']     = $saved;
        $data['active']    = 'saved';

        return view('user.saved', $data);
    }

    public function quotes()
    {
        $data['quotes']     = Quote::where(['user_id' => Auth::user()->id])->get();
        $data['active']     = 'quotes';

        return view('user.quotes', $data);
    }

    public function orders($order_reference = '')
    {
    	
    	$data['orders']	= Order::where(['user_id' => Auth::user()->id])->get();
    	$data['active']	= 'orders';

    	return view('user.orders', $data);
    }

    public function order($order_reference = '')
    {        
        $order = Order::where(['order_reference' => $order_reference])->first();

        if(!$order)
            return redirect('user/dashboard');
        
        $data['order']  = $order;
        $data['active'] = 'orders';

        return view('user.order', $data);
    }
}
