<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class ProfileController extends Controller
{
   public function index()
   {
       $data['active']	= 'profile';
       $data['user']	= Auth::user();

       return view('user.profile', $data);
   }

   public function basicInfo(Request $request)
   {
       User::where(['id' => Auth::user()->id])->update([

          'first_name'	=> request('first_name'),
          'last_name'		=> request('last_name')
      ]);

       session()->flash('message', 'Profile Updated.');

       return redirect('user/profile');
   }

   public function billing(Request $request)
   {
       User::where(['id' => Auth::user()->id])->update([

          'country_id'	=> request('country_id'),
          'state_id'		=> request('state_id'),
          'address'		=> request('address')
      ]);

       session()->flash('message', 'Profile Updated.');

       return redirect('user/profile');
   }

   public function password(Request $request)
   {
       $this->validate($request, [

          'password'	=> 'required|confirmed'
      ]);

       if(Auth::user()->password != bcrypt(request('current_password'))){

          session()->flash('error', 'The current password is invalid');

          return redirect('user/profile');
      }

      User::where(['id' => Auth::user()->id])->update([

          'password'	=> bcrypt('password')
      ]);

      session()->flash('message', 'Profile Updated.');

      return redirect('user/profile');
  }
  
}
