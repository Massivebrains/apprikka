<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth;

class ClaimController extends Controller
{
    public function index()
    {
        if(request()->isMethod('post')){

            if(request('hash') != md5(request('code'))){

                session()->flash('error', 'Invalid Verification Code. A new code has been sent.');

                return redirect('claim-verify/'.request('slug'));
            }

            $product    = Product::where(['slug' => request('slug')])->first();

            if(!$product)
                return redirect('claim-business');

            $product->user_id = Auth::user()->id;
            $product->save();

            $subject    = 'You have succesfully Claimed '.$product->name.'!';
            $email      = view('emails.claim-succesful')->with(['product' => $product])->render();

            _sendEmail(Auth::user()->email, $subject, $email);

            session()->flash('message', 'You have succesfully claimed '.$product->name.'!');

            return redirect('merchant');
        }

        $data['search']		= false;

        return view('claim.index', $data);
    }

    public function verify($slug = '')
    {
        if(!Auth::check()){

            session()->put('previous_url', request()->url());

            return redirect('auth');
        }

        if(request('name')){

          $product = Product::where(['type' => 'business', 'name' => request('name')])->first();

      }else{

          $product = Product::where(['type' => 'business', 'slug' => $slug])->first();
      }


      if(!$product)
          return redirect('claim-business');

      $code       = time();
      $subject    = 'Claim '.$product->name.' on Apprikaa with '.$code.'!';
      $email      = view('emails.claim')->with(['product' => $product, 'code' => $code])->render();

      _sendEmail($product->email, $subject, $email);

      $data['search']		= false;
      $data['product'] 	= $product;
      $data['code']       = $code;

      return view('claim.verify', $data);
  }
}
