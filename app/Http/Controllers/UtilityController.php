<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;

class UtilityController extends Controller
{
    public function states($country_id = 0)
    {
        foreach(DB::table('states')->where(['country_id' => $country_id])->get() as $row){

            echo "<option value='{$row->id}''>{$row->name}</option>";
        }
    }

    public function subCategories($category_id = 0)
    {
        foreach(Category::where(['parent_id' => $category_id])->get() as $row){

            echo "<option value='{$row->id}''>{$row->name}</option>";
        }
    }

    public function mailchimp()
    {
        $email = request('email');

        if(empty($email))
            return response()->json(['status' => false, 'data' => 'Please provide your email address']);

        try{

            $endpoint   = 'https://us19.api.mailchimp.com/3.0/lists/b2fa59296a/members/';
            $apikey     = '6454c2548544cd70fff9006472e527a4-us19';

            $auth = base64_encode('user:'.$apikey);

            $data = json_encode([

                'apikey'        => $apikey,
                'email_address' => $email,
                'status'        => 'subscribed',
                'merge_fields'  => ['FNAME' => request('name')]
            ]);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://us19.api.mailchimp.com/3.0/lists/b2fa59296a/members/');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [

                'Content-Type: application/json', 'Authorization: Basic '.$auth
            ]);

            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            
            $response = json_decode(curl_exec($ch));


            if(isset($response->id)){

                return response(['status' => true, 'data' => 'Email Subscribed Successfully.']);
            }

            return response()->json(['status' => false, 'data' => $response->title]);

        }catch(Exception $e){

            return response()->json(['status' => false, 'data' => 'Some error occured.']);
        }

    }
}
