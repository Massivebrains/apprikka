<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class AuthController extends Controller
{
    public function index()
    {
        if(Auth::user() != null){

            if(Auth::user()->type == 'admin')
                return redirect('admin');

            if(Auth::user()->type == 'merchant')
                return redirect('merchant');

            if(Auth::user()->type == 'user')
                return redirect('user');
        }

        return view('auth.login');
    }

    public function login()
    {
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')], request('remember'))){

            $user = Auth::user();

            if($user->status == 'inactive'){

                $url        = url('auth/activate/'.$user->id);
                $email      = view('emails.register-email')->with(['user' => $user, 'url' => $url])->render();
                $subject    = 'Activate your account on Apprikaa';

                _sendEmail($user->email, $subject, $email);

                $error = 'Your account is currently inactive. an email has been sent to you to confirm your account.';

                Auth::logout();

                if(request()->ajax())
                    return response()->json(['status' => false, 'data' => $error]);
                
                request()->session()->flash('error', $error);
                
                return $this->index();
            }

            if(request()->ajax())
                return response()->json(['status' => true, 'data' => Auth::user()]);

            if(session('previous_url')){

                return redirect(session()->pull('previous_url', '/'));
            }

            if(Auth::user()->type == 'admin')
                return redirect('admin');

            if(Auth::user()->type == 'merchant'){

                $step = Auth::user()->step;

                if($step != 5)
                    return redirect('merchant/verify/step/'.$step);


                return redirect('merchant');
            }

            return redirect('user');
        }

        if(request()->ajax())
            return response()->json(['status' => false, 'data' => 'Invalid username or Password']);

        request()->session()->flash('error', 'Invalid Email or Password');

        return redirect()->back();
    }

    public function logout()
    {
        $route = 'auth';

        if(Auth::user() && Auth::user()->type != 'user'){

            $route = 'auth/merchant';
        }

        Auth::logout();

        return redirect($route);
    }

    public function merchant()
    {
        $data['page']   = 'login';

        return view('auth.merchant.login', $data);
    }

    public function merchantRegister()
    {
        $data['page']   = 'register';

        return view('auth.merchant.register', $data);
    }
    public function userRegister()
    {
        return view('auth.register');
    }

    public function register(Request $request, $type = '')
    {
        if(!in_array($type, ['merchant', 'user']))
            return redirect('/');

        if(request()->isMethod('post')){

            $this->validate($request, [

                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|email|max:255|unique:users,email',
                'phone'         => 'required|digits:11',
                'password'      => 'required|confirmed'
            ]);

            $user = User::create([

                'first_name'        => request('first_name'),
                'last_name'         => request('last_name'),
                'email'             => request('email'),
                'phone'             => request('phone'),
                'slug'              => str_slug(request('first_name').'-'.request('last_name').'-'.str_random(6)),
                'type'              => $type,
                'country_id'        => request('country_id') ?? 0,
                'state_id'          => request('state_id') ?? 0,
                'password'          => bcrypt(request('password'))
            ]);

            $url        = url('auth/activate/'.$user->id);
            $email      = view('emails.register-email')->with(['user' => $user, 'url' => $url])->render();
            $subject    = 'Activate your account on Apprikaa';

            _sendEmail($user->email, $subject, $email);

            request()->session()->now('message', 'We have sent an email to '.$user->email.'. Click on the link in the email to confirm your account.');
        }

        $view = $type == 'merchant' ? 'auth.merchant.register' : 'auth.register';

        $data['page'] = 'regsiter';

        return view($view, $data);
    }

    public function activate($user_id = '')
    {
        $id = (int)$user_id;

        $user = User::where(['id' => $id, 'status' => 'inactive'])->first();

        if(!$user)
            return redirect('/');

        $user->status = 'active';
        $user->save();

        request()->session()->flash('message', 'Your account has been activated successfully. You can now login');

        return redirect('auth');
    }

    public function forgotPassword(Request $request, $token = '')
    {
        if($token){

            $user = User::where(['remember_token' => $token])->first();

            if(!$user)
                return redirect('auth');

            return view('auth.reset-password', ['user' => $user]);
        }

        if(request()->isMethod('post')){

            $this->validate($request, ['email' => 'required|exists:users,email']);

            $user = User::where(['email' => request('email')])->first();

            $token = str_random(16);

            $user->update(['remember_token' => $token]);

            $subject    = 'Reset your password.';
            $email      = view('emails.reset-password')->with(['user' => $user])->render();

            _sendEmail($user->email, $subject, $email);

            session()->flash('message', 'We have sent a reset password email to you. use the link in the email to continue.');
        }

        return view('auth.forgot-password');
    }

    public function resetPassword(Request $request, $token = '')
    {
        if(request()->isMethod('post')){

            $this->validate($request, ['password' => 'required|confirmed']);

            $user = User::where(['remember_token' => $token])->first();

            if(!$user)
                return redirect('auth');

            $user->update(['password' => bcrypt('password')]);

            Auth::loginUsingId($user->id);

            session()->flash('message', 'Password changed successfully');

            return redirect('auth');
        }

        if($token){

            $user = User::where(['remember_token' => $token])->first();

            if(!$user)
                return redirect('auth');

            return view('auth.reset-password', ['token' => $token]);
        }

        return redirect('auth');
        
    }
}
