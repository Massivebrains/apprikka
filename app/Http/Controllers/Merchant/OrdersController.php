<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Auth;

class OrdersController extends Controller
{
    
    public function index()
    {
        if(_isAdmin()){

            $orders = Order::get();

        }else{

            $orders = Order::getMerchantOrders(Auth::user()->id);
        }

        $data['orders']   = $orders;

        return view('merchant.orders.index', $data);
    }

    public function order($order_reference = 0)
    {
        $order = Order::where(['order_reference' => $order_reference])->first();

        if(!$order){

            $page = _isAdmin() ? 'admin' : 'merchant';

            return redirect($page);
        }

        $data['order']  = $order;

        return view('merchant.orders.order', $data);
    }
}
