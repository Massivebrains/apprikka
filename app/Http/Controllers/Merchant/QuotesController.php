<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Quote;
use Auth;

class QuotesController extends Controller
{
    public function index($status = 'pending')
    {
        if(_isAdmin()){

            $quotes = Quote::where(['status' => $status])->get();

        }else{

            $quotes = Quote::where(['status' => $status])->whereHas('product', function($query){

                $query->where(['user_id' => Auth::user()->id]);

            })->orderBy('id', 'asc')->get();
        }

        $data['quotes']      = $quotes;
        $data['title']       = ucfirst($status).' Quotes';

        return view('merchant.quotes.index', $data);
    }

    public function quote($id = 0)
    {
        $quote = Quote::find($id);

        if(!$quote){

            $page = _isAdmin() ? 'admin' : 'merchant';

            return redirect($page);
        }
        
        $data['quote']  = $quote;
        $data['title']  = 'Quote';
        
        return view('merchant.quotes.quote', $data);
    }

    public function status($id = 0)
    {
        $quote = Quote::find($id);

        if(!$quote)
            return redirect('merchant/quotes');

        if(Auth::user()->type != 'admin'){

            if(Auth::user()->id != $deal->user_id)
                return redirect('merchant/deals');
        }

        $quote->status = $quote->status == 'pending' ? 'completed' : 'pending';

        $quote->save();

        if(_admin() && $quote->status == 'completed'){

            $body       = view('emails.quote-status')->with('quote', $quote)->render();
            $subject    = 'Your quote has been marked as completed';

            _sendEmail($quote->email, $subject, $body);
        }

        request()->session()->flash('message', 'Quote updated succesfully');

        return redirect('merchant/quotes');
    }
}
