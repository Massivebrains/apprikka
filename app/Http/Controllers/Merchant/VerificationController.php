<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class VerificationController extends Controller
{
    public function index($step = 0)
    {
        if(!in_array($step, [0, 1, 2, 3, 4, 5]))
            return redirect('/');

        $step = $step == 0 ? 1 : $step;
        
    	return view('merchant.verification.step_'.$step);
    }

    public function terms()
    {
        $step = Auth::user()->step;

        if($step > 1)
            return redirect('merchant/verify/step/'.$step);

        User::where(['id' => Auth::user()->id])->update(['step' => 2]);

    	return redirect('merchant/verify/step/2');
    }

    public function orientation()
    {
        $user = Auth::user();

        if($user->step > 2)
            return redirect('merchant/verify/step/'.$step);

        User::where(['id' => Auth::user()->id])->update(['step' => 3]);

        return redirect('merchant/verify/step/3');
    }

    public function upload(Request $request)
    {
        $user = Auth::user();

        if($user->step > 3)
            return redirect('merchant/verify/step/'.$step);

        if(!request('document')){

            session()->flash('error', 'Valid ID is compulsory');
            return redirect('merchant/verify/step/3');
        }

        $response = _cloudinary($request->file('document'));

        $user->update(['document' => $response->link, 'step' => 4]);

        if(!request('photo')){

            $response = _cloudinary($request->file('document'));

            $user->update(['image' => $response->link]);

        }

        return redirect('merchant/verify/step/4');
    }

    public function bank()
    {
        $user = Auth::user();

        if($user->step > 4)
            return redirect('merchant/verify/step/'.$step);

        $user->update([

            'bank'              => request('bank'),
            'account_number'    => request('account_number'),
            'account_name'      => request('account_name'),
            'step'              => 5
        ]);

        return redirect('merchant/verify/step/5');
    }
}
