<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Payout;

class PayoutController extends Controller
{
    public function index()
    {
        $data['title']		= 'Payouts';
        $data['payouts']  	= Payout::where(['user_id' => Auth::user()->id])->get();

    	return view('merchant.payouts.index', $data);
    }

    public function request(Request $request)
    {
        $user       = Auth::user();
        $balance    = Payout::balance($user->id);

        if(request()->isMethod('post')){

            $this->validate($request, [

                'amount'    => 'required|numeric|lte:'.$balance
            ]);

            if(Payout::where(['user_id' => $user->id, 'status' => 'pending'])->count() > 0){

                session()->flash('error', 'You currently have a payout request yet to be attended to. You cannot create a new one.');

                return redirect('merchant/payouts');
            }

            $payout = Payout::create([

                'user_id'       => $user->id,
                'amount'        => request('amount'),
                'reference'     => _payoutReference(),
                'description'   => request('description')
            ]);

            $subject    = 'A New Payout Request was made on your account';
            $email      = view('emails.payout-request')->with(['user' => $user, 'payout' => $payout])->render();

            _sendEmail($user->email, $subject, $email);

            session()->flash('message', 'Payout request made successfully.');

            return redirect('merchant/payouts');
        }
        
        $data['title']      = 'Request Payout';
        $data['balance']    = $balance;

        return view('merchant.payouts.request', $data);
    }

    public function payout($reference = '')
    {
        $payout = Payout::where(['reference' => $reference])->first();

        if(!$payout)
            return redirect('payouts');

        $data['title']  = 'Payout - '.$payout->reference;
        $data['payout'] = $payout;

        return view('merchant.payouts.payout', $data);
    }
}
