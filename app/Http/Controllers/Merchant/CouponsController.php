<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Coupon;
use App\User;
use Auth;

class CouponsController extends Controller
{
	public function index($status = 'pending')
	{
		if(_isAdmin()){

			$coupons = Coupon::get();

		}else{

			$coupons = Coupon::where(['merchant_id' => Auth::user()])->get();
		}

		$data['coupons']     = $coupons;
		$data['title']       = 'Coupons';

		return view('merchant.coupons.index', $data);
	}

	public function coupon($id = 0)
	{
		$coupon				= Coupon::firstOrNew(['id' => $id]);
		$coupon->code 		= $coupon->code ?? _couponCode();
		$data['coupon'] 	= $coupon;
		$data['merchants']	= User::merchants()->active()->get();
		$data['users']		= User::users()->active()->get();

		$data['title'] 	= 'Coupon';

		return view('merchant.coupons.coupon', $data);
	}

	public function saveCoupon(Request $request, $id = 0)
	{
		if($id == 0){

			$this->validate($request, [

				'name'			=> 'required',
				'code'			=> 'required|unique:coupons,code',
				'start_date'	=> 'date',
				'end_date'		=> 'date',
				'type'			=> 'required',
				'value'			=> 'required|numeric',
				'status'		=> 'required'

			]);
		}

		$coupon = Coupon::updateOrCreate(['id' => $id], [

			'merchant_id'	=> (int)request('merchant_id', Auth::user()->id),
			'user_id'		=> (int)request('user_id', 0),
			'name'			=> request('name'),
			'code'			=> request('code'),
			'start_date'	=> date('Y-m-d', strtotime(request('start_date'))),
			'end_date'		=> date('Y-m-d', strtotime(request('end_date'))),
			'type'			=> request('type'),
			'value'			=> request('value'),
			'status'		=> request('status')
		]);

		request()->session()->flash('message', 'Coupon saved succesfully.');

		return redirect('merchant/coupons');
	}
}
