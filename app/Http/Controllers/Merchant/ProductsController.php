<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Category;
use App\User;
use App\Tag;
use App\ProductTag;
use App\Option;
use App\Order;
use App\Product;
use App\BusinessHour;

class ProductsController extends Controller
{
    public function index($type = '')
    {   
        if(!in_array($type, ['deal', 'business', 'event', 'group']))
            return redirect('merchant');

        $data['title']      = str_plural(strtoupper($type));

        if(_isAdmin()){

            $products = Product::where(['type' => $type])->get();

        }else{

            $products = Product::where(['type' => $type, 'user_id' => Auth::user()->id])->get();

        }
        
        $data['products']   = $products;  

        return view('merchant.products.'.$type, $data);

    }

    public function basicInfo(Request $request, $id = '', $type = '')
    {   

        $product  = Product::firstOrNew(['id' => $id]);
        $type     = $product->type == '' ? $type : $product->type;

        if(!in_array($type, ['deal', 'business', 'event', 'group']))
            return redirect('merchant');

        if(request()->isMethod('post')){

            $this->validate($request, [

                'name'              => 'required',
                'category_id'       => 'required',
                'country_id'        => 'required',
                'state_id'          => 'required',
                'address'           => '',
                'start_date'        => '',
                'end_date'          => '',
                'regular_price'     => 'numeric',
                'sale_price'        => 'numeric',
                'address'           => '',
                'website'           => ''
            ]);

            $product = Product::basicInfo($id, $type);
            
            if($product->type == 'business')
                return redirect('merchant/product/business-details/'.$product->id);

            if($product->type == 'group')
                return redirect('merchant/product/group-details/'.$product->id);

            return redirect('merchant/product/options/'.$product->id);
        }

        $productTags = ProductTag::where(['product_id' => $id])->get();
        $tags        = '';

        foreach($productTags as $row){

            $tag    = Tag::firstOrNew(['id' => $row->tag_id])->tag;
            $tags   .= " '{$tag}', ";

        }

        $view                   = "merchant.$type.basic-info";

        $data['title']          = strtoupper($type).' Form';
        $data['product']        = $product;
        $data['start_date']     = $data['product']->start_date;
        $data['end_date']       = $data['product']->end_date;
        $data['categories']     = Category::where(['type' => $type])->active()->parents()->get();
        $data['subcategories']  = Category::where(['type' => $type, 'parent_id' => $product->category_id])->active()->get();
        $data['merchants']      = User::merchants()->active()->get();
        $data['tags']           = "[{$tags}]";

        return view($view, $data);
    }

    public function businessDetails(Request $request, $id = 0)
    {
        $product = Product::find($id);

        if(!$product) redirect('merchant/businesses');

        if(request()->isMethod('post')){

            $product= Product::updateOrCreate(['id' => $product->id], [

                'website'         => request('website'),
                'description'     => request('description')
            ]);

            foreach(request('days') as $row){

                BusinessHour::where(['id' => $row['id']])->update([

                    'opens'     => $row['opens'] ?? null,
                    'closes'    => $row['closes'] ?? null,
                    'status'    => $row['dayoff']
                ]);
            }

            ProductTag::where(['product_id' => $product->id])->delete();

            if(request('tags') && is_array(request('tags'))){

                foreach(request('tags') as $row){

                    $tag = Tag::updateOrCreate(['tag' => strtolower($row)], ['tag' => $row]);
                    ProductTag::create(['product_id' => $product->id, 'tag_id' => $tag->id]);
                }
            }

            return redirect('merchant/product/gallery/'.$product->id);
            
        }

        $productTags = ProductTag::where(['product_id' => $product->id])->get();
        $tags        = '';

        foreach($productTags as $row){

            $tag    = Tag::firstOrNew(['id' => $row->tag_id])->tag;
            $tags   .= " '{$tag}', ";

        }

        $data['product']            = $product;
        $data['business_hours']     = BusinessHour::where(['product_id' => $product->id])->get();
        $data['tags']               = "[{$tags}]";

        return view('merchant.business.business-details', $data);
    }

    public function groupDetails(Request $request, $id = 0)
    {
        $product = Product::find($id);

        if(!$product) redirect('merchant/groups');

        if(request()->isMethod('post')){

            $product= Product::updateOrCreate(['id' => $product->id], [

                'website'         => request('website'),
                'description'     => request('description')
            ]);

            ProductTag::where(['product_id' => $product->id])->delete();

            if(request('tags') && is_array(request('tags'))){

                foreach(request('tags') as $row){

                    $tag = Tag::updateOrCreate(['tag' => strtolower($row)], ['tag' => $row]);
                    ProductTag::create(['product_id' => $product->id, 'tag_id' => $tag->id]);
                }
            }

            return redirect('merchant/product/gallery/'.$product->id);
            
        }

        $productTags = ProductTag::where(['product_id' => $product->id])->get();
        $tags        = '';

        foreach($productTags as $row){

            $tag    = Tag::firstOrNew(['id' => $row->tag_id])->tag;
            $tags   .= " '{$tag}', ";

        }

        $data['product']            = $product;
        $data['tags']               = "[{$tags}]";

        return view('merchant.group.group-details', $data);
    }


    public function options(Request $request, $id = 0)
    {

        $product = Product::find($id);

        if(!$product) redirect('merchant/dashboard');

        $data['product']    = $product;
        $data['options']    = Option::where(['product_id' => $product->id])->get();
        
        $view = "merchant.$product->type.options";

        return view($view, $data);
    }

    
    public function gallery(Request $request, $id = 0)
    {
        $product = Product::find($id);

        if(!$product) 
            redirect('merchant/dashboard');

        $type = $product->type == '' ? $type : $product->type;

        if(request()->isMethod('post')){

            if(request('banner')){

                $response = _cloudinary($request->file('banner'));

                if($response->link != null){

                    Product::where(['id' => $product->id])->update(['banner' => $response->link]);
                }
            }

            if(request('logo')){

                $response = _cloudinary($request->file('logo'));

                if($response->link != null){

                    Product::where(['id' => $product->id])->update(['logo' => $response->link]);
                }
            }

            return redirect('merchant/product/contact/'.$product->id);
        }

        $view              = "merchant.$type.gallery";

        $data['title']      = ucfirst($type).' Form - Gallery';
        $data['product']    = $product;

        return view($view, $data);
    }

    public function upload(Request $request, $id = 0)
    {
        $product = Product::find($id);

        if(!$product)
            return response()->json(['status' => false, 'data' => 'product not found']);

        if(!request('file'))
            return response()->json(['status' => false, 'data' => 'No image uploaded']);

        $response = _cloudinary($request->file('file'));

        if($response->link != null){

            if($product->images == null){

                $images = [$response->link];

            }else{

                $images = $product->images ?? [];
                $images = array_add($images, count($images), $response->link);
            }

            Product::where(['id' => $product->id])->update(['images' => json_encode($images)]);

            return response()->json(['status' => true, 'data' => 'Image uploaded succesfully']);
        }

        return response()->json(['status' => false, 'data' => 'No image uploaded']);
        
    }

    public function contact(Request $request, $id = 0)
    {
        $product = Product::find($id);

        if(!$product) redirect('merchant/dashboard');

        if(request()->isMethod('post')){

            Product::where(['id' => $product->id])->update([

                'personnel'    => request('personnel'),
                'email'        => request('email'),
                'phone'        => request('phone')
            ]);

            request()->session()->flash('message', 'Information saved succesfully.');

            return redirect('merchant/products/'.$product->type);

        }

        $data['title']      = ucfirst($product->type).' Form - Contact';
        $data['product']    = $product;

        return view('merchant.'.$product->type.'.contact', $data);
    }

    public function status($id = 0)
    {
        $product = Product::find($id);

        if(!$product)
            return redirect('merchant/dashboard');

        if(Auth::user()->type != 'admin'){

            if(Auth::user()->id != $product->user_id)
                return redirect('merchant/dashboard');
        }

        if($product->status == 'inactive'){

            if($product->options->sum('price') > 0)
                $product->price_type = 'paid';
        }
        
        $product->status = $product->status == 'active' ? 'inactive' : 'active';

        $product->save();

        if(_admin() && $product->status == 'active'){

            $subject     = "We just activated {$product->name}";
            $view        = 'emails.'.$product->type.'-status';
            $email       = view($view)->with(['product' => $product])->render();

            _sendEmail($product->email, $subject, $email);
        }

        request()->session()->flash('message', 'Information updated succesfully');

        return redirect('merchant/products/'.$product->type);
    }

    public function featured($id = 0)
    {
        $product = Product::find($id);

        if(!$product)
            return redirect('merchant/dashboard');

        if(Auth::user()->type != 'admin'){

            if(Auth::user()->id != $product->user_id)
                return redirect('merchant/dashboard');
        }


        $product->update(['featured' => $product->featured == 1 ? 0 : 1]);
        
        request()->session()->flash('message', 'Information updated succesfully');

        return redirect('merchant/products/'.$product->type);
    }
}
