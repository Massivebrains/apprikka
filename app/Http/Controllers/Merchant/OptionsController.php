<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Option;
use App\Product;

class OptionsController extends Controller
{
    public function add($product_id = 0)
    {
        $product = Product::firstOrNew(['id' => $product_id]);

        if($product){

            $name = $product->type == 'event' ? 'Free Ticket' : 'Free';

            $option = Option::create(['name' => $name, 'product_id' => $product->id]);
        }

        $data['product']   = $product;
        $data['option']    = $option;

        return view('merchant.'.$product->type.'.option', $data);
    }

    public function updateName($id = 0)
    {
        $name = request('value');

        Option::where(['id' => $id])->update(['name' => $name]);

        echo $name;
    }

    public function updatePrice($id = 0)
    {
        $price = (float)str_replace(',', '', request('value'));

        Option::where(['id' => $id])->update(['price' => $price]);

        echo number_format($price);
    }

    public function updateQuantity($id = 0)
    {
        $quantity = (float)request('value');

        Option::where(['id' => $id])->update(['quantity' => $quantity]);

        echo $quantity;
    }

    public function remove()
    {
        Option::destroy(['id' => request('id')]);

        return response()->json(['status' => true]);
    }
}
