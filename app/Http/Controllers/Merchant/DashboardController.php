<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Product;
use App\Review;

class DashboardController extends Controller
{
	
	public function index()
	{
		$step = Auth::user()->step;

		if($step != 5)
			return redirect('merchant/verify/step/'.$step);

		$user_id = Auth::user()->id;

		$data['title']					= 'Dashboard';
		$data['businesses']				= Product::where(['type' => 'business', 'user_id' => $user_id])->count();
		$data['deals']					= Product::where(['type' => 'deal', 'user_id' => $user_id])->count();
		$data['events']					= Product::where(['type' => 'event', 'user_id' => $user_id])->count();
		$data['active_businesses']		= Product::where(['type' => 'business', 'status' => 'active', 'user_id' => $user_id])->count();
		$data['active_deals']			= Product::where(['type' => 'deal', 'status' => 'active', 'user_id' => $user_id])->count();
		$data['active_events']			= Product::where(['type' => 'event', 'status' => 'active', 'user_id' => $user_id])->count();
		$data['reviews']				= Review::userReviews($user_id)->count();
		$data['coupons']				= 0;
		$data['payouts']				= 0;

		return view('merchant.dashboard.index', $data);
	}

}
