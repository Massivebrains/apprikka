<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Option;
use Auth;
use App\Coupon;
use Validator;

class CartController extends Controller
{
	public function addToCart(Request $request)
	{   
		if(!Auth::check()) 
			return response()->json(['status' => false, 'data' => 'auth']);


		$validator = Validator::make($request->all(), [

			'product_id'	=> 'required|numeric|exists:products,id',
			'option_id'		=> 'required|numeric|exists:options,id'
		]);

		if($validator->fails())
			return response()->json(['status' => false, 'data' => $validator->errors()]);

		$order = Order::where([

			'user_id'			=> Auth::user()->id,
			'order_reference' 	=> session('order_reference'), 
			'status' 			=> 'cart',

		])->first();

		if(!$order){

			$order = Order::where([

				'user_id'		=> Auth::user()->id,
				'status'		=> 'cart'

			])->first();
		}
		
		if(!$order){

			$order = Order::create([

				'order_reference'   => substr((string)time()*rand(99,999), 0, 10),
				'user_id'           => Auth::user()->id,
				'ip_address'        => request()->ip(),
				'device'            => request()->header('User-Agent')
			]);
		}

		$product 		= Product::find(request('product_id'));
		$option 		= Option::where(['id' => request('option_id')])->first();

		$orderDetail	= OrderDetail::where([

			'order_id' 		=> $order->id,
			'product_id' 	=> $product->id,
			'option_id' 	=> $option->id

		])->first();

		if($orderDetail)
			return response()->json(['status' => false, 'data' => 'Item is already in your cart.']);
		
		OrderDetail::create([

			'order_id'		=> $order->id,
			'product_id'	=> $product->id,
			'merchant_id'	=> $product->user_id,
			'option_id'		=> $option->id,
			'amount'		=> $option->price,
			'option_name'	=> $option->name,
			'status'		=> 'cart'
		]);

		$total = 0;

		foreach(OrderDetail::where(['order_id' => $order->id])->get() as $row)
			$total+= $row->amount;

		$order->sub_total = $order->total = $total;

		$order->save();
		
		session()->put('order_reference', $order->order_reference);

		return response()->json(['status' => true, 'data' => 'Item has been added to your cart']);
	}

	public function cart($order_reference = '')
	{	

		if(!Auth::check())
			return redirect('auth');

		$order = Order::where([

			'order_reference'   => session('order_reference'), 
			'user_id'           => Auth::user()->id,
			'status'            => 'cart'

		])->first();

		$view = $order == null ? 'order.no-items' : 'order.cart';

		$data['order']  = $order;

		return view($view, $data);
	}

	public function count()
	{
		$order = Order::where(['order_reference' => session('order_reference'), 'status' => 'cart'])->first();

		if(!$order)
			return response()->json(['status' => false, 'data' => 0]);

		$count = OrderDetail::where(['order_id' => $order->id])->count();

		return response()->json(['status' => true, 'data' => $count]);
	}

	public function coupon(Request $request)
	{
		$coupon = Coupon::where(['code' => request('coupon')])->first();

		if($coupon && $coupon->used == 0 && $coupon->status == 'active'){

			if($coupon->start_date != ''){

				if(date('Y-m-d', strtotime($coupon->start_date)) > date('Y-m-d')){

					session()->flash('error', 'Promo period for this Coupon has not started yet.');
					return redirect('cart#coupon');
				}
			}

			if($coupon->end_date != ''){

				if(date('Y-m-d') > date('Y-m-d', strtotime($coupon->end_date))){

					session()->flash('error', 'Promo period for this Coupon ended '._d($coupon->end_date));
					return redirect('cart#coupon');
				}
			}

			if($coupon->user_id != 0 && $coupon->user_id != Auth::user()->id){

				session()->flash('error', 'This promo code is not avaliable for your account.');
				return redirect('cart#coupon');
			}

			$order = Order::where(['order_reference' => request('order_reference')])->first();

			if(!$order || $order->status != 'cart')
				return redirect('cart');

			$discount = 0;

			if($coupon->type == 'percent')
				$discount = $order->sub_total * $coupon->value * 0.01;

			if($coupon->type == 'flat')
				$discount = $coupon->value;

			$order->update(['discount' => -$discount, 'total' => $order->sub_total - $discount]);
			$coupon->update(['order_id' => $order->id, 'used' => 1, 'status' => 'inactive']);

			session()->flash('message', 'A discount of '.number_format($discount).' has been applied to your order.');
			return redirect('cart#coupon');
		}

		session()->flash('error', 'Invalid Coupon Code');
		return redirect('cart#coupon');

	}

	public function address()
	{
		$order = Order::where([

			'order_reference'   => session('order_reference'), 
			'user_id'           => Auth::user()->id,
			'status'            => 'cart'

		])->first();

		if(!$order)
			return response()->json(['status' => false, 'data' => 'Order is invalid']);

		Order::where(['order_reference' => session('order_reference')])->update([

			'first_name'    => request('first_name'),
			'last_name'     => request('last_name'),
			'email'         => request('email'),
			'phone'         => request('phone'),
			'country_id'    => request('country_id'),
			'state_id'      => request('state_id')
		]);

		$order = Order::find($order->id);

		return response()->json(['status' => true, 'data' => $order]);
	}
}
