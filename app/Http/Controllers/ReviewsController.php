<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use Auth;
use App\Product;
use App\User;
use App\Category;

class ReviewsController extends Controller
{
    public function save()
    {

    	if(Auth::check()){

    		$review = Review::where(['user_id' => Auth::user()->id, 'product_id' => request('product_id')])->first();

    		if($review){

    			return response()->json(['status' => false, 'data' => 'You have a review for this product already.']);
    		}
    	}

    	$user_id = Auth::check() ? Auth::user()->id : 0;

        $product = Product::find(request('product_id'));

    	Review::create([

    		'user_id'		=> $user_id,
    		'product_id'	=> request('product_id'),
            'merchant_id'   => request('merchant_id', optional($product)->user_id),
    		'name'			=> request('name'),
    		'email'			=> request('email'),
    		'phone'			=> request('phone'),
    		'review'		=> (int)request('review'),
    		'comment'		=> request('comment')
    	]);

    	return response()->json(['status' => true, 'data' => 'Thank you for your review.']);
    }

    public function merchant($slug = '')
    {
        $user = User::where(['slug' => $slug])->first();

        if(!$user) return abort('404');

        $data['categories']         = Category::orderBy('name', 'asc')->get();
        $data['user']               = $user;
        $data['reviews']            = $user->reviews()->visible()->get();

        return view('reviews.merchant', $data);
    }
}
