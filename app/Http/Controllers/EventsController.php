<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Events\ProductViewed;

class EventsController extends Controller
{
    public function index($category_slug = '')
    {
        $where = [];

        if($category_slug != ''){

            $category = Category::events()->where(['slug' => $category_slug])->first();

            if($category){

                $where = ['category_id' => $category->id];
            }
        }

        $products = Product::events()->active();
        $events   = Product::events()->active();

        switch(request('sort')){

            case 'recently-viewed'  : $products->recentlyViewed(); break;
            case 'newest'           : $products->orderBy('created_at', 'desc'); break;
            case 'oldest'           : $products->orderBy('created_at', 'asc'); break;
            default                 : $products->where($where)->orderBy('featured', 'desc'); break;
        }

        switch(request('price_type')){

            case 'free' : $products->free(); break;
            case 'paid' : $products->paid(); break;
        }

    	$data['categories']      = Category::events()->orderBy('name', 'asc')->get();
    	$data['products']	     = $products->paginate(20);
        $data['recently_viewed'] = $events->recentlyViewed()->orderBy('id', 'random')->take(10)->get();
    	
    	return view('events.index', $data);
    }

    public function event(Request $request, $slug = '')
    {
        $product = Product::where(['slug' => $slug])->with(['category', 'user'])->first();

        if(!$product)
            return redirect('events');

        event(new ProductViewed($product));

        $data['product']            = $product;
        $data['recently_viewed']    = Product::deals()->active()->recentlyViewed()->orderBy('id', 'random')->take(20)->get();
        
    	return view('events.event.index', $data);
    }
}
