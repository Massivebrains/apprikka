<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use App\ProductTag;
use App\Product;

class TagsController extends Controller
{
    public function index(Request $request, $tag = '')
    {
    	$tag = Tag::where(['tag' => $tag])->first();

    	if(!$tag)
    		return redirect('/');

    	$product_tags	= ProductTag::where(['tag_id' => $tag->id])->get();

    	$product_ids 	= $product_tags->pluck('product_id');

    	$data['tag']		= $tag;
    	$data['products'] 	= Product::whereIn('id', $product_ids)->take(30)->get();

    	return view('tags.index', $data);
    }
}
