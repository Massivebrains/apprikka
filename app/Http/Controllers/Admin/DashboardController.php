<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Review;
use App\Order;

class DashboardController extends Controller
{
    public function index()
    {
    	$data['businesses']				= Product::where(['type' => 'business'])->count();
    	$data['deals']					= Product::where(['type' => 'deal'])->count();
    	$data['events']					= Product::where(['type' => 'event'])->count();
    	$data['active_businesses']		= Product::where(['type' => 'business', 'status' => 'active'])->count();
    	$data['active_deals']			= Product::where(['type' => 'deal', 'status' => 'active'])->count();
    	$data['active_events']			= Product::where(['type' => 'event', 'status' => 'active'])->count();
    	$data['reviews']				= Review::count();
    	$data['orders']					= Order::count();
    	$data['coupons']				= 0;
    	$data['payouts']				= 0;

    	return view('admin.dashboard.index', $data);
    }

}
