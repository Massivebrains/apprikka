<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Tag;
use App\UserTag;

class MerchantsController extends Controller
{
    public function index()
    {
        $data['users']      = User::where(['type' => 'merchant'])->get();
        $data['title']      = 'Merchants';
        
        return view('admin.merchants.index', $data);
    }

    public function basicInfo(Request $request, $id = '')
    {
        $user = User::firstOrNew(['id' => $id]);

        if(request()->isMethod('post')){

            $this->validate($request, [

                'first_name'    => 'required|string|max:255',
                'last_name'     => 'required|string|max:255',
                'email'         => 'required|email|max:255|unique:users,email,'.$user->id,
                'phone'         => 'required|digits:11',
                'address'       => 'required',
                'country_id'    => 'required',
                'state_id'      => 'required'
            ]);


            $user = User::updateOrCreate(['id' => $user->id], [

                'first_name'        => request('first_name'),
                'last_name'         => request('last_name'),
                'email'             => request('email'),
                'phone'             => request('phone'),
                'slug'              => str_slug(request('first_name').'-'.request('last_name').'-'.str_random(6)),
                'type'              => 'merchant',
                'password'          => bcrypt(request('first_name').':'.request('last_name')),
                'place_id'          => request('place_id'),
                'country_id'        => request('country_id'),
                'state_id'          => request('state_id'),
                'address'           => request('address')
            ]);

            return redirect('admin/merchants/details/'.$user->id);
        }

        $data['title']         = 'Merchant Form';
        $data['user']      = $user;

        return view('admin.merchants.form.basic-info', $data);
    }

    public function userDetails(Request $request, $id = '')
    {

        $user = User::where(['id' => $id, 'type' => 'merchant'])->first();

        if(!$user) return redirect('admin/merchants');

        if(request()->isMethod('post')){

            User::where(['id' => $user->Id])->update([

                'website'   => request('website'),
                'info'      => request('info')
            ]);

            UserTag::where(['user_id' => $user->id])->delete();

            if(request('tags') && is_array(request('tags'))){

                foreach(request('tags') as $row){

                    $tag = Tag::updateOrCreate(['tag' => strtolower($row)], ['tag' => $row]);
                    UserTag::create(['user_id' => $user->id, 'tag_id' => $tag->id]);

                }
            }

            if(request('image')){

                $response = _cloudinary($request->file('image'));

                if($response->link != null){

                    User::where(['id' => $user->id])->update(['image' => $response->link]);

                }
            }

            return redirect('admin/merchants/contact/'.$id);
        }

        $userTags   = UserTag::where(['user_id' => $user->id])->get();
        $tags        = '';

        foreach($userTags as $row){

            $tag    = Tag::firstOrNew(['id' => $row->tag_id])->tag;
            $tags   .= " '{$tag}', ";

        }

        $data['title']      = 'Merchant Form';
        $data['merchant']   = User::firstOrNew(['id' => $id]);
        $data['tags']       = "[{$tags}]";

        return view('admin.merchants.form.user-details', $data);
    }

    public function contact(Request $request, $id = '')
    {
        $userId = $id;

        if(request()->isMethod('post')){

            User::where(['id' => $userId, 'type' => 'merchant'])->update([

                'office_address'    => request('office_address'),
                'office_email'      => request('office_email'),
                'office_phone'      => request('office_phone')
            ]);

            request()->session()->flash('message', 'Merchant information saved succesfully.');
            
            return redirect('admin/merchants');
        }

        $data['title']         = 'Merchant Form';
        $data['merchant']      = User::firstOrNew(['id' => $userId]);

        return view('admin.merchants.form.contact', $data);
    }

    public function status($id = '')
    {
        $user = User::find($id);

        if(!$user)
            return redirect('admin/merchants');
        
        $user->status = $user->status == 'active' ? 'inactive' : 'active';

        $user->save();

        request()->session()->flash('message', 'Merchant updated succesfully');

        return redirect('admin/merchants');
    }
}
