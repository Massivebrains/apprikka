<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminRequest;
use App\User;
use App\Http\Requests\AdministratorRequest;

class AdminController extends Controller
{
    
    public function index()
    {
        $data['title']      = 'System Administrators';
        $data['admins']     = User::where(['type' => 'admin'])->orderBy('first_name', 'asc')->get();

    	return view('admin.admins.index', $data);
    }

    public function form($id = '')
    {
    	$data['admin'] = User::firstOrNew(['id' => $id]);

    	return view('admin.admins.form', $data);
    }

    public function save(CreateAdminRequest $request)
    {
        User::create([

            'first_name'    => request('first_name'),
            'last_name'     => request('last_name'),
            'email'         => request('email'),
            'phone'         => request('phone'),
            'type'          => 'admin',
            'status'        => 'active',
            'password'      => bcrypt(request('password'))
        ]);

    	request()->session()->flash('message', 'Admin Created Succesfully.');

        return redirect('admin/admins');
    }
}
