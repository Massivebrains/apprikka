<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;

class ReviewsController extends Controller
{
    public function index()
    {
        $data['reviews']  = Review::get();
        $data['title']    = 'Reviews';
        
        return view('admin.reviews.index', $data);
    }

    public function review($id = 0)
    {
    	$review = Review::find($id);

    	if(!$review)
    		return redirect('admin/reviews');

    	$data['review']	= $review;

    	return view('admin.reviews.review', $data);
    }

    public function status($id = 0){

    	$review = Review::find($id);

    	if(!$review)
    		return redirect('admin/reviews');

    	$review->status = $review->status == 'visible' ? 'hidden' : 'visible';
    	$review->save();

    	session()->flash('message', 'Review has been updated succesfully.');

    	return redirect('admin/reviews');
    }
}
