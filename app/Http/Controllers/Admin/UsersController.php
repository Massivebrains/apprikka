<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UsersController extends Controller
{
    public function index()
    {
    	$data['users']  	= User::where(['type' => 'user'])->get();
        $data['title']      = 'Users';
        
        return view('admin.users.index', $data);
    }
}
