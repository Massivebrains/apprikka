<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCategoryRequest;
use App\Category;

class CategoriesController extends Controller
{

    public function index($type = '', $id = '')
    {
    	$data['category']	= Category::firstOrNew(['id' => $id]);
    	$data['categories']	= Category::where(['type' => $type, 'parent_id' => 0])->get();
    	$data['type']		= $type;
    	$data['title']		= $type.' Categories';

    	return view('admin.categories.categories', $data);
    }

    public function save(CreateCategoryRequest $request, $id = '')
    {
    	Category::updateOrCreate(['id' => $id], [

            'parent_id'     => request('parent_id') ?? 0,
            'type' 			=> request('type'),
            'name'			=> request('name'),
            'slug'			=> str_slug(request('name')),
            'description'	=> request('description'),
            'status'		=> request('status')
        ]);

        if(request('parent_id')){

            request()->session()->flash('message', 'Sub Category saved successfully');

            return redirect('admin/sub-categories/'.request('type'));
        }

        request()->session()->flash('message', 'Category saved successfully');

        return redirect('admin/categories/'.request('type'));

    }

    public function delete($id = '')
    {
        $category = Category::find($id);

        if(!$category){

            request()->session()->flash('error', 'Invalid Category');
            return redirect('admin');
        }

        $type = $category->type;

        Category::where(['id' => $category->id])->delete();

        request()->session()->flash('message', 'Category deleted successfully');

        return redirect('admin/categories/'.$type);
    }

    public function subCategories($type = '', $id = '')
    {

        $data['category']   = Category::firstOrNew(['id' => $id]);
        $data['categories'] = Category::where(['type' => $type])->where('parent_id', '!=', 0)->get();
        $data['parents']    = Category::where(['type' => $type])->where(['parent_id' => 0])->get();
        $data['type']       = $type;
        $data['title']      = $type.' Sub-categories';

        return view('admin.categories.sub-categories', $data);
    }
}
