<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payout;

class PayoutsController extends Controller
{
	public function index()
	{
		$data['title']		= 'Payouts';
		$data['payouts']  	= Payout::orderBy('id', 'desc')->get();

		return view('admin.payouts.index', $data);
	}

	public function payout($reference = '')
	{
		$payout = Payout::where(['reference' => $reference])->first();

		if(!$payout)
			return redirect('payouts');

		$data['title']  = 'Payout - '.$payout->reference;
		$data['payout'] = $payout;

		return view('admin.payouts.payout', $data);
	}

	public function complete($id = 0)
	{
		$payout = Payout::find($id);

		if(!$payout || $payout->status != 'pending')
			return redirect('admin/payouts');

		$payout->update(['status' => 'paid', 'payment_date' => date('Y-m-d H:i:s')]);

		$subject    = 'Payout Completed!';
		$email      = view('emails.payout-complete')->with(['payout' => $payout])->render();

		_sendEmail($payout->user->email, $subject, $email);

		session()->flash('message', 'Payout marked as completed succesfully.');

		return redirect('admin/payouts');
	}
}
