<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
	public function howItWorks()
	{
		return view('pages.how-it-works');
	}

	public function termsOfUse()
	{
		return view('pages.terms-of-use');
	}

	public function privacyPolicy()
	{
		return view('pages.privacy-policy');
	}

	public function faq()
	{
		return view('pages.faq');
	}

	public function contactUs()
	{
		return view('pages.contact-us');
	}
}
