<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Search;

class SearchController extends Controller
{
    public function index()
    {
        if(request()->isMethod('post')){

            $data['query']      = request('query');
            $data['results']    = Search::search(request('query'));

            return view('search.index', $data);
        }

        return view('search.index');
    }
}
