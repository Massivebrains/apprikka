<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Review;
use App\Local;
use App\State;

class HomeController extends Controller
{
    public function index()
    {    
        $data['businesses']		= Product::businesses()->active()->orderBy('id', 'random')->get();
    	$data['deals']			= Product::deals()->active()->orderBy('id', 'random')->get();
    	$data['events']			= Product::events()->active()->orderBy('id', 'random')->get();
    	$data['reviews']		= Review::visible()->orderBy('id', 'random')->take(3)->get();

    	return view('home.index', $data);
    }

    public function location()
    {
    	$latitude 	= request('latitude');
    	$longitude	= request('longitude');

    	if(!_validateCoordinates($latitude, $longitude))
    		return response()->json(['status' => false, 'data' => 'Invalid Location']);

    	session()->put('location', true);
    	session()->put('latitude', $latitude);
    	session()->put('longitude', $longitude);

    	return response()->json(['status' => true, 'data' => [

    		'latitude' 		=> $latitude,
    		'longitude'		=> $longitude
    	]]);
    }
}
