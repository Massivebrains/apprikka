<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use App\Place;
use App\State;
use App\Local;

class PlacesController extends Controller
{
    public function searchByAddress()
    {
    	try{

    		$address  	= request('term');
    		$key 		= env('GOOGLE_API_KEY');
    		$endpoint 	= "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=$address&inputtype=textquery&fields=formatted_address,name,rating,opening_hours,geometry,place_id&key=$key";
    		
    		$response = (new Client())->request('POST', $endpoint)->getBody();
    		$response = json_decode($response);

    		if(count($response->candidates) < 1){

    			return response()->json(['results' => ['id' => 1, 'text' => 'No places match your search query']]);
    		}

    		$data  	 	= $response->candidates[0];

    		$place = Place::updateOrCreate(['google_place_id' => $data->place_id], [

    			'name'				=> $data->name,
    			'address'			=> $data->formatted_address,
    			'google_place_id'	=> $data->place_id,
    			'latitude'			=> $data->geometry->location->lat,
    			'longitude'			=> $data->geometry->location->lng,
    			'viewport'			=> json_encode($data->geometry->viewport)
    		]);

    		$result = [

    			'id' 	=> $place->id,
    			'text'	=> $place->address
    		];

    		return response()->json(['results' => [$result]]);

    	}catch(Exception $e){


    	}
    }

    public function places()
    {
        $places = Place::get();

        $data = [];

        foreach($places as $row){

            $data[] = ['id' => $row->id, 'name' => $row->address];
        }

        return response()->json($data);
    }

    public function locations(Request $request)
    {
        $state = request('region');

        $state = State::where(['name' => request('region')])->first();

        if($state){

            $locals = Local::where(['state_id' => $state->id])->orderBy('name', 'asc')->get();

        }else{

            $locals = Local::where(['state_id' => 2412])->orderBy('name', 'asc')->get();
        }

        return response()->json($locals);
    }
}
