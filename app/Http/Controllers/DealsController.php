<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Review;
use App\Events\ProductViewed;

class DealsController extends Controller
{
    public function index($category_slug = '')
    {
        $where = [];

        if($category_slug != ''){

            $category = Category::deals()->where(['slug' => $category_slug])->first();

            if($category) $where = ['category_id' => $category->id];
        }

        $products = Product::deals()->active();

        switch(request('sort')){

            case 'recently-viewed'  : $products->recentlyViewed(); break;
            case 'newest'           : $products->orderBy('created_at', 'desc'); break;
            case 'oldest'           : $products->orderBy('created_at', 'asc'); break;
            default                 : $products->where($where)->orderBy('featured', 'desc'); break;
        }

        $data['categories']      = Category::deals()->orderBy('name', 'asc')->get();
        $data['products']        = $products->paginate(20);
        $data['recently_viewed'] = Product::deals()->active()->recentlyViewed()->orderBy('id', 'random')->take(20)->get();

        return view('deals.index', $data);
    }

    public function deal(Request $request, $slug = '')
    {
        $product = Product::where(['slug' => $slug])->with(['category', 'user'])->first();

        if(!$product)
            return redirect('deals');

        event(new ProductViewed($product));

        $data['product']            = $product;
        $data['reviews']            = Review::where(['product_id' => $product->id, 'status' => 'visible'])->where('review', '>', 0)->get();
        $data['recently_viewed']    = Product::deals()->active()->recentlyViewed()->orderBy('id', 'random')->take(20)->get();

        return view('deals.deal.index', $data);
    }
}
