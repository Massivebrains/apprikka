<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MerchantAuth
{
    public function handle($request, Closure $next)
    {	
        if(!in_array(Auth::user()->type, ['admin', 'merchant'])){

            return redirect('auth/logout');
        }

        return $next($request);
    }
}
