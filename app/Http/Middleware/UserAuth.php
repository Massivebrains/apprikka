<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserAuth
{
    public function handle($request, Closure $next)
    {   
        if(Auth::user()->type != 'user'){

            return redirect('auth/logout');
        }

        return $next($request);
    }
}
