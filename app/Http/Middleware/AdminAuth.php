<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{
    public function handle($request, Closure $next)
    {	
        if(Auth::user()->type != 'admin'){

            return redirect('auth/logout');
        }

        return $next($request);
    }
}
