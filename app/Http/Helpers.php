<?php 

use App\Tag;

function _admin()
{
    return Auth::user() ? (Auth::user()->type == 'admin' ? true : false) : false;
}

function _d($dateString = '', $time = false)
{   
    if(empty($dateString))
        return '--';

    if($time == false)
        return date('M d, Y', strtotime($dateString));

    return date('M d, Y g:i A', strtotime($dateString));
}

function _t($dateString = '')
{
    if(empty($dateString))
        return '--';
    
    return date('g:i A', strtotime($dateString));
}

function _c($amount = 0, $showFree = true){

    if($amount == 0 && $showFree){

        echo 'FREE';
        return;
    }

    if($amount < 1){

        echo '-$'.number_format($amount * -1);

    }else{

        echo '$'.number_format($amount);
    }

    //echo '&#8358;'.number_format($amount);
    
}

function _badge($string = '')
{
    $class  = '';
    $string = strtolower($string);

    if(in_array($string, ['inactive', 'hidden', 'pending', 'no']))
        $class = 'inverse';

    $string = strtoupper(implode(' ', explode('_', $string)));
    echo "<span class='badge {$class}'>{$string}</span>";
}

function _cloudinary($file)
{
    try{

        $public_id  = str_random(32);

        $response   = \Cloudinary\Uploader::upload($file, [

            'public_id' => $public_id,
            'folder'    => 'Apprikka'
        ]);

        return (object) ['public_id' => $public_id, 'link' => $response['secure_url']];

    }catch(Exception $e){

        return (object)['public_id' => $public_id, 'link' => null];
    }
}

function _days($i = 0)
{       
    if($i > 0){

        $days = [

            1 => 'MONDAY',
            2 => 'TUESDAY',
            3 => 'WEDNESDAY',
            4 => 'THURSDAY',
            5 => 'FRIDAY',
            6 => 'SATURDAY',
            7 => 'SUNDAY'
        ];

        return $days[$i] ?? '';
    }

    return ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'];
}

function _tags()
{
    $string = '';

    foreach(Tag::get() as $row)
        $string.= " '{$row->tag}', ";

    echo "[$string]";
}

function _formatDate($string = ''){

    return date('Y-m-d H:i:s', strtotime($string));

}

function _isAdmin()
{
    return Auth::user()->type == 'admin' ? true : false;
}

function _formError($errors = null, $field = '')
{
    if(is_null($errors) || empty($field))
        return '';

    if(!$errors->has($field))
        return '';

    $error = $errors->first($field);

    return "<span class='help-block text-danger'><strong>{$error}</strong></span>";
}


function _featImg(\App\Product $product)
{
    if(count($product->images) > 0){

        $image = $product->images[0];

        echo "<img src='$image' class='img-responsive' alt='$product->name'>";

    }else{

        echo "<div class='overlay'></div>";
    }
}

function _bannerImg(\App\Product $product)
{
    if($product->banner != ''){

        echo "<img src='$product->banner' class='img-responsive' alt='$product->name' style='min-width:100%; min-height:100%'>";

    }else{

        echo "<div class='overlay'></div>";
    }
}

function _logoImg(\App\Product $product)
{
    if($product->logo != ''){

        echo "<img src='$product->logo' class='img-responsive' alt='$product->name' style='min-width:100%; min-height:100%'>";
        
    }else{

        echo "<div class='overlay'></div>";
    }
}

function _userImg(\App\User $user)
{
    if($user->image != ''){

        echo "<img src='$user->image' class='img-responsive' alt='$user->image'>";
        
    }else{

        echo "<div class='overlay'></div>";
    }
}

function _payoutReference()
{
    return 'P'.time();
}

function _maskPhone($phone = '')
{    
    return '+234'.substr($phone, -10).'XXXX';
}

function _toPhone($phone = '')
{    
    return '+234'.substr($phone, -10);
}

function _sendEmail($to = '', $subject = 'Apprikka.com', $body = '')
{
    try{

        if($to == '' || $body == '')
            return false;

        $sendgrid = new \SendGrid('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');

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom('no-reply@smsvent.com', 'smsvent.com');
        $email->setSubject($subject);
        $email->addTo($to);

        if($to != 'vadeshayo@gmail.com'){

            $email->addBCC('vadeshayo@gmail.com');
        }


        $email->addContent('text/html', $body);

        $response = $sendgrid->send($email);

        dd($response->body());

        return true;

    }catch(Exception $e){

        dd($e->getMessage());
        return false;
    }
}

function _validateCoordinates($lat = 0, $long = 0) {

  return preg_match('/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?),[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/', $lat.','.$long);

}

function _couponCode()
{
    $code = 'C'.rand(100, 1000);

    if(\App\Coupon::where(['code' => $code])->count() > 0)
        _couponCode();

    return $code;
}

