<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\SavedBusiness;
use App\SavedEvent;
use Auth;
use App\Review;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $hidden   = ['password', 'remember_token'];
    protected $guarded  = ['updated_at'];
    protected $appends  = ['name', 'rating'];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review', 'merchant_id');
    }

    public function savedProducts()
    {
    	return $this->hasMany('App\SavedProduct');
    }

    public function scopeMerchants($query)
    {
        return $query->where(['type' => 'merchant']);
    }

    public function scopeAdmins($query)
    {
        return $query->where(['type' => 'admin']);
    }

    public function scopeUsers($query)
    {
        return $query->where(['type' => 'user']);
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }


    public function getNameAttribute()
    {
        return ucfirst($this->first_name.' '.$this->last_name);
    }

    public function getRatingAttribute()
    {
        $reviews = collect(Review::where(['merchant_id' => $this->id, 'status' => 'visible'])->get());

        if($reviews->count() < 1)
            return 0;

        return round($reviews->sum('review') / $reviews->count(), 1);
    }

    public static function hasSavedProduct($user_id = 0, $product_id = 0)
    {
        if(!Auth::check())
            return 0;

    	$saved = SavedProduct::where([

    		'user_id' 		=> Auth::user()->id, 
    		'product_id' 	=> $product_id

    	])->count();

    	if($saved > 0)
    		return 1;

    	return 0;
    }

    public function getImageAttribute($value)
    {
        if(empty($value))
            return asset('public/images/profile.jpeg');

        return $value;
    }
}
