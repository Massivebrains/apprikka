<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local extends Model
{
	public $timestamps 	= false;
    protected $guarded  = ['updated_at'];

    public function state()
    {
    	return $this->belongsTo('App\State');
    }
}
