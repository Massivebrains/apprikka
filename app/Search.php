<?php

namespace App;
use DB;
use App\Product;

class Search
{
    public static function search($term = '')
    {
        return Product::where('name', 'like', "%$term%")->take(20)->get();
    }
}
