<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $hidden   = ['updated_at', 'deleted_at'];
    protected $guarded  = ['updated_at'];
    protected $appends 	= ['parent'];

    public function getParentAttribute()
    {
    	return $this->attributes['parent'] = Category::firstOrNew(['id' => $this->parent_id]);
    }

    public function products()
    {
    	return $this->hasMany('\App\Product');
    }

    public function scopeBusinesses($query)
    {
        return $query->where(['type' => 'business']);
    }

    public function scopeDeals($query)
    {
        return $query->where(['type' => 'deal']);
    }

    public function scopeEvents($query)
    {
        return $query->where(['type' => 'event']);
    }

    public function scopeGroups($query)
    {
        return $query->where(['type' => 'group']);
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }

    public function scopeParents($query)
    {
        return $query->where(['parent_id' => 0]);
    }
}
