<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	protected $hidden   = ['updated_at', 'deleted_at'];
	protected $guarded  = ['updated_at'];

	public function order()
	{
		return $this->belongsTo('\App\Order');
	}

    public function merchant()
    {
        return $this->belongsTo('App\User', 'merchant_id');
    }

	public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function option()
    {
    	return $this->belongsTo('App\Option');
    }
}
