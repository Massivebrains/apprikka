<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Review;
use App\BusinessHour;
use Carbon\Carbon;
use Auth;
use App\ProductTag;
use App\Tag;

class Product extends Model
{
    protected $hidden   = ['updated_at', 'deleted_at'];
    protected $guarded  = ['updated_at'];

    protected $appends  = [

        'rating', 
        'review_count', 
        'open', 
        'facebook', 
        'twitter', 
        'google', 
        'whatsapp', 
        'suggested',
        'distance'
    ];

    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');  
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function place()
    {
        return $this->hasOne('App\Place', 'id', 'place_id');
    }

    public function options()
    {
        return $this->hasMany('App\Option');
    }

    public function days()
    {
        return $this->hasMany('App\BusinessHour');
    }

    public function views()
    {
        return $this->hasMany('App\Views');
    }

    public function scopeBusinesses($query)
    {
        return $query->where(['type' => 'business']);
    }

    public function scopeDeals($query)
    {
        return $query->where(['type' => 'deal']);
    }

    public function scopeEvents($query)
    {
        return $query->where(['type' => 'event']);
    }

    public function scopeGroups($query)
    {
        return $query->where(['type' => 'group']);
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }

    public function scopeFeatured($query)
    {
        return $query->where(['featured' => 1]);
    }

    public function scopeNotFeatured($query)
    {
        return $query->where(['featured' => 0]);
    }

    public function scopeRecentlyViewed($query)
    {
        $date = (new Carbon())->subWeek()->toDateTimeString();

        return $query->where('last_viewed_at', '>=', $date);
    }

    public function scopeBy($query, $user_id = 0)
    {
        return $query->where(['user_id' => $user_id]);
    }

    public function scopeFree($query)
    {
        return $query->where(['price_type' => 'free']);
    }

    public function scopePaid($query)
    {
        return $query->where(['price_type' => 'paid']);
    }

    public function scopeToday($query)
    {
        return $query->whereDate('start_date', date('Y-m-d'));
    }

    public function scopeTomorrow($query)
    {
        return $query->whereDate('start_date', date('Y-m-d', strtotime('+1 day')));
    }

    public function scopeThisWeek($query)
    {
        $start_date     = (new Carbon())->startOfWeek()->toDateTimeString();
        $end_date       = (new Carbon())->endOfWeek()->toDateTimeString();

        return $query->whereDate('start_date', '>=', $start_date)->whereDate('start_date', '<=', $end_date);
    }

    public function scopeNextWeek($query)
    {
        $start_date     = (new Carbon())->addDays(7)->toDateTimeString();
        $end_date       = (new Carbon())->addDays(14)->toDateTimeString();

        return $query->whereDate('start_date', '>=', $start_date)->whereDate('start_date', '<=', $end_date);

    }

    public function scopeNextMonth($query)
    {
        return $query->whereMonth('start_date', date('m')+1);
    }

    public function scopeThisYear($query)
    {
        return $query->whereYear('start_date', date('Y'));
    }

    public function getImagesAttribute($value)
    {
        if(is_null($value) || empty($value))
            return [];

        return json_decode($value);
    }

    public function getTagsAttribute()
    {
        $product_tags = ProductTag::where(['product_id' => $this->id])->get();

        $tags = [];

        foreach($product_tags as $row){

            $tags[] = $row->tag;
        }

        return $this->attributes['tags'] = $tags;
    }

    public function getRatingAttribute()
    {
        $reviews = collect(Review::where(['product_id' => $this->id, 'status' => 'visible'])->get());

        if($reviews->count() < 1)
            return 0;

        return round($reviews->sum('review') / $reviews->count(), 1);
    }

    public function getReviewCountAttribute()
    {
        return Review::where(['product_id' => $this->id, 'status' => 'visible'])->count();
    }

    public function getOpenAttribute()
    {
        $businessHour = BusinessHour::where(['day' => _days(date('w')), 'product_id' => $this->id])->first();

        if(!$businessHour)
            return 'Closed Today';

        $now        = Carbon::now('UTC')->format('H:i');
        $opens      = Carbon::parse($businessHour->opens)->format('H:i');
        $closes     = Carbon::parse($businessHour->closes)->format('H:i');

        if($now >= $opens && $now <= $closes)
            return 'Open Now';

        return 'Open Today';
    }

    public function getFacebookAttribute()
    {   
        $link = \Share::page(url($this->type.'/'.$this->slug), null, [], '', '')->facebook();
        $link = str_replace('span', 'i', $link);
        $link = str_replace('<li>', '', $link);
        $link = str_replace('<li/>', '', $link);

        echo $link;
    }

    public function getTwitterAttribute()
    {   
        $link = \Share::page(url($this->type.'/'.$this->slug), null, [], '', '')->twitter();
        $link = str_replace('span', 'i', $link);
        $link = str_replace('<li>', '', $link);
        $link = str_replace('<li/>', '', $link);

        echo $link;
    }

    public function getGoogleAttribute()
    {   
        $link = \Share::page(url($this->type.'/'.$this->slug), null, [], '', '')->googlePlus();
        $link = str_replace('span', 'i', $link);
        $link = str_replace('<li>', '', $link);
        $link = str_replace('<li/>', '', $link);

        echo $link;
    }

    public function getWhatsappAttribute()
    {   
        $url = url($this->type.'/'.$this->slug);

        echo "<a href='https://web.whatsapp.com/send?text=$url'><i class='fa fa-whatsapp'></i></a>";
    }

    public function getSuggestedAttribute()
    {
        return self::where(['type' => $this->type])->orderBy('id', 'random')->take(3)->get();
    }

    public function getDistanceAttribute()
    {
        if(!session('location'))
            return '';

        $latitude   = session('latitude');
        $longitude  = session('longitude');

        if(!$latitude || !$longitude)
            return '';

        $place = $this->place;

        if(!$place)
            return '';

        $theta      = $place->longitude - $longitude;
        $distance   = sin(deg2rad($place->latitude)) * sin(deg2rad($latitude)) + cos(deg2rad($place->latitude)) * cos(deg2rad($latitude)) * cos(deg2rad($theta));
        $distance   = acos($distance);
        $distance   = rad2deg($distance);
        $miles      = $distance * 60 * 1.1515;

        if($miles < 1)
            return 'close by';

        return round($miles, 1).' mi';
    }

    public function getIsFreeAttribute()
    {   
        if($this->options->first()->price == 0)
            return true;

        return false;
    }


    public static function basicInfo($product_id = 0, $type = 'deal')
    {
        if(request('event_date') && request('duration')){

            $start_date = _formatDate(request('event_date'));
            
        }else{

            $start_date  = request('start_date') ? _formatDate(request('start_date')) : null;

        }

        $end_date   = request('end_date') ? _formatDate(request('end_date')) : null;

        $product = Product::updateOrCreate(['id' => $product_id], [

            'user_id'           => request('user_id') ?? Auth::user()->id,
            'type'              => $type,
            'name'              => request('name'),
            'slug'              => str_slug(request('name')),
            'category_id'       => request('category_id'),
            'subcategory_id'    => request('subcategory_id'),
            'country_id'        => request('country_id'),
            'state_id'          => request('state_id'),
            'place_id'          => request('place_id'),
            'address'           => request('address'),
            'start_date'        => $start_date,
            'end_date'          => $end_date,
            'regular_price'     => request('regular_price'),
            'sale_price'        => request('sale_price'),
            'description'       => request('description'),
            'website'           => request('website'),
            'duration'          => request('duration'),
            'start_time'        => request('start_time'),
            'end_time'          => request('end_time')
        ]);

        ProductTag::where(['product_id' => $product->id])->delete();

        if(request('tags') && is_array(request('tags'))){

            foreach(request('tags') as $row){

                $tag = Tag::updateOrCreate(['tag' => strtolower($row)], ['tag' => $row]);
                ProductTag::create(['product_id' => $product->id, 'tag_id' => $tag->id]);
            }
        }

        if($product_id < 1 && $type == 'business'){

            foreach(_days() as $row){

                BusinessHour::create([

                    'product_id'    => $product->id,
                    'day'           => $row
                ]);
            }

        }

        return $product;
    }
}
