<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class Payout extends Model
{
    protected $hidden   = ['updated_at', 'deleted_at'];
    protected $guarded  = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public static function balance($user_id = 0)
    {
    	$orderTotal 	= collect(Order::getMerchantOrders($user_id))->sum('amount');
    	$payoutTotal 	= self::where(['user_id' => $user_id])->where('status', '!=', 'cancelled')->sum('amount');

    	return $orderTotal - $payoutTotal;
    }
}
