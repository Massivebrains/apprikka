<div class="sidebar-widget request-quote" id="requestQuote">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="material-icons">&#xE14F;</i> REQUEST A QUOTE</h3>
        </div>
        <div class="panel-body">
            <form role="form" v-on:submit.prevent="submit" action="{{url('business/request-quote')}}">
                <div class="form-group">
                    <label>Full Name</label>
                    <input type="text" name="name" v-model="form.name" v-bind:disabled="disabled" class="form-control" placeholder="Your Name" required>
                </div>

                <div class="form-group">
                    <label>Email Address</label>
                    <input type="email" name="email" v-model="form.email" v-bind:disabled="disabled" class="form-control" placeholder="me@email.com" required>
                </div>

                <div class="form-group">
                    <label>Phone Number</label>
                    <input type="number" name="phone" v-model="form.phone" v-bind:disabled="disabled" class="form-control" placeholder="XXXXXXXXXX" required>
                </div>

                <div class="form-group">
                    <label>Needs</label>
                    <input type="text" name="title" v-model="form.title" v-bind:disabled="disabled" class="form-control" placeholder="Exact Services needed" required>
                </div>

                <div class="form-group">
                    <label>Tell us about your project</label>
                    <textarea name="description" v-model="form.description" v-bind:disabled="disabled" id="description" class="form-control" rows="3" placeholder="Hi, i need help with my project. I need to have it done by the end of next week, and i live in Lagos Nigeria. How muhc will it cost? Thanks!" rows="10"></textarea>
                </div>

                <button type="submit" id="submit" class="btn btn-success" v-html="submitText" v-bind:disabled="disabled"></button>
            </form>
        </div>
    </div>
</div>

@push('script')

<script type="text/javascript">

    var requestQuote = new Vue({

        el : '#requestQuote',

        data : {

            form : {

                name : '',
                email : '',
                phone : '',
                title : '',
                description : '',
                product_id : '{{$product->id}}',
                _token : '{{csrf_token()}}'
            },
            submitText : 'Submit',
            disabled : false
        },
        methods : {

            submit : function(e){

                this.submitText = '<i class="fa fa-spin fa-spinner"></i> Requesting...';
                this.disabled = true;
                
                $.post(e.target.action, this.form, response => {

                    if(response.status == false){

                        if(response.data === 'auth'){

                            $('#login-modal').modal();

                        }else{

                            toastr.error(response.data);
                        }

                        this.disabled = false;
                        this.submitText = 'Submit';
                        

                    }else{

                        toastr.success(response.data);
                        this.submitText = 'Quote request submited';
                    }

                });

            }
        }
    })
</script>
@endpush

