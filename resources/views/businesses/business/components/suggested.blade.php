@if(count($products) > 0)

<div class="sidebar-widgets" style="margin:0 20px; float:left; width:100%:">
    <h5>SUGGESTED BUSINESSES</h5>

    <div class="sidebar-suggested">

        <ul>

            @foreach($products as $row)

            <li>
                <div class="recent-view">

                    <div class="col-md-3">
                        <a href="{{url('business/'.$row->slug)}}">
                            <div class="feat-img">
                                {{_featImg($row)}}
                            </div>
                        </a>
                    </div>


                    <div class="col-md-9">
                        <div class="card-details">
                            <h5>
                                <a href="{{url('business/'.$row->slug)}}">
                                    {{$row->name}}
                                    <span>
                                        <i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i>
                                    </span>
                                </a>
                            </h5>
                            <span class="badge">{{$row->open}}</span>
                            @if($row->review > 0)
                            <div class="star-rating">
                                <span>{{$row->review}}.0</span> 
                                <i class="material-icons ic-sm">&#xE8D0;</i> 
                                {{$row->review_count}} Rating
                            </div>
                            @endif
                            <p class="add-mile">{{$row->address}} | <span>3.1 mi</span></p>
                        </div>
                    </div>
                </div>
            </li>

            @endforeach
        </ul>

    </div>

</div>

@endif
