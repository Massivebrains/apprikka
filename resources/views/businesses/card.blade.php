 <div class="featured-cards">
    <div class="row">
        <div class="col-md-5 feat-img-side">

            <a href="{{url('business/'.$business->slug)}}">
                <div class="feat-img">{{_featImg($business)}}</div>
            </a>

            <div class="profile-img">
                {{_logoImg($business)}}
            </div>
        </div>

        <div class="col-md-7">
            <div class="card-details-cover">
                <div class="card-details">
                    <h5>
                        <a href="{{url('business/'.$business->slug)}}">
                            {{ucfirst($business->name)}} 
                            <span>
                                <i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i>
                            </span>
                        </a>
                    </h5>
                    
                    <div class="star-rating">
                        @if($business->review_count > 0)
                        <span>{{$business->rating}}</span> 
                        <i class="material-icons ic-sm">&#xE8D0;</i> 
                        {{$business->review_count}} {{str_plural('Review', $business->review_count)}}
                        @else
                        &nbsp;
                        @endif
                    </div>
                    
                    <p class="card-desc">{!! str_limit($business->description, 200, '...') !!}</p>
                    <p class="add-mile">{{$business->address}} | <span>{{$business->distance}}</span></p>
                </div>


                <div class="card-meta">
                    <div class="row">

                        <div class="share-signal">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-whatsapp"></i></a></li>
                            </ul>
                        </div>

                        <div class="col-md-9">
                            <div class="tags">
                                <ul>
                                    @foreach($business->tags as $row)
                                    <li><a href="{{url('tag/'.$row->tag)}}" style="line-height: 40px;">{{ucfirst($row->tag)}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <ul class="share-save">
                                <li><a href="javascript:;" onclick="save({{$business->id}})"><i class="material-icons">bookmark</i></a></li>
                                <li>
                                    <a href="{{url('business/'.$business->slug)}}" class="saveddeal">
                                        <i class="material-icons">visibility</i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('business/'.$business->slug)}}" class="sharedeal">
                                        <i class="material-icons">share</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
