<div class="col-md-{{isset($col) ? $col : 6}}">

    <div class="carousel-item">

        <a href="{{url('business/'.$business->slug)}}">

            <div class="feat-img" style="width:350px; height: 230px;">
             {{_bannerImg($business)}}
             <div class="spots-status">{{$business->open}}</div>
         </div>
     </a>


     <div class="spots-head">

        <div class="col-md-3 col-sm-3 col-xs-3">

            <a href="{{url('business/'.$business->slug)}}">
                <div class="img-thumb">
                    @if($business->logo != '')
                    <img src="{{$business->logo}}" class="img-responsive" alt="{{str_limit($business->name, 20, '...')}}" style="min-width:100%; min-height:100%">
                    @else
                    <div class="overlay"></div>
                    @endif
                </div>
            </a>
        </div>

        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="spots-title">
                <h5 style="height:40px;">
                    <a href="{{url('business/'.$business->slug)}}">
                        {{str_limit($business->name, 80)}}
                        <span>
                            <i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i>
                        </span>
                    </a>
                </h5>

                <div class="star-rating">
                    @if($business->review_count > 0)
                    <span>{{round($business->rating, 1)}}.0</span> 
                    <i class="material-icons ic-sm">&#xE8D0;</i> 
                    {{$business->review_count}} {{str_plural('Review', $business->review_count)}}
                    @else
                    &nbsp;
                    @endif
                </div>

            </div>
        </div>
    </div>

    <div class="landmark-share">

        <div class="share-this">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <ul>
                            <li>{{$business->facebook}}</li>
                            <li>{{$business->twitter}}</li>
                            <li>{{$business->google}}</li>
                            <li>{{$business->whatsapp}}</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <i class="material-icons close-share">&#xE5CD;</i>
                    </div>
                </div>                                  
            </div>
        </div>

        <div class="spots-meta">
            <div class="col-md-8 col-sm-8 col-xs-8">
                <p class="add-mile"><i class="material-icons">&#xE55F;</i>  {{str_limit($business->place->name, 15, '...')}} | <span>{{$business->distance}}</span></p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <ul>
                    <li><a href="javascript:;" onclick="save({{$business->id}})"><i class="material-icons">bookmark</i></a></li>
                    <li><a href="{{url('business/'.$business->slug)}}"><i class="material-icons">visibility</i></a></li>
                    <li><a href="javascript:;" class="share-spot"><i class="material-icons">share</i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="tags">
        <ul>
            @foreach($business->tags as $row)
            <li><a href="{{url('tag/'.$row->tag)}}" style="line-height: 40px;">{{ucfirst($row->tag)}}</a></li>
            @endforeach
        </ul>
    </div>
    
</div>
</div>