@extends('layouts.frontend')

@section('content')

<div class="page-filers">
    <div class="container">
        <div class="filter-results">
            <p>FILTER RESULTS</p>
        </div>
    </div>
</div>

<section class="sections medium grey">

    <div class="container">

        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">{{ucfirst($type)}} by {{$user->name}}</li>
        </ol>

        <div class="row">

            <div class="col-md-9">

              <div class="spots-all-by">
                <div class="img-thumb">
                    {{_userImg($user)}}
                </div>

                <div class="spots-all-details">
                    <h4>{{$user->name}}</h4>
                    <div class="star-rating">
                        <span>{{$user->rating}}.0</span> 
                        <i class="material-icons ic-sm">&#xE8D0;</i> Rated | <a href="{{url('reviews-by/'.$user->slug)}}">({{$user->reviews->count()}} {{str_plural('Reviews', $user->reviews->count())}})</a>
                    </div>

                    <p>Member Since: <strong>{{date('M, Y', strtotime($user->created_at))}}</strong></p>

                    <a href="{{url('reviews-by/'.$user->slug)}}" class="btn btn-success">Drop a Review</a>
                </div>


            </div>

            <div class="page-headlines">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8 col-sm-8">
                            <h2 style="font-size:25px;font-weight:bold">{{ucfirst($type)}} by {{$user->name}}</h2>
                        </div>

                        @if($products->count()  > 0)

                        <div class="col-md-4 col-sm-4">
                            <select name="sort" class="form-control sort text-right">
                                <option value="">Sort by:</option>
                                <option value="featured">Featured {{ucfirst($type)}}</option>
                                <option value="recently-viewed">Recently Viewed</option>
                                <option value="newest">Newest to Oldest</option>
                                <option value="oldest">Oldest to Newest</option>
                            </select>
                        </div>

                        @endif

                    </div>
                </div>
            </div>





            <div class="category-results">
                <div class="col-md-12">


                    @if($products->count()  == 0)

                    <img src="{{asset('/svg/undraw_portfolio_essv.svg')}}" class="img img-reponsive text-center svg">

                    <h3 class="text-center" style="margin-top:20px; font-size:20px;">No Business match your search query</h3>

                    @endif

                    @foreach($products as $row)

                    @php
                    $view = $row->featured == 1 ? 'card' : 'card-mini';
                    @endphp

                    @if($row->type == 'business')

                    @component('businesses.'.$view, ['business' => $row]) @endcomponent

                    @elseif($row->type == 'deal')

                    @component('deals.'.$view, ['deal' => $row]) @endcomponent

                    @elseif($row->event == 'event')

                    @component('events.'.$view, ['event' => $row]) @endcomponent

                    @endif

                    @endforeach

                </div>

                {{ $products->links() }}

            </div>

        </div>


        <div class="col-md-3">

            @component('components.categories', ['categories' => $categories, 'type' => $type]) @endcomponent

        </div>

    </div>
</section>

@component('components.recent', ['recent' => 'light', 'feedback' => 'light', 'products' => $recently_viewed]) @endcomponent

@endsection

@push('script')

<script type="text/javascript">

    $('.sort').change(function(){

        let value = $('.sort').val();

        window.location = '{{url($type)}}?sort='+value;
    })

</script>

@endpush