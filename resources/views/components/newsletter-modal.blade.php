<div class="modal fade newsletter-modal" id="newsletter">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="col-md-6 no-pad">
					<img src="images/fashion.jpg" class="img-responsive" alt="">
					<!-- <div class="overlay"></div> -->
				</div>
				<div class="col-md-6">
					<div class="newsletter-box">
						<h1>Subscribe to Newsletter</h1>
						<p>Discover top Deals, Businesses and Events near you delivered to your mail. Easy, fast and simple.</p>
						<form action="" method="POST" role="form">
							<div class="form-group">
								<label for="">Enter your email address</label>
								<input type="text" class="form-control" id="" placeholder="someone@yourmail.com">
							</div>

							<button type="submit" class="btn btn-success">Submit</button>
						</form>
					</div>
				</div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>