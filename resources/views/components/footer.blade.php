<!-- #Global Footer -->
		<footer class="footer-reveal regular grey">
			<div class="container">
				<div class="col-md-4 col-sm-4">
					<div class="logo-footer">
						<a href="#"><img src="{{asset('images/logo.png')}}" class="img-responsive" alt=""></a>
						<h4>About Apprikaa</h4>
						<p>Get Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aliquam voluptates veritatis. </p>
						<ul class="social-icons">
							<li style="margin:10px"><a href="#"><i class="fa fa-facebook fa-2x"></i></a></li>
							<li style="margin:10px"><a href="#"><i class="fa fa-instagram fa-2x"></i></a></li>
							<li style="margin:10px"><a href="#"><i class="fa fa-twitter fa-2x"></i></a></li>
							<li style="margin:10px"><a href="#"><i class="fa fa-youtube fa-2x"></i></a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-2 col-sm-2">
					<h4>Discover</h4>
					<ul>
						<li><a href="{{url('businesses')}}">Businesses</a></li>
						<li><a href="{{url('deals')}}">Deals</a></li>
						<li><a href="{{url('events')}}">Events</a></li>
						<li><a href="javascript:;">Our Blog</a></li>
					</ul>
				</div>

				<div class="col-md-3 col-sm-3">
					<h4>Helpful Links</h4>
					<ul>
						<li><a href="{{url('claim-business')}}">Claim Business</a></li>
						<li><a href="{{url('how-it-works')}}">How it Works</a></li>
						<li><a href="{{url('terms-of-use')}}">Terms of Use</a></li>
						<li><a href="{{url('privacy-policy')}}">Privacy Policy</a></li>
						<li><a href="{{url('faq')}}">FAQs</a></li>
						<li><a href="{{url('contact-us')}}">Contact Us</a></li>
					</ul>
				</div>

				<div class="col-md-3 col-sm-3">
					<h4>Contact Us</h4>
					<ul>
						<li><a href="javascript:;">23, Jones Avenue, New Jersey </a></li>
						<li><a href="javascript:;">Phone: (123) 123-456 </a></li>
						<li><a href="mailto:info@apprikaa.com">E-Mail: info@apprikaa.com </a></li>
					</ul>
				</div>
			</div>

			<!-- Copyrights -->
			<div class="copyrights">
				<p>&copy; {{date('Y')}} Apprikaa. All rights reserved.</p>
			</div>
		</footer>