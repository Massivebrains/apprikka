<div class="sidebar-widgets">
    <h5>TOP EVENTS</h5>

    <div class="sidebar-suggested">
        <ul>

            <li>
                <div class="recent-view">

                    <div class="col-md-3">
                        <a href="services-view.html">
                            <div class="feat-img">
                                <img src="images/business.jpg" class="img-responsive" alt="">
                                <div class="overlay"></div>
                            </div>
                        </a>
                    </div>


                    <div class="col-md-9">
                        <div class="card-details">
                            <h5><a href="#">Rogers Limited <span><i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i></span></a></h5>
                            <span class="badge inverse">Closed Today</span>
                            <div class="star-rating">
                                <span>3.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> 6 Rating
                            </div>
                            <p class="add-mile">Ikeja, Lagos | <span>3.1 mi</span></p>
                        </div>
                    </div>
                </div>
            </li>


            <li>
                <div class="recent-view">

                    <div class="col-md-3">
                        <a href="services-view.html">
                            <div class="feat-img">
                                <img src="images/business.jpg" class="img-responsive" alt="">
                                <div class="overlay"></div>
                            </div>
                        </a>
                    </div>


                    <div class="col-md-9">
                        <div class="card-details">
                            <h5><a href="#">Rogers Limited <span><i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i></span></a></h5>
                            <span class="badge inverse">Closed Today</span>
                            <div class="star-rating">
                                <span>3.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> 6 Rating
                            </div>
                            <p class="add-mile">Ikeja, Lagos | <span>3.1 mi</span></p>
                        </div>
                    </div>
                </div>
            </li>

        </ul>
    </div>

</div>