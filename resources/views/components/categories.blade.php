<div class="main-sidebar" id="categoriesSidebar">

    <div class="sidebar-widgets">
        <div class="widget-title">
            <h5>CATEGORIES<span><i class="material-icons">add</i></span></h5>
        </div>

        <div class="widget-menu visible-default">
            <ul>
                @foreach($categories as $row)

                <li>

                    <a href="{{url($type.'/'.$row->slug)}}">
                        {{$row->name}} <span>({{$row->products()->count()}})</span>
                    </a>

                </li>

                @endforeach
            </ul>
        </div>
    </div>

    <div class="sidebar-widgets">
        <div class="widget-title">
            <h5>LOCATION <span><i class="material-icons">add</i></span></h5>
        </div>
        <div class="widget-menu visible-default">
            <ul>
                <li v-for="(place, index) in places" :key="place.id">
                    <a href="{{url('/search')}}">@{{place.name}} <span>(0)</span></a>
                </li>
            </ul>
        </div>
    </div>

    @php

        $events = \App\Product::events()->active();

    @endphp

    @if(isset($price_range_sidebar))

    <div class="sidebar-widgets">
        <div class="widget-title">
            <div class="widget-title">
                <h5>PRICE RANGE <span><i class="material-icons">add</i></span></h5>
            </div>
        </div>
        <div class="widget-menu visible-default">
            <ul>
                <li><a href="#">Free <span>(100)</span></a></li>
                <li><a href="#">Paid <span>(234)</span></a></li>
            </ul>
        </div>
    </div>
    @endif


    @if(isset($price_sidebar))

    <div class="sidebar-widgets">
        <div class="widget-title">
            <div class="widget-title">
                <h5>PRICES <span><i class="material-icons">add</i></span></h5>
            </div>
        </div>
        <div class="widget-menu visible-default">
            <ul>
                <li><a href="{{url('events?price_type=free')}}">Free <span>({{$events->free()->count()}})</span></a></li>
                <li><a href="{{url('events?price_type=paid')}}">Paid <span>({{$events->paid()->count()}})</span></a></li>
            </ul>
        </div>
    </div>

    @endif

    @if(isset($date_sidebar))

    <div class="sidebar-widgets">
        <div class="widget-title">
            <div class="widget-title">
                <h5>DATE <span><i class="material-icons">add</i></span></h5>
            </div>
        </div>
        <div class="widget-menu visible-default">
            <ul>
                <li><a href="#">Today <span>({{$events->today()->count()}})</span></a></li>
                <li><a href="#">Tomorrow <span>({{$events->tomorrow()->count()}})</span></a></li>
                <li><a href="#">This Week <span>({{$events->thisWeek()->count()}})</span></a></li>
                <li><a href="#">Next Week <span>({{$events->nextWeek()->count()}})</span></a></li>
                <li><a href="#">Next Month <span>({{$events->nextMonth()->count()}})</span></a></li>
                <li><a href="#">This Year <span>({{$events->thisYear()->count()}})</span></a></li>
            </ul>
        </div>
    </div>

    @endif

    @if(isset($event_type_sidebar))

    <div class="sidebar-widgets">
        <div class="widget-title">
            <div class="widget-title">
                <h5>EVENT TYPE <span><i class="material-icons">add</i></span></h5>
            </div>
        </div>
        <div class="widget-menu">
            <ul>
                <li><a href="#">Class <span>(100)</span></a></li>
                <li><a href="#">Conference <span>(34)</span></a></li>
                <li><a href="#">Networking <span>(65)</span></a></li>
                <li><a href="#">Seminar <span>(90)</span></a></li>
                <li><a href="#">Performance</span></a></li>
                <li><a href="#">Expo <span>(23)</span></a></li>
                <li><a href="#">Party <span>(15)</span></a></li>
                <li><a href="#">Other Festival <span>(99)</span></a></li>
                <li><a href="#">Retreat <span>(45)</span></a></li>
                <li><a href="#">Convention <span>(28)</span></a></li>
                <li><a href="#">Rally <span>(96)</span></a></li>
                <li><a href="#">Appearance <span>(17)</span></a></li>
                <li><a href="#">Tournament <span>(76)</span></a></li>
                <li><a href="#">Game <span>(87)</span></a></li>
                <li><a href="#">Gala <span>(36)</span></a></li>
            </ul>
        </div>
    </div>

    @endif


</div>

@push('script')
<script type="text/javascript">

    var categoriesSidebar = new Vue({

        el : '#categoriesSidebar',

        data : {

            places : []
        },

        created : function(){

            $.get('https://ipinfo.io', response => {

                $.post('{{url('places/locations')}}', response, places => this.places = places);

            }, 'jsonp');
            
        }
    })
</script>
@endpush