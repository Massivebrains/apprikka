<section class="sections regular text-center feedbacks {{isset($feedback) ? $feedback : 'white'}}">
	<div class="container">
		<div class="col-md-12">
			<img src="{{asset('/images')}}/chat.svg" class="img-responsive" alt="">
			<h3>Got Feedbacks?</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, vel! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, vel!</p>
			<a href="{{url('contact-us')}}" class="btn btn-success">Get In Touch</a>
		</div>
	</div>
</section>