<div class="modal fade" id="login-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header gradient-left-right">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Please Login to Continue</h4>
            </div>
            <div class="modal-body">

                <form method="POST" role="form" id="login-form">
                    <div class="form-group">

                        <div class="alert alert-danger error" style="display:none">test</div>

                        <div class="row">
                            
                            <div class="col-md-6">
                                <label>Email:</label>
                                <input type="email" name="login_email" class="form-control" placeholder="Email Address" required>   
                            </div>

                            <div class="col-md-6">
                                <label>Password:</label>
                                <input type="password" name="login_password" class="form-control" required>
                            </div>
                            
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Login</button>
                    <span class="pull-right">Dont have an account? <a href="{{url('auth/user-register')}}">Create an Account.</a></span>
                </form>

            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
    $(() => {

        let $form     = $('#login-form');
        let $email    = $('input[name=login_email]');
        let $password = $('input[name=login_password]');
        let $error    = $('.error');

        $form.submit((e) => {

            e.preventDefault();

            if(!$form.valid()) return;

            let data = {email : $email.val(), 'password' : $password.val()};

            $.post('{{url('auth/login')}}', data, response => {

                if(response.status == false){

                    $email.val('');
                    $password.val('');

                    $error.html(response.data).show();

                }else{

                    $('#login-modal').modal('toggle');

                    toastr.success('You are now logged in.');

                    setTimeout(() => location.reload(), 1500);
                    
                }
            });

        })
    })
</script>