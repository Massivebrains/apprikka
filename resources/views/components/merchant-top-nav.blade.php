<nav class="navbar navbar-default solid" role="navigation">

    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('/images')}}/logo.png" class="img-responsive" alt=""></a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">


            <ul class="nav navbar-nav navbar-right">
                <li class="action-btn backhome"><a href="{{url('/')}}">Back to Apprikaa <i class="material-icons">&#xE0B2;</i> </a></li>
                <li><a href="#">Hello {{Auth::user()->first_name}}!</a></li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle account-thumb" data-toggle="dropdown">
                        <img src="{{Auth::user()->image}}" class="img-responsive" alt="">
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{url('merchant/profile')}}">Profile</a></li>
                        <li><a href="{{url('auth/logout')}}">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>