@php

$public_key = env('PAYSTACK_STATUS') == 'test' ? env('PAYSTAC_TEST_PUBLIC') : env('PAYSTAC_LIVE_PUBLIC');

@endphp
<script type="text/javascript" src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript">

	function paystack(email = '', amount = 0, orderid = '', redirecturl = '', type = 'deal')
	{
		button = $('.pay');

		button.prop('disabled', true).html("<i class='fa fa-spin fa-spinner'></i> Loading Gateway...");

		var handler = PaystackPop.setup({

			key         : '{{$public_key}}',
			email       : email,
			amount      : amount*100,
			metadata    : {orderid : orderid },

			callback    : response => {

				button.prop('disabled', true).html("<i class='fa fa-spin fa-spinner'></i> Verifying Payment...");

				let url = `{{url('transaction/requery')}}/${response.reference}/${orderid}`;

				$.get(url, response => {

					if(response.status == 1){

						window.location = redirecturl+'/'+response.order_reference;

					}else{

						button.prop('disabled', false).html('Payment Failed. Click to try again');
					}
				})
			},

			onClose : function(){

				button.prop('disabled', false).html('Payment Failed. Click to try again');
			}
		});

		handler.openIframe();
	}
</script>