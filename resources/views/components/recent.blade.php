@if(isset($products) && $products->count() > 3)

 <section class="sections regular {{isset($recent) ? $recent : 'white'}}">
    <div class="container">

        <div class="titles-all">
            <div class="col-md-12">
                <h3>Recently Viewed</h3>
            </div>
        </div>

        <div id="recent">

            @foreach($products as $row)

            <div class="col-md-4">
                <div class="recent-view">

                    <div class="col-md-5">
                        <a href="{{url($row->type.'/'.$row->slug)}}">
                            <div class="feat-img">
                                {{_featImg($row)}}
                            </div>
                        </a>
                    </div>


                    <div class="col-md-7">
                        <div class="card-details">
                            <h5>
                                <a href="{{url($row->type.'/'.$row->slug)}}">
                                    {{str_limit($row->name, 30, '...')}} 
                                    <span>
                                        <i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i>
                                    </span>
                                </a>
                            </h5>
                            <span class="badge">{{$row->open}}</span>
                            @if($row->review_count > 0)
                            <div class="star-rating">
                                <span>{{$row->review}}.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> {{$row->review_count}} Rating
                            </div>
                            @endif
                            <p class="add-mile">{{$row->address}} | <span>3.1 mi</span></p>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach
            
        </div>

    </div>
</section>

@endif

@include('components.feedback')
