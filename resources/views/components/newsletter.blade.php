<section class="newsletter">
    <div class="container">
        <div class="col-md-4">
            <h3 class="text-left">Subscribe to Newsletter:</h3>
        </div>

        <div class="col-md-8">
            <form class="form-inline" id="newsletter_form">
                <div class="form-group">
                    <input type="text" class="form-control" id="newsletter_name" placeholder="Name">
                </div>
               
                <div class="form-group">
                    <input type="email" class="form-control" id="newsletter_email" placeholder="Enter your email address" required>
                </div>
                <button type="submit" id="newsletter_submit" class="btn btn-success">Subscribe</button>
            </form>
        </div>
    </div>
</section>

<script src="{{asset('/js/jquery.min.js')}}"></script>

<script type="text/javascript">
    
    $('#newsletter_form').submit(e => {

        e.preventDefault();

        $('#newsletter_form :input').prop('disabled', true);
        
        let data = {

            email   : $('#newsletter_email').val(),
            name    : $('#newsletter_name').val()
        }

        $.post('{{url('utils/mailchimp')}}', data, response => {

            $('#newsletter_form :input').prop('disabled', false);
            $('#newsletter_submit').text(response.data);
        });

    });
</script>