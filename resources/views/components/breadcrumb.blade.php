@php
    $path = request()->path();
    $paths = explode('/', $path);
    $i = 0;
@endphp

@if(count($paths) > 1 && $path != '/')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{url('/')}}">Home</a></li>
        @foreach($paths as $row)
            @if($i == count($paths)-1)
                <li class="active">{{ucwords($row)}}</li>
            @else
                <li><a href="#">{{ucwords($row)}}</a></li>
            @endif
        @php $i++ @endphp
        @endforeach
    </ol>
</div>
@endif