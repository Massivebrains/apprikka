@if(session('message'))
<div class="alert alert-success">
	{{session('message')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-info">
	{{session('error')}}
</div>
@endif
