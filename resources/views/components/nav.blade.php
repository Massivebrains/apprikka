<nav class="navbar navbar-default {{$nav}}" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle offcanvas-toggle pull-right" data-toggle="offcanvas" data-target="#js-bootstrap-offcanvas" style="float:left;">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/'.$logo)}}" class="img-responsive" alt=""></a>
		</div>

		<a class="login-btn-mobile visible-sm visible-xs" href="{{url('auth')}}">
			<i class="material-icons ic-sm">&#xE853;</i> My Account
		</a>

		<div  class="navbar-offcanvas navbar-offcanvas-touch" id="js-bootstrap-offcanvas">
			<a class="mobile-logo" href="{{url('/')}}"><img src="{{asset('images/'.$logo)}}" class="img-responsive" alt=""></a>
			
			<ul class="nav navbar-nav navbar-right">
				<li><a href="{{url('businesses')}}">Businesses</a></li>
				<li><a href="{{url('deals')}}">Deals</a></li>
				<li><a href="{{url('events')}}">Events</a></li>
				<li><a href="{{url('groups')}}">Groups</a></li>
				<li class="action-btn"><a href="{{url('auth/merchant')}}"><i class="material-icons">&#xE8D3;</i> Merchants</a></li>

				<li class="account-btn"><a href="{{url('auth')}}"><i class="material-icons ic-sm">&#xE853;</i> My Account</a></li>
				<li class="dropdown cart-holder">
					<a href="{{url('cart')}}">
						<i class="material-icons">&#xE8CB;</i> <span class="badge" id="cartCount">0</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>

<script type="text/javascript">

	@if(session('order_reference'))
	setInterval(() => {

		$.get('{{url('cart/count')}}', response => {

			$('#cartCount').html(response.data);

		});

	}, 1000 * 2);
	@endif
</script>