<div class="col-md-2">

    <div class="row">
        <div class="navigation">
            <div class="col-md-12">
                <div class="toggle-menu">
                    <button type="button" class="btn btn-success">MENU</button>
                </div>
            </div>
            <div class="profile-details">
                <div class="img-thumb">
                    <img src="{{Auth::user()->image}}" class="img-responsive" alt="{{Auth::user()->first_name}}">
                </div>

                <div class="acount-summary">
                    <h4>{{Auth::user()->first_name.' '.Auth::user()->last_name}}</h4>
                    <span class="badge">{{strtoupper(Auth::user()->type)}}</span>
                </div>
            </div>


            <div class="menu">
                <ul>

                    <li>
                        <a href="{{url(Auth::user()->type)}}" class="active">
                            <i class="material-icons">&#xE871;</i> Dashboard
                        </a>
                    </li>

                    <li class="has-dropdown">
                        <a href="#">
                            <i class="material-icons">&#xE563;</i> Businesses 
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/products/business')}}">
                                    All Businesses
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/product/basic-info/0/business')}}">
                                    Create New
                                </a>
                            </li>
                            @if(Auth::user()->type == 'admin')
                            <li>
                                <a href="{{url('admin/categories/business')}}">
                                    Categories
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/sub-categories/business')}}">
                                    Sub Categories
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="has-dropdown"><a href="javasacript:;"><i class="material-icons">&#xE8CC;</i> Deals</a> <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/products/deal')}}">
                                    All Deals
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/product/basic-info/0/deal')}}">
                                    Create New
                                </a>
                            </li>
                            @if(Auth::user()->type == 'admin')
                            <li>
                                <a href="{{url('admin/categories/deal')}}">
                                    Categories
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/sub-categories/deal')}}">
                                    Sub Categories
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a href="javascript:;">
                            <i class="material-icons">&#xE24F;</i> Events
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/products/event')}}">
                                    All Events
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/product/basic-info/0/event')}}">
                                    Create New
                                </a>
                            </li>
                            @if(Auth::user()->type == 'admin')
                            <li>
                                <a href="{{url('admin/categories/event')}}">
                                    Categories
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/sub-categories/event')}}">
                                    Sub Categories
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a href="#">
                            <i class="material-icons">people_outline</i> Groups 
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/products/group')}}">
                                    All Groups
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/product/basic-info/0/group')}}">
                                    Create New
                                </a>
                            </li>
                            @if(Auth::user()->type == 'admin')
                            <li>
                                <a href="{{url('admin/categories/group')}}">
                                    Categories
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/sub-categories/group')}}">
                                    Sub Categories
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a href="{{url('merchant/orders')}}">
                            <i class="material-icons">&#xE89A;</i> Orders
                        </a>
                    </li>
                    
                    <li class="has-dropdown">
                        <a href="javascript:;">
                            <i class="material-icons">card_giftcard</i> Coupons
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/coupons')}}">
                                    All Coupons
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/coupons/coupon/0')}}">
                                   New Coupon
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/coupons?used=1')}}">
                                   Used Coupons
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/coupons?unsed=0')}}">
                                   Unused Coupons
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a href="javascript:;">
                            <i class="material-icons">assignment</i> Quotes
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/quotes')}}">
                                    All Quotes
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/quotes/completed')}}">
                                    Completed Quotes
                                </a>
                            </li>
                        </ul>
                    </li>

                    @if(Auth::user()->type == 'admin')

                    <li class="has-dropdown">
                        <a href="javascript:;">
                            <i class="material-icons">nature_people</i> Merchants
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('admin/merchants')}}">
                                    All Merchants
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/merchants/basic-info/0')}}">
                                    Create New
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="has-dropdown">
                        <a href="{{url('admin/users')}}">
                            <i class="material-icons">group</i> Users
                        </a> 
                    </li>

                    <li class="has-dropdown">
                        <a href="#">
                            <i class="material-icons">supervised_user_circle</i> Admin
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('admin/admins')}}">
                                    All Admins
                                </a>
                            </li>
                            <li>
                                <a href="{{url('admin/admins/0')}}">
                                    Create Admin
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{url('admin/settings')}}">
                            <i class="material-icons">&#xE8B8;</i> Settings
                        </a>
                    </li>
                    
                    <li>
                        <a href="{{url('admin/reviews')}}">
                            <i class="material-icons">&#xE8D0;</i> Reviews
                        </a>
                    </li>
                    
                    @endif

                    @if(Auth::user()->type == 'merchant')

                    <li class="has-dropdown">
                        <a href="#">
                            <i class="material-icons">&#xE41D;</i> Payouts
                        </a> 
                        <i class="material-icons arrow">keyboard_arrow_down</i>
                        <ul>
                            <li>
                                <a href="{{url('merchant/payouts')}}">
                                    All Payouts
                                </a>
                            </li>
                            
                            <li>
                                <a href="{{url('merchant/payouts/request')}}">
                                    Request Payout
                                </a>
                            </li>
                            
                        </ul>
                    </li>

                    @else

                    <li>
                        <a href="{{url('admin/payouts')}}">
                            <i class="material-icons">&#xE41D;</i> Payouts
                        </a>
                    </li>

                    @endif
                    
                    {{-- <li><a href="#"><i class="material-icons">&#xE8B8;</i> Settings</a></li> --}}
                    <li>
                        <a href="{{url('auth/logout')}}">
                            <i class="material-icons">&#xE8AC;</i> Logout
                        </a>
                    </li>

                </ul>
            </div>

        </div>
    </div>
</div>