<div class="page-searches">
    <div class="col-md-12">

        <div class="container">
            <form action="{{url('search')}}" method="POST" class="form-inline" role="form">

                {{csrf_field()}}
                <div class="form-group">
                    <label class="sr-only">search</label>
                    <input type="text" name="query" class="form-control" placeholder="Search Apprikaa" required>
                </div>

                <div class="form-group">
                    <label class="sr-only" for="">location</label>
                    <input type="text" name="location" class="form-control" placeholder="Location">
                </div>

                <button type="submit" class="btn btn-success">Search</button>

            </form>
        </div>
    </div>
</div>