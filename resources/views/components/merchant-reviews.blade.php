@php
$name = $email = $phone = '';

if(Auth::check()){

    $user   = Auth::user();
    $name   = $user->first_name.' '.$user->last_name;
    $email  = $user->email;
    $phone  = $user->phone;
}

@endphp

<div class="section-bottom" id="reviews">

    <button type="button" class="btn btn-success btn-review btn-lg"><i class="material-icons"> create </i> Write a Review</button>

    <hr id="hr">


    <div class="comment-box">

        <h4>Rate {{$user->name}} & Write a Review</h4>

        <form id="reviewForm" method="post">

            <div class="comment-rate">
                <input id="rating-system" type="number" name="review" class="rating" min="1" max="5" step="1" size="lg">
            </div>

            <div class="row">
                <div class="col-md-4">
                    <label>Your name</label>
                    <div class="form-group">
                        <input type="text" name="name"  class="form-control" value="{{$name}}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Your email</label>
                    <div class="form-group">
                        <input type="text" name="email"  class="form-control" value="{{$email}}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <label>Your Phone </label>
                    <div class="form-group">
                        <input type="text" name="phone"  class="form-control" value="{{$phone}}" required>
                    </div>
                </div>                          
            </div>
            <div class="form-group">
                <label>Comments</label>
                <textarea placeholder="Tell us more" rows="5" class="form-control" name="comment"></textarea>
            </div>
            <div class="form-group">
                <input type="hidden" name="merchant_id" value="{{$user->id}}">
                <button class="btn btn-success" type="submit">Submit Review</button>
            </div>
        </form>

    </div>


    <h4>REVIEWS AND RATINGS</h4>
    <div class="review-head">
        <p>{{$user->reviews->count()}} Reviews for {{$user->name}}</p>
    </div>

    <div class="reviews-wrap" id="reviews">

        @foreach($reviews as $row)
        <div class="rev-box">
            <div class="col-md-9">
                <div class="feat-img">
                    <div class="overlay"></div>
                </div>
                <div class="reviewer-details">
                    <h4>Review for {{$user->name}}</h4>
                    <p>by {{$row->name}}| <span><i class="material-icons">&#xE192;</i> {{_d($row->created_at)}}</span></p>
                </div>
            </div>
            <div class="col-md-3 text-right">
                <div class="star-rating">
                    <span>{{$row->review}}.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> {{$row->review}} Rating
                </div>
                <p>Excellent</p>
            </div>

            <p class="rev-contents">
                {{$row->comment}}
            </p>
        </div>
        @endforeach


    </div>
</div>

<script type="text/javascript">

    $('#reviewForm').submit(e => {

        e.preventDefault();

        let payload = $('#reviewForm').serializeObject();

        $.post('{{url('review')}}', payload, response => {

            $('.comment-box').slideUp();
            $('.reviews-wrap').prepend(`<div class="alert alert-info">${response.data}</div>`);
        });
    })
</script>