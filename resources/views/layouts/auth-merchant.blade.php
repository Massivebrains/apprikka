
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Apprikaa Login</title>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700">
  <link rel="stylesheet" href="{{asset('/dist/css/auth-merchant.css')}}">
</head>

<body>

  <div class="site-wrapper">

    <div class="site-wrapper-inner">

      <div class="cover-container">

        <div class="masthead clearfix">
          <div class="inner">
            <h3 class="masthead-brand">
              <a href="{{url('/')}}">
                <img src="{{asset('')}}images/logo-light.png" class="img-responsive" alt="">
              </a>
            </h3>
            <nav>
              <ul class="nav masthead-nav">
                <li class="active">
                  @if(isset($page) && $page == 'login')
                  <a href="{{url('auth/merchant-register')}}">
                    <i class="material-icons">&#xE148;</i> Create an Account
                  </a>
                  @else
                  <a href="{{url('auth/merchant')}}">
                    <i class="material-icons">&#xE8A6;</i> Login
                  </a>
                  @endif
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <div class="inner cover">
          <div class="col-md-7 col-md-offset-2">
            <h2>{{$page == 'login' ? 'Login to Your Account' : 'Creat an Account'}}</h2>
            <br>

            @yield('content')

          </div>
        </div>

        <div class="mastfoot">
          <div class="inner">
            <p>&copy; {{date('Y')}} Apprikaa. All right reserved.</p>
          </div>
        </div>

      </div>

    </div>

  </div>

  <script src="{{asset('/js/jquery.min.js')}}"></script>
  <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>

  <script type="text/javascript">

    $('.country_id').change(() => {

      $.get('{{url('utils/states')}}/'+$('.country_id').val(), response => {

        $('.state_id').html(response)
      })
    })

    $('.country_id').trigger('change');

  </script>
</body>
</html>
