@extends('layouts.frontend')

@section('content')

<div class="profile-wrap">
    <div class="container">

        <div class="col-md-9">
            @yield('user-content')
        </div>      

        <div class="col-md-3">
            <div class="profile-user-menu">
                <div class="img-thumb">
                    <img src="{{asset('/images')}}//thumb.png" class="img-responsive" alt="">
                    <div class="overlay"></div>
                </div>
                <h4>{{Auth::user()->first_name.' '.Auth::user()->last_name}}</h4>
                <p>Lagos Nigeria</p>

                <div class="user-menu">
                    <div class="row">
                        <div class="col-md-12">
                            <ul>
                                <li>
                                    <a href="{{url('user')}}" class="{{$active == 'dashboard' ? 'active' : ''}}">
                                        <i class="material-icons">&#xE871;</i> Dashboard
                                    </a>
                                </li>

                                <li class="has-dropdown">
                                    
                                    <a href="{{url('user/saved')}}" class="{{$active == 'saved' ? 'active' : ''}}">
                                        <i class="material-icons">&#xE866;</i> Saved Items
                                    </a>
                                </li>

                                <li class="has-dropdown"><a href="{{url('user/orders')}}" class="{{$active == 'orders' ? 'active' : ''}}">
                                    <i class="material-icons">&#xE8CC;</i> Orders</a>
                                </li>
                                <li>
                                    <a href="{{url('user/quotes')}}" class="{{$active == 'quotes' ? 'active' : ''}}">
                                        <i class="material-icons">&#xE85D;</i> My Quotes
                                    </a>
                                </li>
                            </li>
                            <li>
                                <a href="#" class="{{$active == 'reviews' ? 'active' : ''}}">
                                    <i class="material-icons">&#xE8D0;</i> My Reviews
                                </a>
                            </li>
                            <li>
                                <a href="{{url('user/profile')}}" class="{{$active == 'profile' ? 'active' : ''}}">
                                    <i class="material-icons">&#xE8B8;</i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{url('auth/logout')}}">
                                    <i class="material-icons">&#xE8AC;</i> Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

</div>
@endsection