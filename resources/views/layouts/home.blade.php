<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Apprikaa {{isset($title) ? '- '.$title : ''}}</title>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700">
	<link rel="stylesheet" href="{{asset('/dist/css/home.css')}}">
	<script type="text/javascript">
		let BASE_PATH = '{{asset('/')}}'
	</script>
</head>
<body>
	
	<div id="main">
		
		@component('components.nav', ['nav' => '', 'logo' => 'logo-light.png']) @endcomponent

		@yield('content')

		@include('components.newsletter')
		@include('components.footer')
		
	</div>

	<script src="{{asset('/dist/js/home-min.js')}}"></script>
	<script src="{{asset('/js/location.js')}}"></script>
	<script type="text/javascript">

		@if(!session('location'))

		getLocationFromBrowser().then(location => {

			$.post('{{url('location')}}', location, response => {

				window.location = '{{url('/')}}'
			});

		}).catch(err => {

			console.error({'Location' : err.message});
		})

		@endif

		save = id => {

			$.get('{{url('products/save')}}/'+id, response => {

				if(response.status == true){

					toastr.success(response.data.message, 'SUCCESSFUL');

				}else{

					toastr.error(response.data, 'ERROR');
				}
			})
			 
		}

	</script>
	@include('components.drift')
</body>
</html>