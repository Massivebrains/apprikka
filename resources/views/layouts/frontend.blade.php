<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Apprikaa</title>
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700">
	<link rel="stylesheet" href="{{asset('/dist/css/frontend.css')}}">
	@yield('style')
	<script src="{{asset('/js/jquery.min.js')}}"></script>
	<script type="text/javascript">
		const IP_ADDRESS 	= '{{request()->ip()}}'
		const BASE_URL 		= '{{url('/')}}'
	</script>
</head>
<body>

	<div id="main">
		
		@component('components.nav', ['nav' => 'solid', 'logo' => 'logo.png']) @endcomponent

		@if(!isset($search))
		@component('components.search') @endcomponent
		@endif

		
		@yield('content')

		@include('components.footer')
		
	</div>


	<a href="#main" class="top"><i class="material-icons">&#xE5CE;</i></a>


	<div class="contact-btn">
		<a href="{{url('contact-us')}}" class="btn btn-default"><i class="material-icons">&#xE0B7;</i> Contact Us</a>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/vue"></script>
	<script src="{{asset('/dist/js/frontend-min.js')}}"></script>
	<script type="text/javascript">

		$('.country_id').change(() => {

			$.get('{{url('utils/states')}}/'+$('.country_id').val(), response => {

				$('.state_id').html(response)
			})
		})

		$('.country_id').trigger('change')
		$('.select2').select2()

		save = id => {

			$.get('{{url('products/save')}}/'+id, response => {

				if(response.status == true){

					toastr.success(response.data.message, 'SUCCESSFUL');

				}else{

					toastr.error(response.data, 'ERROR');
				}
			})
			 
		}
		
	</script>
	
	@stack('script')
	@include('components.drift')

</body>
</html>