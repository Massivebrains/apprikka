<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>My Account - Apprikaa</title>
  <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700">
  <link rel="stylesheet" href="{{asset('/dist/css/auth.css')}}">
</head>
<body>

  <div id="login">

   <div class="col-md-6 col-sm-6 col-md-push-6">
    <div class="divide-right">
      <div class="col-md-6 col-md-offset-3">
       <a class="loginlogo" href="{{url('/')}}">
        <center>
          <a href="{{url('/')}}">
            <img src="{{asset('')}}images/logo.png" alt="" style="width: 9em; margin-bottom: 30px;">
          </a>
        </center>
      </a>

      @yield('content')


    </div>
  </div>

</div>


<div class="col-md-6 col-sm-6 col-md-pull-6 no-pad lefty" data-vide-bg="{{asset('')}}images/video.mp4">
  <div class="overlay login"></div>
  <div class="divide-left">
   <div class="contents">
     <h1>Welcome to Apprikaa, Let's go!</h1>
     <h4>Create an account with Apprikaa or Login to your account and get started.</h4>
     <ul class="social">
      <p style="color: #fff;">Follow Apprikaa:</p>
      <li><a href="#"><i class="fa fa-2x fa-facebook"></i></a></li>
      <li><a href="#"><i class="fa fa-2x fa-twitter"></i></a></li>
      <li><a href="#"><i class="fa fa-2x fa-instagram"></i></a></li>
      <li><a href="#"><i class="fa fa-2x fa-linkedin"></i></a></li>
    </ul>
  </div>
  <div class="footer-info">
   <ul>
     <li><a href="#">ABOUT US</a></li>
     <li><a href="#">HOW IT WORKS</a></li>
     <li><a href="#">CONTACT US</a></li>
   </ul>
 </div>
</div>
</div>



<div class="modal fade" id="forgot-pass">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

</div>

<script src="{{asset('/js/jquery.min.js')}}"></script>
<script src="{{asset('/js/vide.js')}}"></script>
<script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/select2/js/select2.min.js')}}"></script>

<script type="text/javascript">
  
  $('.select2').select2();

  $('.country_id').change(() => {

    $.get('{{url('utils/states')}}/'+$('.country_id').val(), response => {

      $('.state_id').html(response)
    })
  })

  $('.country_id').trigger('change');

</script>
</body>
</html>