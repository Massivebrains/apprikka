<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Apprikaa {{isset($title) ? '- '.$title : ''}}</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700">
    <link rel="stylesheet" href="{{asset('/dist/css/merchant.css')}}">
    <script src="{{asset('/js/jquery.min.js')}}"></script>
    <script type="text/javascript">
        const IP_ADDRESS = '{{request()->ip()}}'
    </script>
    <body>

        <div id="main">

            @include('components.merchant-top-nav')


            <div class="global-dashboard">

                <div class="container">

                    @include('components.merchant-sidebar')

                    <div class="col-md-10">

                        <div class="row">
                            <div class="main-dash-panel">

                                <div class="dash-title">
                                    <div class="col-md-12">
                                        <h1>{{isset($title) ? strtoupper($title) : 'APPRIKAA DASHBOARD'}}</h1>                  
                                    </div>
                                </div>

                                @yield('content')

                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <a href="#main" class="top"><i class="material-icons">&#xE5CE;</i></a>

            <div class="contact-btn">
                <a href="{{url('contact-us')}}" class="btn btn-default"><i class="material-icons">&#xE0B7;</i> Contact Us</a>
            </div>
        </div>
        <script src="{{asset('/dist/js/merchant-min.js')}}"></script>

        <script>

            $(() => {

                @if(session('message'))
                toastr.success('{{session('message')}}', 'SUCCESSFUL')
                @endif

                @if(session('error'))
                toastr.error('{{session('error')}}', 'FAILED')
                @endif

                $('.start_datetime').datetimepicker({

                    inline : true,
                    sideBySide : true,
                    @if(isset($start_date) && $start_date != '')
                    defaultDate: moment('{{$start_date}}', 'YYYY-MM-DD HH:mm'),
                    @endif
                })

                $('.end_datetime').datetimepicker({

                    inline : true,
                    sideBySide : true,
                    @if(isset($end_date) && $end_date != '')
                    defaultDate : moment('{{$end_date}}', 'YYYY-MM-DD HH:mm'),
                    @endif
                })

            })

            $('.tags').magicSuggest({

                allowFreeEntries: true, 
                allowDuplicates : false,
                'placeholder' : 'Press enter after every tag to add to list.',
                data: {{_tags()}},
                value : <?=isset($tags) ? $tags : '[]' ?>
            })

            $('.category_id').change(() => {

                $.get('{{url('utils/sub-categories')}}/'+$('.category_id').val(), response => {

                    $('.subcategory_id').html(response)
                })
            })

            $('.country_id').change(() => {

                $.get('{{url('utils/states')}}/'+$('.country_id').val(), response => {

                    $('.state_id').html(response)
                })
            })

            $('#datatable').DataTable()
            $('.select2').select2({ theme : 'bootstrap' })
            $('.date').datepicker()
            $('.time').timepicker()
            $('.textarea').trumbowyg({

                svgPath: '{{url('public/js/trumbowyg/ui/icons.svg')}}',
                autogrow: true
            })

            $('.confirm').click((e) => {

                e.preventDefault();

                swal({

                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#12ad2c',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, continue it!'

                }).then(result => {

                    if(result.value) window.location = e.target.href;
                });

            });

            $('.dropify').dropify()
            $('.tip').tooltipster({theme: 'tooltipster-shadow'})
            $('.editable').jinplace()

            $('.place').select2({

                ajax:{

                    url : '{{url('places/search-by-address')}}',
                    dataType: 'json',
                    delay : 250,
                    cache : true,
                    placeholder : 'Search for places',
                    minimumInputLength : 5
                },
                theme : 'bootstrap'

            })

        </script>
        @yield('scripts')
        @include('components.drift')
    </body>
    </html>