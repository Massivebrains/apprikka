<div class="col-md-{{isset($col) ? $col : 6}}">
    <div class="carousel-item">
        <a href="{{url('event/'.$event->slug)}}">
            <div class="feat-img" style="width:350px; height: 230px;">
                {{_bannerImg($event)}}
            </div>
        </a>


        <div class="spots-head">
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <div class="spots-title">
                        <h5 style="height:40px;">
                            <a href="{{url('event/'.$event->slug)}}">
                                {{str_limit($event->name, 40, '...')}}
                            </a>
                        </h5>
                        <p class="seller">
                            By: 
                            <a href="#">
                                {{$event->user->first_name.' '.$event->user->last_name}}
                            </a>
                        </p>
                        <div class="star-rating">
                            <p>
                                <i class="material-icons">&#xE616;</i>
                                @if($event->duration == 'one') 
                                {{_d($event->start_date)}}
                                @else
                                {{_d($event->start_date)}} - {{_d($event->end_date)}}
                                @endif

                            </p>
                            <p>
                                <i class="material-icons ic-sm">&#xE192;</i>
                                {{_t($event->start_time)}} - {{_t($event->end_time)}}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="coupon-prices">
                        <p>
                            <span class="sales">
                                {{_c($event->options->first()->price)}}
                            </span>
                        </p>

                    </div>
                </div>
            </div>
        </div>

        <div class="landmark-share">


            <div class="share-this">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <ul>
                                <li>{{$event->facebook}}</li>
                                <li>{{$event->twitter}}</li>
                                <li>{{$event->google}}</li>
                                <li>{{$event->whatsapp}}</li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <i class="material-icons close-share">&#xE5CD;</i>
                        </div>
                    </div>                                  
                </div>
            </div>

            <div class="spots-meta">
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <p class="add-mile" style="height:30px;">
                        <i class="material-icons">&#xE55F;</i>  
                        {{str_limit($event->place->name, 12, '...')}} | {{$event->distance}}
                    </p>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <ul>
                        <li><a href="javascript:;" onclick="save({{$event->id}})"><i class="material-icons">bookmark</i></a></li>
                        <li><a href="{{url('event/'.$event->slug)}}"><i class="material-icons">visibility</i></a></li>
                        <li><a href="javascript:;" class="share-spot"><i class="material-icons">&#xE80D;</i></a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tags">
            <ul>
                @foreach($event->tags as $row)
                <li><a href="{{url('tag/'.$row->tag)}}">{{ucfirst($row->tag)}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>