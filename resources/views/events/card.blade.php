<div class="featured-cards event">
	<div class="row">
		<div class="col-md-5 feat-img-side">
			<a href="{{url('event/'.$event->slug)}}">
				<div class="feat-img">
					{{_featImg($event)}}
				</div>
			</a>
		</div>

		<div class="col-md-7">
			<div class="card-details-cover">
				<div class="card-details">
					<h5><a href="{{url('event/'.$event->slug)}}">{{str_limit($event->name, 40, '...')}}</a></h5>
					<p class="seller">by <a href="#">{{$event->user->first_name.' '.$event->user->last_name}}</a></p>


					<ul class="share-save deal-share">
						<li><a href="javascript:;" onclick="save({{$event->id}})"><i class="material-icons">bookmark</i></a></li>
						<li><a href="{{url('event/'.$event->slug)}}"><i class="material-icons">visibility</i></a></li>
						<li><a href="javascript:;"><i class="material-icons">&#xE80D;</i></a></li>
					</ul>

					<p class="card-desc">{!! str_limit($event->description, 150, '...') !!}</p>
				</div>


				<div class="card-meta">
					<div class="row">
						<div class="col-md-7 col-sm-7 col-xs-7">
							<div class="tags">
								<p><i class="material-icons">&#xE55F;</i> {{$event->venue}} | <span>{{$event->distance}}</span></p>
								<p><i class="material-icons">&#xE616;</i> {{_d($event->start_date)}}</p>
								<p><i class="material-icons ic-sm">&#xE192;</i> {{_t($event->start_time)}} - {{_t($event->end_time)}}</p>
							</div>
						</div>
						<div class="col-md-5 col-sm-5 col-xs-5">
							<div class="coupon-prices">
								<p>
									<span class="sales">
										@if($event->options->first()->price > 0)
										{{_c($event->options->first()->price)}}
										@else
										FREE
										@endif
									</span>
								</p>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>