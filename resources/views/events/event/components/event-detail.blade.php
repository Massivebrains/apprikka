<div class="evt-all-details" id="eventDetail">
    <div class="evt-details-wrap">
        <div class="title">
            <h4>EVENT DETAILS</h4>
        </div>
        <div class="date-time-loc">
            <ul>
                <li><i class="material-icons">&#xE616;</i> {{_d($product->start_date)}}</li>
                <li><i class="material-icons">&#xE55F;</i> {{$product->address}}</li>
            </ul>
        </div>

        <div class="time-start-end">
            <ul>
                <li>
                    <p>STARTS</p>
                    {{_t($product->start_time)}}
                </li>
                <li>
                    <p>ENDS</p>
                    {{_t($product->end_time)}}
                </li>
            </ul>
        </div>

        <div v-if="options == 0" class="reg">
            <div class="row">
                <div class="col-md-6">
                    <button type="button" class="btn btn-success reg-evt pull-left" v-on:click="showOptions"><i class="material-icons">assignment_turned_in</i> REGISTER</button>
                </div>
                <div class="col-md-6">
                    <h4 class="pull-right">
                        {{$product->options->count()}} {{str_plural('Ticket', $product->options->count())}}
                    </h4>
                </div>
            </div>
        </div>
    </div>

    <div v-if="options == 1">
        <div class="col-md-12">
            <hr>

            <form method="post" action="{{url('add-to-cart')}}" v-on:submit.prevent="submit">

                <div class="form-group">
                    <label style="margin-bottom:2px;">Select Ticket</label>
                    <select name="option_id" class="form-control" id="option" v-model="form.option_id" v-bind:disabled="disabled" required>
                        @foreach($product->options as $row)
                        @if($row->name != '')
                        <option value="{{$row->id}}">
                            {{$row->name}} ({{$row->price > 0 ? _c($row->price) : 'FREE'}})</option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-success" v-html="submitText" v-bind:disabled="disabled" v-if="cart == 0"></button>
                <a href="{{url('cart')}}" class="btn btn-success" v-else>Proceed to Checkout</a>
                <a href="javascript:;" v-on:click="hideOptions" class="pull-right back-one">Back</a>
            </form>
        </div>
    </div>

</div>

@push('script')

<script type="text/javascript">

    var eventDetail = new Vue({

        el : '#eventDetail',

        data : {
            form : {
                option_id   : {{$product->options->first()->id}},
                product_id  : {{$product->id}},
                _token      : '{{csrf_token()}}'
            },
            submitText        : 'PROCEED',
            checkoutUrl       : '{{url('user/events/order/checkout')}}/',
            disabled          : false,
            options           : 0,
            cart              : 0
        },
        methods : {

            submit : function(e){

                this.submitText = '<i class="fa fa-spin fa-spinner"></i> Requesting...';
                this.disabled   = true;

                if(parseInt(this.form.option_id) < 1){

                    toastr.warning('Please select one of the options')
                    this.submitText = 'PROCEED';
                    this.disabled = false;

                    return;
                }
                
                $.post(e.target.action, this.form, response => {

                    if(response.status == false){

                        if(response.data === 'auth'){

                            $('#login-modal').modal();

                        }else{

                            toastr.error(response.data);
                            this.submitText = 'PROCEED';
                        }

                    }else{

                        toastr.success('Event has been added to your cart');
                        this.submitText = 'Event has been added to your cart';
                        this.cart = 1;
                    }

                    this.disabled = false;
                });

            },
            showOptions : function(){

                this.options = 1;
            },
            hideOptions : function(){

                this.options = 0;
            }
        }
    })
</script>

@endpush