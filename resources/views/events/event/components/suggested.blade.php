<div class="section-bottom">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">SUGGESTED EVENTS</h3>
        </div>
        <div class="panel-body">
            <div class="sidebar-suggested">

                <ul>

                    @foreach($product->suggested as $row)

                    <li>
                        <div class="recent-view">

                            <div class="col-md-3">
                                <a href="{{url('event/'.$row->slug)}}">
                                    <div class="feat-img">
                                        {{_featImg($row)}}
                                    </div>
                                </a>
                            </div>


                            <div class="col-md-9">
                                <div class="card-details">
                                    <h5>
                                        <a href="{{url('event/'.$row->slug)}}">
                                            {{$row->name}}
                                        </a>
                                    </h5>
                                    <span class="badge">{{_d($row->start_date)}}</span>
                                    <p class="add-mile">{{$row->address}} | <span>3.1 mi</span></p>
                                </div>
                            </div>
                        </div>
                    </li>

                    @endforeach
                </ul>

            </div>
        </div>
    </div>
</div>