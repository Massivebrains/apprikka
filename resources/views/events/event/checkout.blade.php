@extends('layouts.frontend')

@section('content')

<section class="deal-view regular">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Your Items</h1>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{$event->banner}}" class="img img-responsive img-rounded">
                            </div>
                            <div class="col-md-4">
                                <h3>{{$event->name}}</h3>
                                <code>Sold by {{$event->user->first_name. ' '.$event->user->last_name}}</code>
                            </div>
                            <div class="col-md-4">
                                <h1>{{_c($order->option->price)}}</h1>
                                <p>{{$order->option->name}}</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Billing Details</h1>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="checkout">

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{Auth::user()->first_name}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{Auth::user()->last_name}}" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Email Address</label>
                                    <input type="email" name="email" class="form-control" placeholder="Email Address" value="{{Auth::user()->email}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Phone</label>
                                    <input type="number" name="phone" class="form-control" placeholder="Phone" value="{{Auth::user()->phone}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Country</label>

                                    <select name="country_id" class="form-control country_id" required>
                                     @foreach(\App\Country::get() as $row)
                                     @if($row->id == Auth::user()->country_id)
                                     <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                     @else
                                     <option value="{{$row->id}}">{{$row->name}}</option>
                                     @endif
                                     @endforeach
                                 </select>

                             </div>
                             <div class="form-group col-md-6">
                                <label>State</label>
                                <select name="state_id" class="form-control state_id" required>
                                    <option value="0">--select country--</option>
                                </select>
                            </div>
                        </div>


                        <input type="hidden" name="order_reference" value="{{$order->order_reference}}">
                        {{csrf_field()}}
                        <button type="submit" class="btn btn-success pay">Place Order</button>
                    </form>
                </div>
            </div>

        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Order Summary</h1>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h3>
                                <strong>Subtotal</strong>
                                <p class="pull-right">{{_c($order->total)}}</p>
                            </h3>
                        </li>
                        <li class="list-group-item">
                            <h4>
                                <strong class="text-success">Promo / Gift Code</strong>
                                <p class="pull-right">{{_c(0)}}</p>
                            </h4>
                        </li>
                        <li class="list-group-item">
                            <h4>
                                <strong>Order Total</strong>
                                <p class="pull-right">{{_c($order->total)}}</p>
                            </h4>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
</section>

<script type="text/javascript">

    $checkout = $('#checkout');

    $checkout.submit(e => {

        e.preventDefault();

        let data = $checkout.serializeObject();
        $('#checkout :input').prop('disabled', true);

        $.post('{{url('user/events/order/details')}}', data, response => {

            toastr.success('Your billing details has been saved. Loading payment...');
            
            let { data }        = response;
            let redirectUrl     = '{{url('transaction/successful')}}';

            payWithPaystack(data.email, data.total, data.order_reference, redirectUrl);
        })
    })

    button = $('.pay');
</script>
@endsection