@extends('layouts.frontend')

@section('content')

<div class="business-hero evts gradient regular" style="background:url({{$product->banner}}) no-repeat; background-size: cover; background-position: center;">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="events-meta">
                    <h2>{{$product->name}}</h2>
                    <p>Organized by <a href="#">{{$product->user->first_name.' '.$product->user->last_name}}</a></p>
                </div>

            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">

            </div>
        </div>
    </div>
</div>

<div class="sections green">
    <div class="container">
        <div class="col-md-12">
            <ul class="evt-share-save">
                <li>
                    @if(\App\User::hasSavedProduct($product->id))
                    <a href="javascript:;" onclick="unsaveEvent({{$product->id}})">
                        <i class="material-icons">&#xE8E7;</i> UNSAVE EVENT
                    </a>
                    @else
                    <a href="javascript:;" onclick="saveEvent({{$product->id}})">
                        <i class="material-icons">&#xE8E7;</i> SAVE EVENT
                    </a>
                    @endif
                    
                </li>
                <li><a href="javascript:;" onclick=""><i class="material-icons">&#xE80D;</i> SHARE THIS</a></li>
            </ul>
        </div>
    </div>
</div>


<div class="sections regular grey">

    <div class="container">
        <div class="col-md-8">

            <div class="section-bottom">
               <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">DETAILS</h3>
                </div>
                <div class="panel-body text-justify">
                   {!! $product->description !!}
               </div>
           </div>               
       </div>

       <div class="section-bottom">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">LOCATION ON MAP</h3>
            </div>
            <div class="panel-body" style="padding:0;margin:0; height:500px;">
                <iframe src="https://maps.google.com/maps?q={{$product->place->latitude}},{{$product->place->longitude}}&hl=es;z=14&amp;output=embed" width="auto" frameborder="0" style="border:0;width:100%;height:500px;">
                </iframe>
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">

    @include('events.event.components.event-detail')

    @include('events.event.components.suggested')

</div>
</div>
</div>


@component('components.recent', ['recent' => 'grey', 'feedback' => 'white', 'products' => $recently_viewed]) @endcomponent

@include('components.login-modal')

@endsection


