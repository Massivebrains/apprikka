<div class="meta" id="groupHeader">

    <ul v-if="shareIcons">
        <li class="close-share-tools tips" title="Close" v-on:click="closeShare">
            <i class="fa fa-close"></i>
        </li>
        <li>{{$product->facebook}}</li>
        <li>{{$product->twitter}}</li>
        <li>{{$product->google}}</li>
        <li>{{$product->whatsapp}}</li>
    </ul>

    <ul class="head-tool" v-if="headTool">

        <li>
            <a href="javascript:;" v-if="saved == 1" @click="save">
                <i class="material-icons">&#xE8E6;</i> Saved
            </a>
        </li>

        <li>
            <a href="javascript:;" v-if="saved == 0" v-on:click="save">
                <i class="material-icons">&#xE8E7;</i> Save
            </a>
        </li>

        <li>
            <a href="javascript:;" class="share-business" @click="openShare">
                <i class="material-icons">&#xE80D;</i> Share
            </a>
        </li>
        <li>
            <a href="javascript:;" onclick="$('html, body').animate({ scrollTop: $('.comment-box').height() }, 1000);">
                <i class="material-icons">&#xE560;</i> Rate/Review
            </a>
        </li>
        
    </ul>
</div>

@push('script')
<script type="text/javascript" src="{{asset('/js/share.js')}}"></script>
<script type="text/javascript">

    var headerApp = new Vue({

        el : '#groupHeader',

        data : {

            saved       : {{\App\User::hasSavedProduct($product->id)}},
            save_url    : '{{url('products/save/'.$product->id)}}',
            shareIcons  : false,
            headTool    : true
        },
        methods : {

            save : function(){

                $.get(this.save_url, response => {

                    if(response.status == true){

                        toastr.success(response.data.message);
                        this.saved = response.data.saved;

                    }else{

                        toastr.error(response.data);
                    }
                })
                
            },
            openShare : function(){

                this.headTool = false;
                this.shareIcons = true;

            },
            closeShare : function(){

                this.headTool = true;
                this.shareIcons = false;
            }
        }
    })
</script>
@endpush