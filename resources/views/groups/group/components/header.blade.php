<section class="business-hero gradient regular" style="background:url({{$product->banner}}) no-repeat; background-size: cover; background-position: center;">
    <div class="container">
        @include('businesses.business.components.header-meta')
    </div>
</section>

<section class="spotlight-page-header">
    <div class="container">
        <div class="col-md-12">

            <div class="tools-btn">
                <i class="material-icons">reorder</i>   
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="feat-img">
                        @if($product->logo != '')
                        <img src="{{$product->logo}}" class="img-responsive" alt="">
                        @else
                        <div class="overlay"></div>
                        @endif
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="business-head-details">
                        <h3>
                            {{$product->name}} 
                            <span><i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i></span>
                        </h3>
                        <div class="star-rating">
                            <span>3.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> 6 Rating
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="addy-cat">
                        <ul>
                            <li><i class="material-icons">&#xE55F;</i> {{$product->address}} | 3.1 Mi</li>
                            <li>
                                <i class="material-icons">&#xE551;</i> 
                                @if(Auth::check())
                                {{_toPhone($product->phone)}}
                                @else
                                {{_maskPhone($product->phone)}} <button data-toggle="modal" href='#login-modal' class="btn btn-success btn-xs">VIEW</button>
                                @endif
                            </li>
                            <li><i class="material-icons">&#xE554;</i> {{$product->email}}</li>
                            <li><i class="material-icons">&#xE80B;</i> <a href="{{$product->website}}">{{$product->website}}</a></li>
                            <li><span class="badge badge-success">VERIFIED</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>