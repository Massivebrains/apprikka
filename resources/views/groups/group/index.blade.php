@extends('layouts.frontend')

@section('content')

@include('groups.group.components.header')


<div class="menu-sticky">
    <div class="container">
        <ul>
            <li><a href="#description">ABOUT</a></li>
            <li><a href="#hours-direction">LOCATION</a></li>
            <li><a href="#features">BUSINESSES</a></li>
            <li><a href="#reviews">REVIEWS</a></li>
        </ul>
    </div>
</div>

<section class="sections light medium">

    <div class="container">

        <div class="col-md-8">


            <div class="section-bottom" id="description">
                <h4>DESCRIPTION</h4>
                {!! $product->description !!}
            </div>

            <div class="section-bottom" id="hours-direction">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" style="height:310px;">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="material-icons">&#xE1B7;</i> VIEW ON MAP</h3>
                            </div>
                            <div class="panel-body" style="padding:0;margin:0">
                                <iframe src="https://maps.google.com/maps?q={{$product->place->latitude}},{{$product->place->longitude}}&hl=es;z=14&amp;output=embed" width="auto" frameborder="0" style="border:0;width:100%;height:calc(310px - 43px);">
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-bottom" id="features">
                <h4>TAGS</h4>
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            @foreach($product->tags as $row)
                            <li>
                                <a href="#">
                                    <i class="material-icons">&#xE5CA;</i> {{strtoupper($row->tag)}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            @include('components.reviews')

        </div>

        <div class="col-md-4">
            
            @component('groups.group.components.suggested', ['products' => $product->suggested]) @endcomponent

        </div>

    </div>
</div>
</section>

@component('components.recent', ['recent' => 'grey', 'feedback' => 'white', 'products' => $product->recently_viewed]) @endcomponent

@include('components.login-modal')

@endsection
