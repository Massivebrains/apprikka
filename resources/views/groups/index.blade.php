@extends('layouts.frontend')

@section('content')

<div class="page-filers">
    <div class="container">
        <div class="filter-results">
            <p>FILTER RESULTS</p>
        </div>
    </div>
</div>

<section class="sections medium grey">

    <div class="container">

        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">Groups</li>
        </ol>

        <div class="row">

            <div class="col-md-9">

                <div class="page-headlines">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8 col-sm-8">
                                <h2><strong>Groups</strong></h2>
                            </div>

                            @if($products->count()  > 0)

                            <div class="col-md-4 col-sm-4">
                                <select name="sort" class="form-control sort text-right">
                                    <option value="">Sort by:</option>
                                    <option value="featured">Featured groups</option>
                                    <option value="recently-viewed">Recently Viewed</option>
                                    <option value="newest">Newest to Oldest</option>
                                    <option value="oldest">Oldest to Newest</option>
                                </select>
                            </div>

                            @endif

                        </div>
                    </div>
                </div>



                <div class="category-results">
                    <div class="col-md-12">


                        @if($products->count()  == 0)

                        <img src="{{asset('/svg/undraw_portfolio_essv.svg')}}" class="img img-reponsive text-center svg">

                        <h3 class="text-center" style="margin-top:20px; font-size:20px;">No group match your search query</h3>

                        @endif

                        @foreach($products as $row)

                        @php
                        $view = $row->featured == 1 ? 'card' : 'card-mini';
                        @endphp

                        @component('groups.'.$view, ['group' => $row]) @endcomponent

                        @endforeach

                    </div>

                    {{ $products->links() }}

                </div>

            </div>


            <div class="col-md-3">

                @component('components.categories', ['categories' => $categories, 'type' => 'groups']) @endcomponent

            </div>

        </div>
    </section>

    @component('components.recent', ['recent' => 'light', 'feedback' => 'grey', 'products' => $recently_viewed]) @endcomponent

    @endsection

    @push('script')

    <script type="text/javascript">

        $('.sort').change(function(){

            let value = $('.sort').val();

            window.location = '{{url('groups')}}?sort='+value;
        })

    </script>

    @endpush