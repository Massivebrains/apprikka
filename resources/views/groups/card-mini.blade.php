<div class="col-md-{{isset($col) ? $col : 6}}">

    <div class="carousel-item">

        <a href="{{url('group/'.$group->slug)}}">

            <div class="feat-img" style="width:350px; height: 230px;">
             {{_bannerImg($group)}}
         </div>
     </a>


     <div class="spots-head">

        <div class="col-md-3 col-sm-3 col-xs-3">

            <a href="{{url('group/'.$group->slug)}}">
                <div class="img-thumb">
                    @if($group->logo != '')
                    <img src="{{$group->logo}}" class="img-responsive" alt="{{str_limit($group->name, 20, '...')}}" style="min-width:100%; min-height:100%">
                    @else
                    <div class="overlay"></div>
                    @endif
                </div>
            </a>
        </div>

        <div class="col-md-9 col-sm-9 col-xs-9">
            <div class="spots-title">
                <h5 style="height:40px;">
                    <a href="{{url('group/'.$group->slug)}}">
                        {{str_limit($group->name, 80)}}
                        <span>
                            <i class="material-icons ic-sm tips" title="Verified">&#xE86C;</i>
                        </span>
                    </a>
                </h5>

                <div class="star-rating">
                    @if($group->review_count > 0)
                    <span>{{round($group->rating, 1)}}.0</span> 
                    <i class="material-icons ic-sm">&#xE8D0;</i> 
                    {{$group->review_count}} {{str_plural('Review', $group->review_count)}}
                    @else
                    &nbsp;
                    @endif
                </div>

            </div>
        </div>
    </div>

    <div class="landmark-share">

        <div class="share-this">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <ul>
                            <li>{{$group->facebook}}</li>
                            <li>{{$group->twitter}}</li>
                            <li>{{$group->google}}</li>
                            <li>{{$group->whatsapp}}</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <i class="material-icons close-share">&#xE5CD;</i>
                    </div>
                </div>                                  
            </div>
        </div>

        <div class="spots-meta">
            <div class="col-md-8 col-sm-8 col-xs-8">
                <p class="add-mile"><i class="material-icons">&#xE55F;</i>  {{str_limit($group->place->name, 15, '...')}} | <span>{{$group->distance}}</span></p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <ul>
                    <li><a href="javascript:;" onclick="save({{$group->id}})"><i class="material-icons">bookmark</i></a></li>
                    <li><a href="{{url('group/'.$group->slug)}}"><i class="material-icons">visibility</i></a></li>
                    <li><a href="javascript:;" class="share-spot"><i class="material-icons">share</i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="tags">
        <ul>
            @foreach($group->tags as $row)
            <li><a href="{{url('tag/'.$row->tag)}}" style="line-height: 40px;">{{ucfirst($row->tag)}}</a></li>
            @endforeach
        </ul>
    </div>
    
</div>
</div>