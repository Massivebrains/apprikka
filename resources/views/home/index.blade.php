@extends('layouts.home')

@section('content')


<section class="hero" id="hero-section">

	<div class="hero-title">
		<div class="container">

			<div class="cd-intro">
				<h1 class="cd-headline rotate-1">
					<span>Find Nearby</span>
					<span class="cd-words-wrapper">
						<b class="is-visible">Services</b>
						<b>Deals</b>
						<b>Events</b>
					</span>
				</h1>
			</div>

			<h4>Expolore services, events, coupons and exciting deals</h4>
		</div>
	</div>


	<div class="hero-search">
		<form action="{{url('search')}}" method="POST" class="form-inline" role="form">

			{{csrf_field()}}

			<div class="form-group">
				<label class="sr-only" for="">search</label>
				<input type="text" class="form-control" name="query" placeholder="Search Apprikaa">
				<img src="{{asset('images/apprikaa-icon.svg')}}" class="img-responsive search-apr">
			</div>

			<div class="form-group">
				<label class="sr-only" for="">location</label>
				<input type="text" class="form-control" name="location" placeholder="Location">
				<i class="material-icons search-apr icon">&#xE0C8;</i>
			</div>

			<button type="submit" class="btn btn-primary">Search</button>
		</form>
	</div>

</section>


<section class="sections light">
	<div class="container">


		<div class="intro-box-wrapper">
			<div class="col-md-4 col-sm-4">
				<div class="intro-boxes">
					<img src="{{asset('images')}}/deals.svg" class="img-responsive" alt="">
					<h3>Discover Deals</h3>
					<p>Discover the best deal around you here, just a few clicks away, very simple.</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-4">
				<div class="intro-boxes">
					<img src="{{asset('images')}}/businesses.svg" class="img-responsive" alt="">
					<h3>Find Services</h3>
					<p>Discover the best deal around you here, just a few clicks away, very simple.</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-4">
				<div class="intro-boxes">
					<img src="{{asset('images')}}/events.svg" class="img-responsive" alt="">
					<h3>Find Events</h3>
					<p>Discover the best deal around you here, just a few clicks away, very simple.</p>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="sections regular grey">

	<div class="container">

		<div class="titles-all">
			<div class="col-md-9 col-sm-8 col-xs-8">
				<h3>Featured Businesses <span>Around You</span></h3>
			</div>

			<div class="col-md-3 col-sm-4 col-xs-4">
				<a href="{{url('businesses')}}" class="btn btn-default">View All <i class="material-icons">&#xE5C8;</i></a>
			</div>
		</div>


		<div class="spotlight-carousel" id="businesses">


			@foreach($businesses as $row)
				@component('businesses.card-mini', ['business' => $row, 'col' => 4]) @endcomponent
			@endforeach


		</div>
	</div>
</section>


<section class="sections regular peach">
	<div class="container">

		<div class="titles-all">
			<div class="col-md-9 col-sm-8 col-xs-8">
				<h3>Featured Deals <span>Around You</span></h3>
			</div>

			<div class="col-md-3 col-sm-4 col-xs-4">
				<a href="{{url('deals')}}" class="btn btn-default">View All <i class="material-icons">&#xE5C8;</i></a>
			</div>
		</div>


		<div class="spotlight-carousel" id="deals">


			@foreach($deals as $row)
				@component('deals.card-mini', ['deal' => $row, 'col' => 4]) @endcomponent
			@endforeach

		</div>
	</div>
</section>


<section class="sections regular grey">
	<div class="container">

		<div class="titles-all">
			<div class="col-md-9 col-sm-8 col-xs-8">
				<h3>Featured Events <span>Around You</span></h3>
			</div>

			<div class="col-md-3 col-sm-4 col-xs-4">
				<a href="{{url('events')}}" class="btn btn-default">View All <i class="material-icons">&#xE5C8;</i></a>
			</div>
		</div>


		<div class="spotlight-carousel" id="events">


			@foreach($events as $row)
				@component('events.card-mini', ['event' => $row, 'col' => 4]) @endcomponent
			@endforeach

		</div>
	</div>
</section>


<section class="sections regular light">

	<div class="container">

		<div class="titles-all">
			<div class="col-md-12">
				<h3>Reviews</h3>
			</div>
		</div>


		<div class="reviews-carousel">

			@foreach($reviews as $row)
			<div class="col-md-4">
				<div class="single-review">
					
					<div class="col-md-7 col-sm-7 col-xs-12">
						<div class="title">
							<h5>{{$row->name}}</h5>
						</div>
					</div>

					<div class="col-md-5 col-sm-5 col-xs-5">
						<div class="title">
							<p>
								<i class="material-icons">&#xE616;</i>{{_d($row->created_at)}}
							</p>
						</div>
					</div>

					

					<div class="col-md-12">
						<div class="rev-contents">
							<p>{{$row->comment}}</p>
						</div>
					</div>

					<div class="review-footer">
						<div class="col-md-12">
							<div class="star-rating">
								<span>{{$row->review}}</span> <i class="material-icons ic-sm">&#xE8D0;</i> {{$row->review}} Rating
							</div>
							<p>Review on: <a href="{{$row->product->type.'/'.$row->product->slug}}">{{$row->product->name}}</a></p>
						</div>
					</div>
				</div>
			</div>
			@endforeach

		</div>
	</div>
</section>


<section class="sections regular grey">
	<div class="container">

		<div class="titles-all">
			<div class="col-md-12">
				<h3>News & Updates</h3>
			</div>
		</div>


		<div class="articles-carousel">


			<div class="col-md-4 col-sm-4">
				<div class="single-article">
					<a href="#"><span class="post-cat">BUSINESS ETHICS</span></a>
					<a href="#">
						<div class="article-overlay">
							<img src="{{asset('images')}}/business.jpg" class="img-responsive" alt="">
							<div class="overlay"></div>
							<div class="article-meta">
								<div class="col-md-12">
									<h4>What Is The Big R For Marketing Your Business</h4>
									<p class="small"><i class="material-icons ic-sm">&#xE192;</i> Posted: 2 days ago</p>
								</div>
							</div>
						</div>
					</a>
					<div class="article-link">
						<a href="#">READ MORE <i class="material-icons">&#xE315;</i></a>
					</div>
				</div>
			</div>



			<div class="col-md-4 col-sm-4">
				<div class="single-article">
					<a href="#"><span class="post-cat">BRAINSTORMING</span></a>
					<a href="#">
						<div class="article-overlay">
							<img src="{{asset('images')}}/business.jpg" class="img-responsive" alt="">
							<div class="overlay"></div>
							<div class="article-meta">
								<div class="col-md-12">
									<h4>What Is The Big R For Marketing Your Business</h4>
									<p class="small"><i class="material-icons ic-sm">&#xE192;</i> Posted: 2 days ago</p>
								</div>
							</div>
						</div>
					</a>
					<div class="article-link">
						<a href="#">READ MORE <i class="material-icons">&#xE315;</i></a>
					</div>
				</div>
			</div>



			<div class="col-md-4 col-sm-4">
				<div class="single-article">
					<a href="#"><span class="post-cat">FINANCE</span></a>
					<a href="#">
						<div class="article-overlay">
							<img src="{{asset('images')}}/business.jpg" class="img-responsive" alt="">
							<div class="overlay"></div>
							<div class="article-meta">
								<div class="col-md-12">
									<h4>What Is The Big R For Marketing Your Business</h4>
									<p class="small"><i class="material-icons ic-sm">&#xE192;</i> Posted: 2 days ago</p>
								</div>
							</div>
						</div>
					</a>
					<div class="article-link">
						<a href="#">READ MORE <i class="material-icons">&#xE315;</i></a>
					</div>
				</div>
			</div>



			<div class="col-md-4 col-sm-4">
				<div class="single-article">
					<a href="#"><span class="post-cat">FINANCE</span></a>
					<a href="#">
						<div class="article-overlay">
							<img src="{{asset('images')}}/business.jpg" class="img-responsive" alt="">
							<div class="overlay"></div>
							<div class="article-meta">
								<div class="col-md-12">
									<h4>What Is The Big R For Marketing Your Business</h4>
									<p class="small"><i class="material-icons ic-sm">&#xE192;</i> Posted: 2 days ago</p>
								</div>
							</div>
						</div>
					</a>
					<div class="article-link">
						<a href="#">READ MORE <i class="material-icons">&#xE315;</i></a>
					</div>
				</div>
			</div>


		</div>

		<div class="btn-centered">
			<a href="#" class="btn btn-default"><i class="material-icons">&#xE8D2;</i> Read More Articles</a>
		</div>

	</div>
</section>


<section class="sections mob-app gradients">
	<div class="container">
		<div class="mobile-app">
			<div class="row">
				<div class="col-md-7">
					<h1>Download Apprikaa<br>Mobile App</h1>
					<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta repudiandae iusto atque.</h4>
					<a href="#" class="btn btn-default"><i class="material-icons">&#xE859;</i> Play Store</a>
					<a href="#" class="btn btn-default"><i class="fa fa-apple"></i> App Store</a>
				</div>

				<div class="col-md-5">
					<img src="{{asset('images')}}/app.png" class="img-responsive" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<div class="contact-btn">
	<a href="#" class="btn btn-default"><i class="material-icons">&#xE0B7;</i> Contact Us</a>
</div>

<a href="#main" class="top"><i class="material-icons">&#xE5CE;</i></a>

@endsection