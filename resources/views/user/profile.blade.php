@extends('layouts.user')

@section('user-content')

<div class="account-overview">

	<div class="dash-title">
		<div class="row">
			<div class="col-md-12">
				<h1>MY PROFILE</h1>					
			</div>
		</div>
	</div>

	<div class="row">
		<div class="account-head">
			<div class="col-md-2">
				<div class="img-thumb">
					<img src="{{asset('/images')}}/thumb.png" class="img-responsive" alt="">
				</div>
			</div>
			<div class="col-md-6">
				<h3>{{$user->first_name. ' '.$user->last_name}}</h3>
				<p>Lagos, Nigeria</p>
			</div>
			<div class="col-md-4 text-right">
				<p>Account Created: {{_d($user->created_at)}}</p>
			</div>
		</div>
	</div>

	<div class="user-account-details">
		<div class="row">
			<div class="col-md-12">
				<div role="tabpanel">

					<div class="col-md-4">
						<ul class="nav nav-tabs nav-pills nav-stacked" role="tablist">
							<li role="presentation" class="active">
								<a href="#basic" aria-controls="basic" role="tab" data-toggle="tab"><i class="material-icons">&#xE853;</i> BASIC INFO</a>
							</li>
							<li role="presentation">
								<a href="#account" aria-controls="account" role="tab" data-toggle="tab"><i class="material-icons">&#xE8B8;</i> ACCOUNT</a>
							</li>
							<li role="presentation">
								<a href="#billings" aria-controls="billings" role="tab" data-toggle="tab"><i class="material-icons">&#xE850;</i> BILLINGS</a>
							</li>
							<li role="presentation">
								<a href="#password" aria-controls="password" role="tab" data-toggle="tab"><i class="material-icons">&#xE88D;</i> PASSWORD</a>
							</li>
						</ul>
					</div>

					<div class="col-md-8">
						<div class="row">

							@include('components.alert')

							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="basic">
									<form action="{{url('user/profile/basic-info')}}" method="POST" role="form">

										{{csrf_field()}}

										<legend>Basic Info</legend>

										<div class="form-group">
											<label>First Name</label>
											<input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{$user->first_name}}" required>
										</div>

										<div class="form-group">
											<label>Last Name</label>
											<input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{$user->last_name}}" required>
										</div>

										<div class="form-group">
											<label>Email Address</label>
											<input type="email" class="form-control" placeholder="Email Address" value="{{$user->email}}" disabled  required>
										</div>

										<div class="form-group">
											<label>Phone</label>
											<input type="number" class="form-control" placeholder="Phone" value="{{$user->phone}}"  disabled required>
										</div>

										

										<button type="submit" class="btn btn-success">Update information</button>
									</form>
								</div>
								<div role="tabpanel" class="tab-pane" id="account">
									Account
								</div>
								<div role="tabpanel" class="tab-pane" id="billings">
									<form action="{{url('user/profile/billing')}}" method="POST" role="form">

										{{csrf_field()}}

										<legend>Billing</legend>

										<div class="form-group">
											<label>Country</label>
											<select name="country_id" class="form-control country_id" required>
												@foreach(\App\Country::get() as $row)
												@if($row->id == $user->country_id)
												<option value="{{$row->id}}" selected>{{$row->name}}</option>
												@else
												<option value="{{$row->id}}">{{$row->name}}</option>
												@endif
												@endforeach
											</select>
										</div>

										<div class="form-group">
											<label>State</label>
											<select name="state_id" class="form-control state_id" required>
												@php
													$state = \App\State::where(['id' => $user->state_id])->first();
												@endphp

												@if($state)
												<option value="{{$state->id}}" selected>{{$state->name}}</option>
												@endif

											</select>
										</div>

										<div class="form-group">
											<label>Address</label>
											<textarea name="address" class="form-control" rows="3" required>{{$user->address}}</textarea>
										</div>

										<button type="submit" class="btn btn-success">Update information</button>
									</form>
								</div>
								<div role="tabpanel" class="tab-pane" id="password">
									<form action="{{url('user/profile/password')}}" method="POST" role="form">

										{{csrf_field()}}

										<legend>Password</legend>

										<div class="form-group">
											<label>Current Password</label>
											<input type="password" name="current_password" class="form-control" placeholder="Current Password" required>
										</div>

										<div class="form-group">
											<label>New Password</label>
											<input type="password" name="new_password" class="form-control" placeholder="New Password" required>
											{!! _formError('password') !!}
										</div>

										<div class="form-group">
											<label>Retype Password</label>
											<input type="password" name="password_confirmation" class="form-control" placeholder="Retype Password" required>
										</div>

										<button type="submit" class="btn btn-success">Update information</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
@endsection