@extends('layouts.user')

@section('user-content')

<div class="account-overview">

	<div class="dash-title">
		<div class="row">
			<div class="col-md-12">
				<h1>DASHBOARD</h1>					
			</div>
		</div>
	</div>


	<div class="row">
		
		<div class="col-md-12">
			@include('components.alert')
		</div>
		<div class="col-md-6">
			<a href="javascript:;">
				<div class="account-stats">
					<div class="row">
						<div class="col-md-8">
							<i class="material-icons ic-stats">&#xE563;</i>
							<h4>Saved Businesses</h4>
						</div>
						<div class="col-md-4">
							<div class="stat-count">
								<h4>{{$saved_businesses}}</h4>
							</div>
						</div>
					</div>
					<i class="material-icons see-more">&#xE8E4;</i>
				</div>
			</a>
		</div>

		
		<div class="col-md-6">
			<a href="javascript:;">
				<div class="account-stats">
					<div class="row">
						<div class="col-md-8">
							<i class="material-icons ic-stats">&#xE8CC;</i>
							<h4>Saved Deals</h4>
						</div>
						<div class="col-md-4">
							<div class="stat-count">
								<h4>{{$saved_deals}}</h4>
							</div>
						</div>
					</div>
					<i class="material-icons see-more">&#xE8E4;</i>
				</div>
			</a>
		</div>

		
		<div class="col-md-6">
			<a href="javascript:;">
				<div class="account-stats">
					<div class="row">
						<div class="col-md-8">
							<i class="material-icons ic-stats">&#xE24F;</i>
							<h4>Saved Events</h4>
						</div>
						<div class="col-md-4">
							<div class="stat-count">
								<h4>{{$saved_events}}</h4>
							</div>
						</div>
					</div>
					<i class="material-icons see-more">&#xE8E4;</i>
				</div>
			</a>
		</div>

		
		<div class="col-md-6">
			<a href="javascript:;">
				<div class="account-stats">
					<div class="row">
						<div class="col-md-8">
							<i class="material-icons ic-stats">&#xE89A;</i>
							<h4>Orders</h4>
						</div>
						<div class="col-md-4">
							<div class="stat-count">
								<h4>{{$orders}}</h4>
							</div>
						</div>
					</div>
					<i class="material-icons see-more">&#xE8E4;</i>
				</div>
			</a>
		</div>

		
		<div class="col-md-6">
			<a href="javascript:;">
				<div class="account-stats">
					<div class="row">
						<div class="col-md-8">
							<i class="material-icons ic-stats">&#xE85D;</i>
							<h4>Quote Request</h4>
						</div>
						<div class="col-md-4">
							<div class="stat-count">
								<h4>{{$quotes}}</h4>
							</div>
						</div>
					</div>
					<i class="material-icons see-more">&#xE8E4;</i>
				</div>
			</a>
		</div>

	</div>

</div>


@endsection