@extends('layouts.user')

@section('user-content')

<div class="account-overview">

		<div class="row">

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Order #{{$order->order_reference}}</h3>
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-sm-6">
								<table class="table">
									<tr>
										<th>ORDER REFERENCE</th>
										<td>#{{$order->order_reference}}</td>
									</tr>
									<tr>
										<th>ORDER DATE</th>
										<td>{{_d($order->created_at)}}</td>
									</tr>
									<tr>
										<th>TRANSACTION REFERENCE</th> 
										<td>{{$order->transaction->reference}}</td>
									</tr>
									<tr>
										<th>TRANSACTION DATE</th> 
										<td>{{_d($order->transaction->transaction_date)}}</td>
									</tr>
									<tr>
										<th>ORDER TOTAL</th> 
										<td>{{_c(\App\Order::getTotal($order))}}</td>
									</tr>
									<tr>
										<th>STATUS</th> 
										<td>{{_badge($order->status)}}</td>
									</tr>
								</table>
							</div>

							<div class="col-sm-6">
								<table class="table">
									<tr>
										<th>CUSTOMER NAME</th>
										<td>{{$order->first_name.' '.$order->last_name}}</td>
									</tr>
									<tr>
										<th>CUSTOMER EMAIL</th>
										<td>{{$order->email}}</td>
									</tr>
									<tr>
										<th>CUSTOMER PHONE</th>
										<td>{{$order->phone}}</td>
									</tr>
									<tr>
										<th>CUSTOMER ADDRESS</th>
										<td>&nbsp;</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

					<table class="table table-striped table-bordered">

						<thead>
							<tr>
								<th>PRODUCT TYPE</th>
								<th>DESCRIPTION</th>
								<th>QUANTITY</th>
								<th>PRICE</th>
								<th>STATUS</th>
								<th>TOTAL</th>
							</tr>
						</thead>
						<tbody>

							@foreach($order->order_details as $row)

							<tr>
								<td>#{{strtoupper($row->product->type)}}</td>
								<td>{{$row->option_name}}</td>
								<td>1</td>
								<td>{{_c($row->price)}}</td>
								<td>{{_badge($row->status)}}</td>
								<td>{{_c($row->price)}}</td>
							</tr>

							@endforeach
							<tr>
								<td colspan="5">Sub Total</td>
								<td>{{_c(\App\Order::getTotal($order))}}</td>
							</tr>
							<tr>
								<td colspan="5">Total</td>
								<td><strong>{{_c(\App\Order::getTotal($order))}}</strong></td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>

	@endsection