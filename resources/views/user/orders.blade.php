@extends('layouts.user')

@section('user-content')

<div class="account-overview">

	<div class="dash-title">
		<div class="row">
			<div class="col-md-12">
				<h1>MY ORDERS</h1>					
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">

			@if($orders->count() > 0 && 1 == 2)
			<table id="datatable" class="table table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Order Reference</th>
						<th>Order Date</th>
						<th>Total</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $row)
					<tr>
						<td><a href="{{url('user/order/'.$row->order_reference)}}">{{$row->order_reference}}</a></td>
						<td>{{_d($row->created_at)}}</td>
						<td>{{_c($row->total)}}</td>
						<td>{{_badge($row->status)}}</td>
					</tr>
					@endforeach
				</tfoot>
			</table>

			@else

			<img src="{{url('public/svg/undraw_data_xmfy.svg')}}" class="text-center img img-responsive" style="width:30%; height:30%; margin:0px auto;">
			<h3 class="text-center" style="margin-top:15px;">
				Your orders will display here when you have one. <a href="{{url('/')}}">Click here</a> to start shopping now!
			</h3>

			@endif
		</div>
	</div>

</div>
@endsection