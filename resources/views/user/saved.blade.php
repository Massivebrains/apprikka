@extends('layouts.user')

@section('user-content')

<div class="account-overview">

    <div class="dash-title">
        <div class="row">
            <div class="col-md-12">
                <h1>SAVED ITEMS</h1>                   
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <h3>Saved Businesses</h3>
            <table class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Business Name</th>
                        <th>Address</th>
                        <th>Email</th>
                        <th>Phone</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($saved as $row)

                    @if($row->product->type == 'business')
                    <tr>
                        <td><a href="{{url('business/'.$row->product->slug)}}">{{$row->product->name}}</a></td>
                        <td>{{$row->product->address}}</td>
                        <td>{{$row->product->email}}</td>
                        <td>{{$row->product->phone}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>

            <h3>Saved Deals</h3>
            <table class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Deal</th>
                        <th>Description</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($saved as $row)

                    @if($row->product->type == 'deal')
                    <tr>
                        <td><a href="{{url('deal/'.$row->product->slug)}}">{{$row->product->name}}</a></td>
                        <td>{{$row->product->description}}</td>
                        <td>{{_d($row->product->start_date)}}</td>
                        <td>{{_d($row->product->end_date)}}</td>
                        <td>{{_c($row->product->regular_price)}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>

            <h3>Saved Events</h3>
            <table class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Event Name</th>
                        <th>Address</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($saved as $row)

                    @if($row->product->type == 'event')
                    <tr>
                        <td><a href="{{url('event/'.$row->product->slug)}}">{{$row->product->name}}</a></td>
                        <td>{{$row->product->address}}</td>
                        <td>{{_d($row->product->start_date)}}</td>
                        <td>{{_d($row->product->end_date)}}</td>
                        <td>{{_c($row->product->regular_price)}}</td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

</div>
@endsection