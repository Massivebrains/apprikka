@extends('layouts.user')

@section('user-content')

<div class="account-overview">

	<div class="dash-title">
		<div class="row">
			<div class="col-md-12">
				<h1>QUOTES</h1>					
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table id="datatable" class="table table-bordered" style="width:100%">
				<thead>
					<tr>
						<th>Business</th>
						<th>Quote Title</th>
						<th>Address</th>
						<th>Date</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach($quotes as $row)
					<tr>
						<td><a href="{{url('business/'.$row->business->slug)}}">{{$row->business->name}}</a></td>
						<td>{{$row->title}}</td>
						<td>{{$row->business->address}}</td>
						<td>{{_d($row->created_at)}}</td>
						<td><span class="badge">Pending</span></td>
					</tr>
					@endforeach
				</tfoot>
			</table>
		</div>
	</div>

</div>
@endsection