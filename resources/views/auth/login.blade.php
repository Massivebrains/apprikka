@extends('layouts.auth')

@section('content')



<div id="logintab">

  <form id="loginforms" method="post" action="{{url('auth/login')}}">
    {{csrf_field()}}
    <div class="row">
      <div class="col-md-12">

        <h3 class="text-center">Login<hr></h3>

        @if(session('error'))
        <p class="text-danger">{{session('error')}}</p>
        @endif

        @if(session('message'))
        <div class="alert alert-info">{{session('message')}}</div>
        @endif
        
        <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
          <input name="email" type="text" class="form-control" placeholder="Your Email" required value="{{old('email')}}">
        </div>

        <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
          <input name="password" type="password" class="form-control" placeholder="Password" required>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-success">Login</button>
          <a href="{{url('auth/forgot-password')}}" class="pull-right"><i class="fa fa-lock"></i> Forgot Password</a>
        </div>
      </div>
    </div>
  </form>


  <hr>
  <p>
    New User? <a href="{{url('auth/user-register')}}" class="link">Create Account</a>
  </p>
</div>
@endsection
