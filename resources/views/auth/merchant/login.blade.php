@extends('layouts.auth-merchant')

@section('content')

<form action="{{url('auth/login')}}" method="POST" role="form">

  {{csrf_field()}}

  @if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
  @endif

  @if(session('message'))
  <div class="alert alert-info">{{session('message')}}</div>
  @endif

  <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
    <input type="text" name="email" class="form-control" id="" placeholder="Your Email" required>
  </div>

  <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
    <input type="password" name="password" class="form-control" placeholder="Password" required>
  </div>

  <div class="form-group">
    <button type="submit" class="btn btn-success" style="margin-bottom:10px;">
      LOGIN <i class="material-icons">&#xE8E4;</i>
    </button>
    <br>
    <a href="{{url('auth/forgot-password')}}">
      <i class="fa fa-lock"></i> Forgot Password</a>
    </div>

  </form>

  @endsection