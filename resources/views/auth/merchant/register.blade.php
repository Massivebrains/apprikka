@extends('layouts.auth-merchant')

@section('content')

<form action="{{url('auth/register/merchant')}}" method="POST" role="form">

  {{csrf_field()}}
  
  @if(session('error'))
  <div class="alert alert-danger">{{session('error')}}</div>
  @endif

  @if(session('message'))
  <div class="alert alert-info">{{session('message')}}</div>
  @endif

  <div class="form-group {{$errors->has('first_name') ? 'has-error' : ''}}">
    <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{old('first_name')}}" required>
    {!! _formError($errors, 'first_name') !!}
  </div>

  <div class="form-group {{$errors->has('last_name') ? 'has-error' : ''}}">
    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{old('last_name')}}" required>
    {!! _formError($errors, 'last_name') !!}
  </div>

  <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
    <input type="email" name="email" class="form-control" placeholder="Email Address" value="{{old('email')}}" required>
    {!! _formError($errors, 'email') !!}
  </div>

  <div class="form-group {{$errors->has('phone') ? 'has-error' : ''}}">
    <input type="text" name="phone" class="form-control" placeholder="Mobile Number" value="{{old('phone')}}" required>
    {!! _formError($errors, 'phone') !!}
  </div>

  <div class="form-group {{$errors->has('country_id') ? 'has-error' : ''}}">
    <select name="country_id" class="form-control select2 country_id">
      @foreach(\App\Country::get() as $row)
      @if(old('country_id', 156) == $row->id)
      <option value="{{$row->id}}" selected>{{$row->name}}</option>
      @else
      <option value="{{$row->id}}">{{$row->name}}</option>
      @endif
      @endforeach
    </select>
    {!! _formError($errors, 'country_id') !!}
  </div>

  <div class="form-group {{$errors->has('state_id') ? 'has-error' : ''}}">
    <select name="state_id" class="form-control select2 state_id">
    </select>
    {!! _formError($errors, 'state_id') !!}
  </div>

  <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
    <input type="password" name="password" class="form-control" placeholder="Password" required>
    {!! _formError($errors, 'password') !!}
  </div>

  <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
    <input type="password" name="password_confirmation" class="form-control" placeholder="Repeat Password" required>
    {!! _formError($errors, 'password_confirmation') !!}
  </div>

  <button type="submit" class="btn btn-success">
    CONTINUE <i class="material-icons">&#xE8E4;</i>
  </button>
</form>

@endsection