@extends('layouts.auth')

@section('content')



<div id="logintab">

  <form method="post" action="{{url('auth/forgot-password')}}">

    {{csrf_field()}}

    <div class="row">

      <h3 class="text-center">Forgot Password<hr></h3>

      <div class="col-md-12">

        @if(session('message'))
        <div class="alert alert-info">{{session('message')}}</div>
        @endif
        
        <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
          <input name="email" type="email" class="form-control" placeholder="Your Email" required>
          @if($errors->has('email'))
          <div class="text-danger text-center">We do not have an account registered with <strong>{{old('email')}}</strong></div>
          @endif
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-success">Submit</button>
          <a class="pull-right" href="{{url('auth')}}">
            <i class="fa fa-user"></i> Back to Login
          </a>
        </div>

      </div>
    </div>
  </form>

  <hr>
  <p>
    New User? <a href="{{url('auth/user-register')}}" class="link">Create Account</a>
  </p>
</div>
@endsection
