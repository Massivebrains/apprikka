@extends('layouts.auth')

@section('content')



<div id="logintab">

  <form method="post" action="{{url('auth/reset-password/'.$token)}}">

    {{csrf_field()}}

    <div class="row">

      <h3 class="text-center">Reset Password<hr></h3>

      <div class="col-md-12">

        <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
          <input name="password" type="password" class="form-control" placeholder="New Password" required>
          {!! _formError($errors, 'password') !!}
        </div>

        <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
          <input name="password_confirmation" type="password" class="form-control" placeholder="confirm New Password" required>
          {{_formError($errors, 'password_confirmation')}}
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-success">Submit</button>
          <a class="pull-right" href="{{url('auth')}}">
            <i class="fa fa-user"></i> Back to Login
          </a>
        </div>

      </div>
    </div>
  </form>

  <hr>
  <p>
    New User? <a href="{{url('auth/user-register')}}" class="link">Create Account</a>
  </p>
</div>
@endsection
