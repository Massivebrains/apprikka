@extends('layouts.auth')

@section('content')



<div id="logintab">

  <form method="post" action="{{url('auth/register/user')}}">

    {{csrf_field()}}

    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center">Create Account<hr></h3>

        @if(session('error'))
        <p class="text-danger">{{session('error')}}</p>
        @endif

        @if(session('message'))
        <div class="alert alert-info">{{session('message')}}</div>
        @endif

        <div class="form-group {{$errors->has('first_name') ? 'has-error' : ''}}">
          <input name="first_name" type="text" class="form-control" placeholder="First Name" required value="{{old('first_name')}}">
          {!! _formError($errors, 'first_name') !!}
        </div>

        <div class="form-group {{$errors->has('last_name') ? 'has-error' : ''}}">
          <input name="last_name" type="text" class="form-control" placeholder="Last Name" required value="{{old('last_name')}}">
          {!! _formError($errors, 'last_name') !!}
        </div>

        <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
          <input name="email" type="text" class="form-control" placeholder="Your Email" required value="{{old('email')}}">
          {!! _formError($errors, 'email') !!}
        </div>

        <div class="form-group {{$errors->has('phone') ? 'has-error' : ''}}">
          <input name="phone" type="number" class="form-control" placeholder="Phone" required value="{{old('phone')}}">
          {!! _formError($errors, 'phone') !!}
        </div>

        <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
          <input name="password" type="password" class="form-control" placeholder="Password" required>
          {!! _formError($errors, 'password') !!}
        </div>

        <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
          <input name="password_confirmation" type="password" class="form-control" placeholder="Confirm Password" required>
          {!! _formError($errors, 'password_confirmation') !!}
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-success">Create Account</button>
          <label class="pull-right"> <a id="forgot-pass"><i class="fa fa-lock"></i> Forgot Password</a>
          </label>
        </div>
      </div>
    </div>
  </form>


  <hr>
  <p>Already have an account? <a href="{{url('auth')}}" class="link">Login</a></p>
</div>

@endsection
