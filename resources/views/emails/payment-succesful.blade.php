@extends('layouts.emails')

@section('content')

<h2>Hello There,</h2>
<p>
	A payment was successfully completed with your Apprikka account, 
	and here is the summary of the transaction:
</p>
<p>
	Amount : <strong>{{number_format($order->total, 2)}}</strong>
</p>
<p>
	Transaction Reference : <strong>{{$order->transaction->reference}}</strong>
</p>
<p>
	<a href='{{url('/auth')}}'>Click here</a>  to Login to your account for full details.
</p>

@endsection