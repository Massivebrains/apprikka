@extends('layouts.emails')

@section('content')

<h2>Reset your password</h2>
<p>You told us you forgot your password. If you really did, click here to choose a new one:</p>
<p><a href='{{url('auth/reset-password/'.$user->remember_token)}}'>Choose a New Password</a> </p>
<br>
<p>If you didn't mean to reset your password, then you can just ignore this email; your password will not change.</p>
@endsection