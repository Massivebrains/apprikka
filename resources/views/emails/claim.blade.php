@extends('layouts.emails')

@section('content')

<h2>Hello {{$product->user->first_name}},</h2>
<p>
	Claim {{$product->name}} now with Verification code <strong>{{$code}}</strong>
</p>

@endsection