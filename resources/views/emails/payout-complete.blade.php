@extends('layouts.emails')

@section('content')

<h2>Hello {{$payout->user->first_name}},</h2>
<p>
	Your payout request of the sum of {{_c($payout->amount)}} has been paid successfully. <br>
</p>
@endsection