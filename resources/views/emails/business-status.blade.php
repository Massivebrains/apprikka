@extends('layouts.emails')

@section('content')

<h2>Hello {{$product->user->first_name}},</h2>
<p>
	Your Business (<strong>{{$product->name}}</strong>) have been activated on Apprikka.</p>
<p>
	Customers can now see your business when they log on to apprikka.com
</p>
<p>
	<a href='{{url('/')}}'>Click here</a>  to Login to your account for full details.
</p>

@endsection