@extends('layouts.emails')

@section('content')

<h2>Hello {{$user->first_name}},</h2>
<p>
	You are one step away from activating your account on Apprikaa.  Click <a href='{{$url}}'>here</a> 
	to continue or copy the link below to your browser. <br><br><br>

	<small>{{$url}}</small>
</p>

@endsection