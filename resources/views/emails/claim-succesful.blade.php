@extends('layouts.emails')

@section('content')

<h2>Hello {{$product->user->first_name}},</h2>
<p>
	You have succesfully claimed {{$product->name}}.
</p>

@endsection