@extends('layouts.emails')

@section('content')

<h2>Hello {{ucfirst($quote->name)}}</h2>
<p>We are glad to inform you that your quote on <strong>{{$quote->product->name}}</strong>has been marked as completed.</p>
<p>Thank you for using Apprikaa.</p>

@endsection