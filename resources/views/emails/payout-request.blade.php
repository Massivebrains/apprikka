@extends('layouts.emails')

@section('content')

<h2>Hello {{$user->first_name}},</h2>
<p>
	A new payout request was made on your account. Find details below <br><br>

	<table>
		<tr>
			<th>Amount</th>
			<td>{{_c($payout->amount)}}</td>
		</tr>
		<tr>
			<th>Date</th>
			<td>{{_d($payout->created_at)}}</td>
		</tr>
	</table>
	<br>
<p>
	If you did not initiate this transaction please reach out to Apprikka immediately.
</p>
@endsection