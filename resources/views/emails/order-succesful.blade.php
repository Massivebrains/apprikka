@extends('layouts.emails')

@section('content')

<h2>Hello There,</h2>
<p>
	Your order <strong>{{$order->order_reference}}</strong> 
	has been successfully confirmed.
</p>
<p>
	It would be processed immediately and you will be contacted 
	via email and sms for futher updates.
</p>
<p>
	<a href='{{url('/')}}'>Click here</a>  
	to Login to your account for full details.
</p>

@endsection