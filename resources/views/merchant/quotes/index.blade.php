@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
	<thead>
		<tr>
			<th>Business</th>
			<th>Name</th>
			<th>Email Address</th>
			<th>Phone Number</th>
			<th>Date Requested</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>

		@foreach($quotes as $row)

		<tr>
			<td>
				<a href="{{url('business/'.$row->product->slug)}}" target="_blank">
					{{$row->product->name}}
				</a>
			</td>
			<td>{{$row->name}}</td>
			<td>{{$row->email}}</td>
			<td>{{$row->phone}}</td>
			<td>{{_d($row->created_at)}}</td>
			<td>{{_badge($row->status)}}</td>
			<td>
				<a href='{{url('merchant/quotes/'.$row->id.'/quote')}}' class="btn btn-xs btn-success">View Details</a>
			</td>
		</tr>

		@endforeach
	</tbody>
</table>

@endsection