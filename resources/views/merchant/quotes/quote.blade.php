@extends('layouts.merchant')

@section('content')

<div class="account-overview">

	<div class="dash-title">
		<div class="row">
			<div class="col-md-12">
				<h1>{{$quote->product->name}} <i class="material-icons ic-sm">&#xE8E5;</i></h1>
				<p class="seller">by {{$quote->product->user->first_name.' '.$quote->product->user->last_name}}</p>
				<div class="star-rating">
					<span>3.5</span> <i class="material-icons ic-sm">&#xE8D0;</i> 6 Rating
				</div>		
			</div>
		</div>
		<br>

		<div class="row">

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Quote Details</h3>
					</div>
					<div class="panel-body">
						<table class="table table-striped table-bordered">
							
							<tbody>
								<tr>
									<th>NAME</th>
									<td>{{$quote->name}}</td>
								</tr>
								<tr>
									<th>EMAIL ADDRESS</th>
									<td>{{$quote->email}}</td>
								</tr>
								<tr>
									<th>PHONE NUMBER</th>
									<td>{{$quote->phone}}</td>
								</tr>
								<tr>
									<th>DATE REQUESTED</th>
									<td>{{_d($quote->created_at)}}</td>
								</tr>
								<tr>
									<th>NEED</th>
									<td>{{$quote->title}}</td>
								</tr>
								<tr>
									<th>ABOUT</th>
									<td>{{$quote->description}}</td>
								</tr>
								<tr>
									<td colspan="2">
										<a href="{{url('merchant/quotes/'.$quote->id.'/status')}}" style="color:#fff" class="btn btn-xs btn-success confirm">Mark as Completed</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

	</div>

	@endsection