@extends('layouts.merchant')

@section('content')

<div class="row">

    <div class="col-md-4">
        <div class="stats">
            <div class="row">
                <div class="col-md-3">
                    <i class="material-icons">&#xE563;</i>
                </div>
                <div class="col-md-9">
                    <h5>ACTIVE BUSS.</h5>
                    <h3>{{$active_businesses}}</h3>
                </div>
            </div>
            <div class="more-details">
                <a href="{{url('merchant/products/business')}}" class="btn btn-default">VIEW ALL <i class="material-icons">&#xE315;</i></a>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="stats">
            <div class="row">
                <div class="col-md-3">
                    <i class="material-icons">&#xE563;</i>
                </div>
                <div class="col-md-9">
                    <h5>PENDING BUSS.</h5>
                    <h3>{{$businesses - $active_businesses}}</h3>
                </div>
            </div>
            <div class="more-details">
                <a href="{{url('merchant/products/business')}}" class="btn btn-default">VIEW ALL <i class="material-icons">&#xE315;</i></a>
            </div>
        </div>
    </div>


    <div class="col-md-4">
        <div class="stats">
            <div class="row">
                <div class="col-md-3">
                    <i class="material-icons">&#xE8CC;</i>
                </div>
                <div class="col-md-9">
                    <h5>ACTIVE DEALS</h5>
                    <h3>{{$active_deals}}</h3>
                </div>
            </div>
            <div class="more-details">
                <a href="{{url('merchant/products/deal')}}" class="btn btn-default">VIEW ALL <i class="material-icons">&#xE315;</i></a>
            </div>
        </div>
    </div>


</div>

<div class="row">
    
    <div class="col-md-4">
    <div class="stats">
        <div class="row">
            <div class="col-md-3">
                <i class="material-icons">&#xE24F;</i>
            </div>
            <div class="col-md-9">
                <h5>ACTIVE EVENTS</h5>
                <h3>{{$active_events}}</h3>
            </div>
        </div>
        <div class="more-details">
            <a href="{{url('merchant/products/event')}}" class="btn btn-default">VIEW ALL <i class="material-icons">&#xE315;</i></a>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="stats">
        <div class="row">
            <div class="col-md-3">
                <i class="material-icons">&#xE89A;</i>
            </div>
            <div class="col-md-9">
                <h5>ACCOUNT BALANCE</h5>
                <h3>0</h3>
            </div>
        </div>
        <div class="more-details">
            <a href="javascript:;" class="btn btn-default">VIEW ALL <i class="material-icons">&#xE315;</i></a>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="stats">
        <div class="row">
            <div class="col-md-3">
                <i class="material-icons">&#xE8D0;</i>
            </div>
            <div class="col-md-9">
                <h5>ALL REVIEWS</h5>
                <h3>{{$reviews}}</h3>
            </div>
        </div>
        <div class="more-details">
            <a href="javascript:;" class="btn btn-default">VIEW ALL <i class="material-icons">&#xE315;</i></a>
        </div>
    </div>
</div>

</div>


@endsection