@extends('layouts.frontend')

@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('/dropify/css/dropify.min.css')}}">

<div class="page-filers" style="background:#fff;">
    <div class="container" style="padding:20px; margin-top:30px">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size:35px; color:#12ad2c; font-weight:bold; text-align:center">3. Upload Identity Credentials</h1>
                <hr>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row" style="margin-bottom:200px;">

        <div class="col-md-12">
            <form action="{{url('merchant/verify/upload')}}" method="POST" role="form" enctype="multipart/form-data" id="logo-form">

                {{csrf_field()}}

                @if(session('error'))
                <div class="alert alert-danger">
                    {{session('error')}}
                </div>
                @endif
                
                <div class="row" style="padding:10px; background: #f8fafc;">
                    <div class="form-group col-sm-6">
                        <div class="form-group">
                            <label style="font-size:18px; font-weight: bold; margin-bottom:5px;">Upload Valid ID</label>
                            <input type="file" name="document" class="dropify" required>
                            <small style="font-size:12px;">Valid ID can be a scanned copy of either Drivers License, National ID, International Passport, Or any valid means of ID.</small>
                        </div>
                    </div>

                    <div class="form-group col-sm-6">
                        <div class="form-group">
                            <label style="font-size:18px; font-weight: bold; margin-bottom:5px;">Upload Your Profile Picture</label>
                            <input type="file" name="photo" class="dropify">
                            <small style="font-size:12px;">Your profile picture will appear on your dashboard and profile page.</small>
                        </div>
                    </div>

                    <div class="col-md-12" style="display:flex; justify-content: center; align-self:center;">
                        <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-upload"></i> UPLOAD</button>
                    </div>

                </div>
            </form>
        </div>
    </div>


</div>

@endsection

@push('script')
<script type="text/javascript" src="{{asset('/dropify/js/dropify.min.js')}}"></script>
<script type="text/javascript">
 $(() => {

   $('.dropify').dropify();
   
})

</script>
@endpush
