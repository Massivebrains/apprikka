@extends('layouts.frontend')

@section('content')


<div class="page-filers" style="background:#fff;">
    <div class="container" style="padding:20px; margin-top:30px">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size:35px; color:#12ad2c; font-weight:bold; text-align:center">5. Complete</h1>

                <hr>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <img src="{{asset('/svg/undraw_Security_on_s9ym.svg')}}" class="img img-responsive svg"><br>

            <h1 style="text-align:center; font-size:20px; padding:10px;">Verification process complete.</h1>
        </div>

        <div class="col-md-12" style="display:flex; justify-content: center; align-self:center; margin-bottom:200px">
            <a href="{{url('merchant')}}" class="btn btn-success btn-lg">DASHBOARD</a>
        </div>
    </div>


</div>

@endsection
