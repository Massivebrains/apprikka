@extends('layouts.frontend')

@section('content')

<div class="page-filers" style="background:#fff;">
    <div class="container" style="padding:20px; margin-top:30px">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size:35px; color:#12ad2c; font-weight:bold; text-align:center">4. Verify Bank Account</h1>
                <hr>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row" style="margin-bottom:200px;">

        <div class="col-md-12">
            
            <form action="{{url('merchant/verify/bank')}}" method="POST" role="form" enctype="multipart/form-data" id="logo-form">

                {{csrf_field()}}

                <div class="row" style="padding:10px; background: #f8fafc;">
                    <div class="form-group col-sm-4">
                        <div class="form-group">
                            <label style="font-size:18px; font-weight: bold; margin-bottom:5px;">Bank</label>
                            <input type="text" name="bank" class="form-control" placeholder="Your bank name" required>
                        </div>
                    </div>

                    <div class="form-group col-sm-4">
                        <div class="form-group">
                            <label style="font-size:18px; font-weight: bold; margin-bottom:5px;">Account Number</label>
                            <input type="number" name="account_number" class="form-control" placeholder="Your bank account number" required>
                        </div>
                    </div>

                    <div class="form-group col-sm-4">
                        <div class="form-group">
                            <label style="font-size:18px; font-weight: bold; margin-bottom:5px;">Account Name</label>
                            <input type="text" name="account_name" class="form-control" placeholder="Your bank account name" required>
                        </div>
                    </div>

                    <div class="col-md-12" style="display:flex; justify-content: center; align-self:center;">
                        <button type="submit" class="btn btn-success btn-lg">COMPLETE</button>
                    </div>

                </div>
            </form>
        </div>
    </div>


</div>

@endsection