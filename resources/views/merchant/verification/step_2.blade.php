@extends('layouts.frontend')

@section('content')


<div class="page-filers" style="background:#fff;">
    <div class="container" style="padding:20px; margin-top:30px">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size:35px; color:#12ad2c; font-weight:bold; text-align:center">2. Orientation Guide</h1>
                <hr>
            </div>
        </div>
    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <img src="{{asset('/svg/undraw_user_flow_vr6w.svg')}}" class="img img-responsive svg"><br>

            @for($i = 0; $i<=10; $i++)
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p><br><br>
            @endfor

        </div>

        <div class="col-md-12" style="display:flex; justify-content: center; align-self:center; margin-bottom:200px">
            <a href="{{url('merchant/verify/orientation')}}" class="btn btn-success btn-lg">CONTINUE</a>
        </div>
    </div>


</div>

@endsection
