@extends('layouts.merchant')

@section('content')

<div role="tabpanel">
	
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="javascript:;">BASIC INFO</a>
		</li>
		<li role="presentation" class="active">
			<a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">Group DETAILS</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">GALLERY</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">CONTACT</a>
		</li>
	</ul>

	
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="basic-info">
			
			<form action="{{url('merchant/product/group-details/'.$product->id)}}" method="POST" role="form">

				{{csrf_field()}}

				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{$errors->has('website') ? 'has-error' : ''}}">
							<label for="">Group Website URL:</label>
							<input type="url" class="form-control" name="website" value="{{old('website', $product->website)}}">
							{!! _formError($errors, 'website') !!}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
							<label for="">Group Description:</label>
							<textarea name="description" class="form-control textarea">{{old('description', $product->description)}}</textarea>
							{!! _formError($errors, 'description') !!}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Tags:</label>
							<input type="text" name="tags" class="form-control tags">
						</div>
					</div>
				</div>

				<hr>
				<button type="submit" class="btn btn-success">Submit and Continue</button>
			</form>
			
		</div>
	</div>
</div>

@endsection