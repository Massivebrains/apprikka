@extends('layouts.merchant')

@section('content')

<div role="tabpanel">
    
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">BASIC INFO</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">GROUP DETAILS</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">GALLERY</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">CONTACT</a>
        </li>
    </ul>

    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="basic-info">
            
            <form action="{{url('merchant/product/basic-info/'.$product->id.'/group')}}" method="POST" role="form">

                {{csrf_field()}}
                
                @if(Auth::user()->type == 'admin')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('user_id') ? 'has-error' : ''}}">
                            <label>Merchant:</label>
                            <select name="user_id" class="form-control select2" required>
                                @php 
                                    $user_id = old('user_id', $product->user_id);
                                @endphp

                                @foreach($merchants as $row)
                                    @if($user_id == $row->id)
                                        <option value="{{$row->id}}" selected>{{$row->first_name.' '.$row->last_name}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{$row->first_name.' '.$row->last_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            {!! _formError($errors, 'user_id') !!}
                        </div>
                    </div>
                </div>
                @endif
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                            <label>Group Name:</label>
                            <input type="text" name="name" class="form-control" value="{{old('name', $product->name)}}" required>
                            {!! _formError($errors, 'name') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Group Category:</label>
                            <select name="category_id" class="form-control select2 category_id" required>
                                <option>--select--</option>
                                @php 
                                    $category_id = old('category_id', $product->category_id);
                                @endphp

                                @foreach($categories as $row)
                                    @if($category_id == $row->id)
                                        <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                    @else
                                        <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            {!! _formError($errors, 'category_id') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Group Sub Category:</label>
                            <select name="subcategory_id" class="form-control select2 subcategory_id">
                                @php 
                                $subcategory_id = old('subcategory_id', $product->subcategory_id);
                                @endphp

                                @foreach($subcategories as $row)
                                @if($subcategory_id == $row->id)
                                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                @else
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endif
                                @endforeach
                                
                            </select>
                            {!! _formError($errors, 'subcategory_id') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('place_id') ? 'has-error' : ''}}">
                            <label>Land Mark:</label>
                            <select name="place_id" class="form-control place" required>
                                @if($product->place_id != '')
                                <option value="{{$product->place->id}}" selected>{{$product->place->address}}</option>
                                @endif
                            </select>
                            {!! _formError($errors, 'place_id') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('address') ? 'has-error' : ''}}">
                            <label>Group Address:</label>
                            <input type="text" name="address" class="form-control" value="{{old('address', $product->address)}}">
                            {!! _formError($errors, 'address') !!}
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('country_id') ? 'has-error' : ''}}">
                            <label> Country:</label>
                            <select name="country_id" class="form-control select2 country_id">
                                @foreach(DB::table('countries')->get() as $row)
                                @if(old('country_id', $product->country_id) == $row->id)
                                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                @else
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            {!! _formError($errors, 'country_id') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('state_id') ? 'has-error' : ''}}">
                            <label> State:</label>
                            <select name="state_id" class="form-control select2 state_id">
                                @php 
                                    $state = DB::table('states')->where(['id' => old('state_id', $product->state_id)])->first();
                                @endphp 
                                @if($state)
                                    <option value="{{$state->id}}" selected>{{$state->name}}</option>
                                @else
                                <option>--select country--</option>
                                @endif
                            </select>
                            {!! _formError($errors, 'state_id') !!}
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-success">Save and Continue</button>

            </form>
            
        </div>
    </div>
</div>

@endsection



