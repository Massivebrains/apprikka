@extends('layouts.merchant')

@section('content')

<div role="tabpanel">
	
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="javascript:;">BASIC INFO</a>
		</li>
		<li role="presentation">
            <a href="javascript:;">TICKETS</a>
        </li>
		<li role="presentation">
			<a href="javascript:;">GALLERY</a>
		</li>
		<li role="presentation" class="active">
			<a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">CONTACT</a>
		</li>
	</ul>

	
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="basic-info">
			
			<form action="{{url('merchant/product/contact/'.$product->id)}}" method="POST" role="form">

				{{csrf_field()}}
				
				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{$errors->has('personnel') ? 'has-error' : ''}}">
							<label>Contact Personnel:</label>
							<input type="text" class="form-control" name="personnel" value="{{old('personnel', $product->personnel)}}">
							{!! _formError($errors, 'personnel') !!}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
							<label>Contact Email:</label>
							<input type="email" class="form-control" name="email" value="{{old('email', $product->email)}}">
							{!! _formError($errors, 'email') !!}
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group {{$errors->has('phone') ? 'has-error' : ''}}">
							<label>Contact Phone:</label>
							<input type="number" class="form-control" name="phone" value="{{old('phone', $product->phone)}}">
							{!! _formError($errors, 'phone') !!}
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-success">Submit and Save</button>
			</form>
			
		</div>
	</div>
</div>

@endsection
