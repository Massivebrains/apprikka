<li id="option-{{$option->id}}">
    <div class="col-md-8">
        <div class="radio">
            <label style="width:50%">
                <input type="radio" checked>
                <h4 class="tip editable" data-url="{{url('merchant/ajax/option/update/name/'.$option->id)}}" title="Click/Tap on me to edit">
                   {{$option->name}}
                </h4>
                <a href="javascript:;" onclick="removeEventOption({{$option->id}})">Remove Ticket</a>
            </label>
        </div>
    </div>
    <div class="col-md-2 text-right" style="cursor:pointer">
        <p><span class="badge tip" title="Set quantity to zero to mark ticket as unlimited sales.">Quantity Avaliable</span></p>
        <h4 class="tip editable" title="Click/Tap on me to edit" data-url="{{url('merchant/ajax/option/update/quantity/'.$option->id)}}">
            {{$option->quantity ?? 0}}
        </h4>
    </div>
    <div class="col-md-2 text-right" style="cursor:pointer">
        <p><span class="badge tip" title="Set price to zero to mark ticket as free">Ticket Price</span></p>
        <h4 class="tip editable" title="Click/Tap on me to edit" data-url="{{url('merchant/ajax/option/update/price/'.$option->id)}}">
            {{$option->price ?? 0}}
        </h4>
    </div>
</li>