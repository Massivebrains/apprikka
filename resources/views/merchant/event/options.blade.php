@extends('layouts.merchant')

@section('content')

<div role="tabpanel">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation">
            <a href="javascript:;">BASIC INFO</a>
        </li>
        <li role="presentation" class="active">
            <a href="javascript:;">TICKETS</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">GALLERY</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">CONTACT</a>
        </li>
    </ul>

    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="basic-info">

            <div class="row">
                <div class="deal-side-detail" style="background:#fff">

                    <div class="all-deal-details">
                        <div class="detail-sec">
                            <div class="star-rating pull-right" style="cursor:pointer" onclick="addOption()">
                                <span>Add more tickets</span>
                            </div>
                        </div>

                        <div class="detail-sec">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="deals-listed">
                                        @foreach($options as $row)

                                        @component('merchant.event.option', ['event' => $product, 'option' => $row])
                                        @endcomponent

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="buy-deal-btn">
                            <a href="{{url('merchant/product/gallery/'.$product->id)}}" class="btn btn-success">Save and Continue</a>
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">

    addOption = () => {

        $.get('{{url('merchant/ajax/option/add/'.$product->id)}}', response => {

            $('.deals-listed').append(response)
            $('.tip').tooltipster({theme: 'tooltipster-shadow'})
            $('.editable').jinplace()
        })
    }

    removeEventOption = option_id => {

        if(!confirm('Are you sure?'))
            return;
        
        $.post('{{url('merchant/ajax/option/remove')}}', {id : option_id}, response => {

            if(response.status == true){

                $('#option-'+option_id).remove();
            }
        })
    }

    @if(count($options) < 1)

    addOption()

    @endif

</script>

@endsection


