@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Deal</th>
            <th>Dates</th>
            <th>Pricings</th>
            <th>Status</th>
            <th>Featured</th>
            <th style="width: 40% !important">Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $row)
        <tr>
            <td style="width:35%"><a href="{{url('deal/'.$row->slug)}}" target="_blank">{{str_limit($row->name, 50, '...')}}</a></td>
            <td><span class="badge">{{_d($row->start_date). ' - '._d($row->end_date)}}</span></td>
            <td style="width:25%"><span class="badge regular">{{_c($row->regular_price)}}</span> <span class="badge sales">{{_c($row->sale_price)}}</span></td>
            <td>{{_badge($row->status)}}</td>
            <td>{{$row->featured == 1 ? 'YES' : 'NO'}}</td>
            <td>
                
                <div class="dropdown">
                    <button class="btn btn-success btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Actions
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">

                            <li><a href="{{url('merchant/product/basic-info/'.$row->id)}}">View Details</a></li>
                            
                            @if(Auth::user()->type == 'admin')
                            
                            <li>
                                <a href="{{url('merchant/product/status/'.$row->id)}}" class="confirm">
                                    {{$row->status == 'active' ? 'Unpublish' : 'Publish'}}
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/product/featured/'.$row->id)}}" class="confirm">
                                    {{$row->featured == 1 ? 'Unmark as Featured' : 'Mark as Featured'}}
                                </a>
                            </li>

                            @endif
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    @endsection