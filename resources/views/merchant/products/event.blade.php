@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Event</th>
            <th>Date</th>
            <th>Pricing</th>
            <th>Tickets</th>
            <th>Status</th>
            <th>Featured</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $row)
        <tr>
            <td style="width:35%">
                <a href="{{url('event/'.$row->slug)}}" target="_blank">
                    {{str_limit($row->name, 50, '...')}}
                </a>
            </td>
            <td style="width:15%">{{_d($row->start_date)}}</td>
            <td>{{_badge($row->options->sum('price') > 0 ? 'Paid' : 'Free')}}</td>
            <td>{{$row->options->count().' '.str_plural('ticket', $row->options->count())}}</td>
            <td>{{_badge($row->status)}}</td>
            <td>{{$row->featured == 1 ? 'YES' : 'NO'}}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-success btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Actions
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">

                            <li><a href="{{url('merchant/product/basic-info/'.$row->id)}}">View Details</a></li>
                            
                            @if(Auth::user()->type == 'admin')
                            
                            <li>
                                <a href="{{url('merchant/product/status/'.$row->id)}}" class="confirm">
                                    {{$row->status == 'active' ? 'Unpublish' : 'Publish'}}
                                </a>
                            </li>
                            <li>
                                <a href="{{url('merchant/product/featured/'.$row->id)}}" class="confirm">
                                    {{$row->featured == 1 ? 'Unmark as Featured' : 'Mark as Featured'}}
                                </a>
                            </li>

                            @endif
                        </ul>
                    </div>
            </td>
        </tr>
        @endforeach
        
    </tfoot>
</table>

@endsection