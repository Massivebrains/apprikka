@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Type</th>
            <th>Value</th>
            <th>Status</th>
            <th>Used</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

        @foreach($coupons as $row)

        <tr>
            <td>{{$row->name}}</td>
            <td>{{$row->code}}</td>
            <td>{{strtoupper($row->type)}}</td>
            <td>{{number_format($row->value)}}</td>
            <td>{{_badge($row->status)}}</td>
            <td>{{$row->used == 1 ? 'YES' : 'NO'}}</td>

            <td>
                <div class="dropdown">
                    <button class="btn btn-success btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Actions
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('merchant/coupons/coupon/'.$row->id)}}">View Details</a></li>
                        </ul>
                    </div>
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>

    @endsection