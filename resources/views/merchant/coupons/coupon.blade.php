@extends('layouts.merchant')

@section('content')

<form action="{{url('merchant/coupons/save-coupon/'.$coupon->id)}}" method="POST" role="form">

    {{csrf_field()}}

    @if(Auth::user()->type == 'admin')

    <div class="row">
        <div class="col-md-12">
            <div class="form-group {{$errors->has('merchant_id') ? 'has-error' : ''}}">
                <label>Merchant:</label>
                <select name="merchant_id" class="form-control select2" required>
                    @php 
                    $merchant_id = old('merchant_id', $coupon->merchant_id);
                    @endphp

                    <option>-- Avaliable to any Merchant Product --</option>
                    @foreach($merchants as $row)
                    @if($merchant_id == $row->id)
                    <option value="{{$row->id}}" selected>{{$row->name}}</option>
                    @else
                    <option value="{{$row->id}}">{{$row->name}}</option>
                    @endif
                    @endforeach
                </select>
                {!! _formError($errors, 'merchant_id') !!}
            </div>
        </div>
    </div>

    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="form-group {{$errors->has('user_id') ? 'has-error' : ''}}">
                <label>Customer:</label>
                <select name="user_id" class="form-control select2" required>
                    @php 
                    $user_id = old('user_id', $coupon->user_id);
                    @endphp

                    <option>-- Avaliable to any Customers --</option>
                    @foreach($users as $row)
                    @if($user_id == $row->id)
                    <option value="{{$row->id}}" selected>{{$row->name}}</option>
                    @else
                    <option value="{{$row->id}}">{{$row->name}}</option>
                    @endif
                    @endforeach
                </select>
                {!! _formError($errors, 'user_id') !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <label>Coupon Name</label>
            <input type="text" name="name" class="form-control" value="{{old('name', $coupon->name)}}" placeholder="Enter a description for this coupon" required>
            {!! _formError($errors, 'name') !!}
        </div>
        <div class="col-md-6">
            <label>Coupon Code</label>
            <input type="text" name="code" class="form-control" value="{{old('code', $coupon->code)}}" placeholder="Coupon code" required>
            {!! _formError($errors, 'code') !!}
        </div>
    </div>


    <div class="row" style="margin-top:15px;">

        <div class="form-group col-md-6 {{$errors->has('start_date') ? 'has-error' : ''}}">
            <label>Start Date:</label>
            <div class="input-group">
                <input type="text" name="start_date" class="form-control date" value="{{old('start_date', $coupon->start_date)}}">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            {!! _formError($errors, 'start_date') !!}
        </div>

        <div class="form-group col-md-6 {{$errors->has('end_date') ? 'has-error' : ''}}">
            <label>End Date:</label>
            <div class="input-group">
                <input type="text" name="end_date" class="form-control date" value="{{old('end_date', $coupon->end_date)}}">
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            {!! _formError($errors, 'end_date') !!}
        </div>
    </div>

    <div class="row">
        <div class="form-contorl col-md-4">
            <label>Type</label>
            <select name="type" class="form-control" required>
                <option value="percent">Give discount in percentage</option>
                <option value="flat">Flat Rate</option>
            </select>
        </div>

        <div class="form-group col-md-4 {{$errors->has('value') ? 'has-error' : ''}}">
            <label>Coupon Value</label>
            <input type="number" name="value" class="form-control" value="{{old('value', $coupon->value)}}" placeholder="Enter Amount/Percentage value" step="0.1" required>
            {!! _formError($errors, 'value') !!}
        </div>

        <div class="form-contorl col-md-4">
            <label>Status</label>
            <select name="status" class="form-control" required>
                @if($coupon->status == 'inactive')
                <option value="active">Active</option>
                <option value="inactive" selected>Inactive</option>
                @else
                <option value="active">Active</option>
                <option value="inactive">Inactive</option>
                @endif
            </select>
        </div>

    </div>

    <button type="submit" class="btn btn-success">Submit</button>

</form>

@endsection


