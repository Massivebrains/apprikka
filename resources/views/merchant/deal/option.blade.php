<li id="option-{{$option->id}}">
    <div class="col-md-10">
        <div class="radio">
            <label style="width:50%">
                <input type="radio" checked>
                <h4 class="tip editable" data-url="{{url('merchant/ajax/option/update/name/'.$option->id)}}" title="Click/Tap on me to edit">
                   {{$option->name}}
                </h4>
                <a href="javascript:;" onclick="removeDealOption({{$option->id}})">Remove Option</a>
            </label>
        </div>
    </div>
    <div class="col-md-2 text-right" style="cursor:pointer">
        <p><span class="badge">Option Price</span></p>
        <h4 class="tip editable" title="Click/Tap on me to edit" data-url="{{url('merchant/ajax/option/update/price/'.$option->id)}}">
            {{number_format($option->price > 0 ? $option->price : $product->sale_price)}}
        </h4>
    </div>
</li>