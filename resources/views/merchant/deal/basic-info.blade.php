@extends('layouts.merchant')

@section('content')

<div role="tabpanel">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">BASIC INFO</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">OPTIONS</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">GALLERY</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">CONTACT</a>
        </li>
    </ul>


    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="basic-info">

            <form action="{{url('merchant/product/basic-info/'.$product->id.'/deal')}}" method="POST" role="form">

                {{csrf_field()}}

                @if(Auth::user()->type == 'admin')
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('user_id') ? 'has-error' : ''}}">
                            <label>Merchant:</label>
                            <select name="user_id" class="form-control select2" required>
                                @php 
                                $user_id = old('user_id', $product->user_id);
                                @endphp

                                @foreach($merchants as $row)
                                @if($user_id == $row->id)
                                <option value="{{$row->id}}" selected>{{$row->first_name.' '.$row->last_name}}</option>
                                @else
                                <option value="{{$row->id}}">{{$row->first_name.' '.$row->last_name}}</option>
                                @endif
                                @endforeach
                            </select>
                            {!! _formError($errors, 'user_id') !!}
                        </div>
                    </div>
                </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                            <label>Deal Name:</label>
                            <input type="text" name="name" class="form-control" value="{{old('name', $product->name)}}" required>
                            {!! _formError($errors, 'name') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('regular_price') ? 'has-error' : ''}}">
                            <label>Default Price:</label>
                            <input type="number" name="regular_price" class="form-control" value="{{old('regular_price', $product->regular_price)}}" placeholder="0">
                            {!! _formError($errors, 'regular_price') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('sale_price') ? 'has-error' : ''}}">
                            <label>Sale/Discounted Price:</label>
                            <input type="number" name="sale_price" class="form-control" value="{{old('sale_price', $product->sale_price)}}" placeholder="0">
                            {!! _formError($errors, 'sale_price') !!}
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
                            <label>Description:</label>
                            <textarea name="description" class="form-control textarea" rows="3">{{old('description', $product->description)}}</textarea>
                            {!! _formError($errors, 'description') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Category:</label>
                            <select name="category_id" class="form-control select2 category_id" required>
                                <option>--select--</option>
                                @php 
                                $category_id = old('category_id', $product->category_id);
                                @endphp

                                @foreach($categories as $row)
                                @if($category_id == $row->id)
                                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                @else
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            {!! _formError($errors, 'category_id') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sub Category:</label>
                            <select name="subcategory_id" class="form-control select2 subcategory_id">
                                @php 
                                $subcategory_id = old('subcategory_id', $product->subcategory_id);
                                @endphp

                                @foreach($subcategories as $row)
                                @if($subcategory_id == $row->id)
                                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                @else
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endif
                                @endforeach

                            </select>
                            {!! _formError($errors, 'subcategory_id') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('country_id') ? 'has-error' : ''}}">
                            <label> Country:</label>
                            <select name="country_id" class="form-control select2 country_id">
                                @foreach(DB::table('countries')->get() as $row)
                                @if(old('country_id', $product->country_id) == $row->id)
                                <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                @else
                                <option value="{{$row->id}}">{{$row->name}}</option>
                                @endif
                                @endforeach
                            </select>
                            {!! _formError($errors, 'country_id') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('state_id') ? 'has-error' : ''}}">
                            <label> State:</label>
                            <select name="state_id" class="form-control select2 state_id">
                                @php 
                                $state = DB::table('states')->where(['id' => old('state_id', $product->state_id)])->first();
                                @endphp 
                                @if($state)
                                <option value="{{$state->id}}" selected>{{$state->name}}</option>
                                @else
                                <option>--select country--</option>
                                @endif
                            </select>
                            {!! _formError($errors, 'state_id') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('place_id') ? 'has-error' : ''}}">
                            <label>Your Location:</label>
                            <select name="place_id" class="form-control place" required>
                                @if($product->place_id != '')
                                <option value="{{$product->place->id}}" selected>{{$product->place->address}}</option>
                                @endif
                            </select>
                            {!! _formError($errors, 'place_id') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tags:</label>
                            <input type="text" name="tags" class="form-control tags">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Deal Start Date and Time</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="start_date" class="start_datetime">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Deal End Date and Time</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="end_date" class="end_datetime">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-success">Save and Continue</button>

            </form>

        </div>
    </div>
</div>

@endsection
