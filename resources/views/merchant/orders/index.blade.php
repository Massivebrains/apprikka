@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
	<thead>
		<tr>
			<th>Reference</th>
			<th>Customer Name</th>
			<th>Customer Phone</th>
			<th>Date</th>
			<th>Total</th>
			<th>Status</th>
			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
		@foreach($orders as $row)

		<tr>
			<td>{{$row->order_reference}}</td>
			<td>{{$row->first_name.' '.$row->last_name}}</td>
			<td>{{$row->phone}}</td>
			<td>{{_d($row->created_at)}}</td>
			<td>{{_c($row->total)}}</td>
			<td>{{_badge($row->status)}}</td>
			<td>
				<a href='{{url('merchant/order/'.$row->order_reference)}}' class="btn btn-xs btn-success">
					View Details
				</a>
			</td>
		</tr>

		@endforeach
	</tbody>
</table>

@endsection