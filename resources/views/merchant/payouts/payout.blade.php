@extends('layouts.merchant')

@section('content')

<div class="account-overview">

  <div class="dash-title">

    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Payout - {{$payout->reference}}<a href="#" class="pull-right">Print</a>
            </h3>
          </div>
          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
                <table class="table">
                  <tr>
                    <th>REFERENCE</th>
                    <td>{{$payout->reference}}</td>
                  </tr>
                  <tr>
                    <th>PAYOUT DATE</th>
                    <td>{{_d($payout->created_at)}}</td>
                  </tr>
                  <tr>
                    <th>DATE PAID</th> 
                    <td>{{_d($payout->payment_date)}}</td>
                  </tr>
                  <tr>
                    <th>AMOUNT</th> 
                    <td>{{_c($payout->amount)}}</td>
                  </tr>
                  <tr>
                    <th>DESCRIPTION</th> 
                    <td>{{$payout->description}}</td>
                  </tr>
                  <tr>
                    <th>STATUS</th> 
                    <td>{{_badge($payout->status)}}</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection