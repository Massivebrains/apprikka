@extends('layouts.merchant')

@section('content')

<div class="col-md-12">
    <div class="request-payout" style="background:#fff">

        <form action="{{url('merchant/payouts/request')}}" method="POST" role="form">
            {{csrf_field()}}

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="stats">
                        <div class="row">
                            <div class="col-md-3">
                                <i class="material-icons">&#xE41D;</i>
                            </div>
                            <div class="col-md-9">
                                <h5>ACCOUNT BALANCE</h5>
                                <h3>{{_c($balance)}}</h3>
                            </div>
                        </div>
                        <div class="more-details">
                            <a href="javascript:;" class="btn btn-default">MINIMUM PAYOUT: {{_c(5000)}} </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Amount</label>
                <input type="number" name="amount" class="form-control" placeholder="0.0" required>
                {!! _formError($errors, 'amount') !!}
            </div>

            <div class="form-group">
                <label>Comments (Optional)</label>
                <textarea name="description" class="form-control" rows="3"></textarea>
            </div>

            
            <button type="submit" class="btn btn-success">Request</button>

        </form>
    </div>
</div>

@endsection