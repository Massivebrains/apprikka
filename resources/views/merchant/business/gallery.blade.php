@extends('layouts.merchant')

@section('content')

<div role="tabpanel">
	
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="javascript:;">BASIC INFO</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">OPTIONS</a>
		</li>
		<li role="presentation" class="active">
			<a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">GALLERY</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">CONTACT</a>
		</li> 
	</ul>

	
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="basic-info">

			<form action="{{url('merchant/product/gallery/'.$product->id)}}" method="POST" role="form" enctype="multipart/form-data" id="logo-form">
				
				{{csrf_field()}}
				
				<div class="row">
					<div class="form-group col-sm-6">
						<div class="form-group">
							<label>Upload Banner</label>
							<input type="file" name="banner" class="dropify">
						</div>

					</div>

					<div class="form-group col-sm-6">
						<div class="form-group">
							<label>Upload Logo</label>
							<input type="file" name="logo" class="dropify">
						</div>
					</div>
				</div>
			</form>

			<hr>

			<div class="form-group">
				<label for="">Upload Gallery:</label>
				<form action="{{url('merchant/product/upload/'.$product->id)}}" id="dropzone" class="dropzone">

					{{csrf_field()}}

					<input type="hidden" name="product_id" value="{{$product->id}}">
				</form>
			</div>

			<button type="submit" class="btn btn-success" onclick="$('#logo-form').submit()">Submit and Continue</button>
			
		</div>
	</div>
</div>

@endsection

