@extends('layouts.merchant')

@section('content')

<div role="tabpanel">
	
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="javascript:;">BASIC INFO</a>
		</li>
		<li role="presentation" class="active">
			<a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">BUSINESS DETAILS</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">GALLERY</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">CONTACT</a>
		</li>
	</ul>

	
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="basic-info">
			
			<form action="{{url('merchant/product/business-details/'.$product->id)}}" method="POST" role="form">

				{{csrf_field()}}

				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{$errors->has('website') ? 'has-error' : ''}}">
							<label for="">Business Website URL:</label>
							<input type="url" class="form-control" name="website" value="{{old('website', $product->website)}}">
							{!! _formError($errors, 'website') !!}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
							<label for="">Business Description:</label>
							<textarea name="description" class="form-control textarea">{{old('description', $product->description)}}</textarea>
							{!! _formError($errors, 'description') !!}
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label>Tags:</label>
							<input type="text" name="tags" class="form-control tags">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<label>Business Hours</label>
						<hr>
						<div class="form-group">
							<ul class="opening-hourss">

								@foreach($business_hours as $row)
								<li>
									<div class="col-md-2">
										{{$row->day}}:
										<input type="hidden" name="days[{{$row->id}}][id]" value="{{$row->id}}">
									</div>
									<div class="col-md-6">
										<div class="col-md-6">
											<p>Opens</p>
											<div class="input-group">
												<input type="text" class="form-control time opens {{$row->id}}" name="days[{{$row->id}}][opens]" value="{{$row->opens}}" {{$row->status == 'inactive' ? 'disabled' : ''}}>
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
										</div>
										<div class="col-md-6">
											<p>Closes</p>
											<div class="input-group">
												<input type="text" class="form-control time closes {{$row->id}}" name="days[{{$row->id}}][closes]" value="{{$row->closes}}" {{$row->status == 'inactive' ? 'disabled' : ''}}>
												<span class="input-group-addon">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<p>&nbsp;</p>
										<input type="hidden" name="days[{{$row->id}}][dayoff]" class="dayoff {{$row->id}}" value="{{$row->status}}">
										<button type="button" class="btn {{$row->status == 'inactive' ? 'btn-danger' : 'btn-default'}} off-button {{$row->id}}" onclick="off('{{$row->id}}')">Day {{$row->status == 'inactive' ? 'Off' : 'On'}}</button>
									</div>
								</li>

								@endforeach
							</ul>

						</div>
					</div>
				</div>

				<hr>
				<button type="submit" class="btn btn-success">Submit and Continue</button>
			</form>
			
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">

	off = (index) => {

		button 	= $(`.off-button.${index}`)
		dayoff 	= $(`.dayoff.${index}`)
		opens 	= $(`.opens.${index}`)
		closes 	= $(`.closes.${index}`)

		if(dayoff.val() === 'active'){

			button.removeClass('btn-default btn-danger').addClass('btn-danger').text('Day Off')
			opens.attr('disabled', true).val('')
			closes.attr('disabled', true).val('')
			dayoff.val('inactive')


		}else{

			button.removeClass('btn-default btn-danger').addClass('btn-default').text('Day On')
			opens.removeAttr('disabled').focus()
			closes.removeAttr('disabled')
			dayoff.val('active')
		}
	}
</script>
@endsection
