@extends('layouts.frontend')

@section('content')

<div class="page-filers">
    <div class="container">
        <div class="filter-results">
            <p>FILTER RESULTS</p>
        </div>
    </div>
</div>

<section class="sections medium grey">

    <div class="container">

        <ol class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a></li>
            <li class="active">Reviews of {{$user->name}}</li>
        </ol>

        <div class="page-headlines">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{$user->name}} / <small>Reviews</small></h2>
                </div>
            </div>
        </div>


        <div class="row">


            <div class="col-md-8">

                <div class="spots-all-by">
                    <div class="img-thumb">
                        {{_userImg($user)}}
                    </div>

                    <div class="spots-all-details">
                        <h4>{{$user->name}}</h4>
                        <div class="star-rating">
                            <span>{{$user->rating}}.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> Rated | <a href="#reviews">({{$reviews->count()}} {{str_plural('Review', $reviews->count())}})</a>
                        </div>

                        <p>Member Since: <strong>{{date('M, Y', strtotime($user->created_at))}}</strong></p>
                    </div>
                </div>


                <div class="category-results">
                    <div class="row">
                        <div class="col-md-12">


                            @component('components.merchant-reviews', ['user' => $user, 'reviews' => $reviews]) @endcomponent

                        </div>
                    </div>

                </div>

            </div>


            <div class="col-md-4">
                <div class="main-sidebar">

                    @component('components.featured-businesses-sidebar') @endcomponent
                    @component('components.trending-deals-sidebar') @endcomponent
                    @component('components.top-events-sidebar') @endcomponent

                </div>

            </div>
        </div>

    </div>
</section>

@component('components.recent', ['feedback' => 'white']) @endcomponent

@endsection
