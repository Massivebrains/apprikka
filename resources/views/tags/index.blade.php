@extends('layouts.frontend')

@section('content')

<div class="page-filers">
    <div class="container">
        <div class="filter-results">
            <p>FILTER RESULTS</p>
        </div>
    </div>
</div>

<section class="sections medium grey">

    <div class="container">

        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="#">Tags</a>
            </li>
            <li class="active">{{ucfirst($tag->tag)}}</li>
        </ol>

        <div class="row">

            <div class="col-md-12">

                <div class="page-headlines">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8 col-sm-8">
                                <h2><strong>{{ucfirst($tag->tag)}}</strong></h2>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="category-results">
                    <div class="col-md-12">


                        @foreach($products as $row)

                        @if($row->type == 'business')

                            @component('businesses.card-mini', ['business' => $row, 'col' => 4]) @endcomponent

                        @elseif($row->type == 'deal')

                            @component('deals.card-mini', ['deal' => $row, 'col' => 4]) @endcomponent

                        @elseif($row->event == 'event')

                            @component('events.card-mini', ['event' => $row, 'col' => 4]) @endcomponent

                        @endif

                        @endforeach

                        @if($products->count() < 1)

                        <div class="alert alert-info">
                            No items in this tag
                        </div>

                        @endif

                    </div>

                </div>

            </div>

        </div>
    </section>

    @endsection