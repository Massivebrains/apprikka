<div class="col-md-{{isset($col) ? $col : 6}}">
	<div class="carousel-item">
		<a href="{{url('deal/'.$deal->slug)}}">
			<div class="feat-img" style="width:350px; height: 230px;">
				{{_bannerImg($deal)}}
				<div class="spots-counter">
					<i class="material-icons ic-sm">&#xE192;</i> 
					<span class="countdown" data-date="{{$deal->end_date}}"></span>
				</div>
			</div>
		</a>


		<div class="spots-head">
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-8">
					<div class="spots-title">
						<h5>
							<a href="{{url('deal/'.$deal->slug)}}">
								{{str_limit($deal->name, 20, '...')}}
								<span>
									<i class="material-icons ic-sm tips trend" title="{{$deal->name}}">&#xE8E5;</i>
								</span>
							</a>
						</h5>
						<p class="seller">
							by 
							<a href="{{url('by/'.$deal->user->slug).'?type=deals'}}">
								{{$deal->user->name}}
							</a>
						</p>
						
						<div class="star-rating">
							@if($deal->review_count > 0)
							<span>{{$deal->rating}}.0</span> 
							<i class="material-icons ic-sm">&#xE8D0;</i> 
							{{$deal->review_count}} {{str_plural('Review', $deal->review_count)}}
							@else
							&nbsp;
							@endif
						</div>
						
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 no-pad-right">
					<div class="coupon-prices">
						<p>
							<span class="reglr">{{_c($deal->regular_price)}}</span>
							<br>
							<span class="sales">{{_c($deal->sale_price)}}</span>
						</p>
					</div>
				</div>
			</div>
		</div>


		<div class="landmark-share">

			<div class="share-this">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-8 col-sm-8 col-xs-8">
							<ul>
								<li>{{$deal->facebook}}</li>
								<li>{{$deal->twitter}}</li>
								<li>{{$deal->google}}</li>
								<li>{{$deal->whatsapp}}</li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<i class="material-icons close-share">&#xE5CD;</i>
						</div>
					</div>									
				</div>
			</div>
			
			<div class="spots-meta">
				<div class="col-md-8 col-sm-8 col-xs-8">
					<p class="add-mile"><i class="material-icons">&#xE55F;</i> {{str_limit($deal->place->name, 15, '...')}} | <span>{{$deal->distance}}</span></p>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4">
					<ul>
						<li><a href="javascript:;" onclick="save({{$deal->id}})"><i class="material-icons">bookmark</i></a></li>
						<li><a href="{{url('deal/'.$deal->slug)}}"><i class="material-icons">visibility</i></a></li>
						<li><a href="javascript:;" class="share-spot"><i class="material-icons">share</i></a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="tags">
			<ul>
				@foreach($deal->tags as $row)
				<li><a href="{{url('tag/'.$row->tag)}}">{{ucfirst($row->tag)}}</a></li>
				@endforeach
			</ul>
		</div>

	</div>
</div>