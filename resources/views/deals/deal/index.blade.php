@extends('layouts.frontend')

@section('style')
    <link rel="stylesheet" href="{{asset('/css/slick-theme.css')}}">
@endsection

@section('content')


<section class="deal-view regular">

    @include('components.breadcrumb')

    <div class="container">
        <div class="deals-header">
            <h3>{{$product->name}} <span><i class="material-icons tips" title="Trending Deal">&#xE8E5;</i></span></h3>
            <p>Sold by: <a href="#">{{$product->user->first_name.' '.$product->user->last_name}}</a></p>
            <p class="location"><i class="material-icons">&#xE55F;</i> 087 Hettie Flats Suite 681 | 10.2 mi </p>
        </div>
        <hr>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="deal-slider">
                    @foreach($product->images as $row)
                    <img src="{{$row}}" class="img-responsive" alt="{{$product->name}}">
                    @endforeach
                </div>
            </div>

            <div class="col-md-5">
                @include('deals.deal.components.deal-side')
            </div>
        </div>
    </div>
</section>

<div class="container">

    <div class="row">
        <div class="col-md-7">

            <div class="section-bottom" id="hours-direction">
                <h4>DETAILS</h4>
                {!! $product->description !!}
            </div>

            @include('components.reviews')


        </div>

        @component('deals.deal.components.suggested', ['products' => $product->suggested]) @endcomponent

        @include('components.login-modal')

    </div>

    <div style="margin-bottom:15px;"></div>
</div>

@component('components.recent', ['recent' => 'grey', 'products' => $recently_viewed, 'feedback' => 'white']) @endcomponent

@endsection