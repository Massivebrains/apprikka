@if(isset($products) && count($products) > 0)
<div class="col-md-5">
    <div class="more-deals-sidebar">
        <h4>SUGGESTED DEALS</h4>

        <ul>
            @foreach($products as $row)
            <li>
                <div class="recent-view">

                    <div class="col-md-5">
                        <a href="{{url('deal/'.$row->slug)}}">
                            {{_featImg($row)}}
                        </a>
                    </div>


                    <div class="col-md-7">
                        <div class="card-details">
                            <h5>
                                <a href="{{url('deal/'.$row->slug)}}">{{$row->name}} 
                                    <span><i class="material-icons ic-sm">&#xE8E5;</i></span>
                                </a>
                            </h5>
                            <p class="seller">by <a href="#">{{$row->user->first_name.' '.$row->user->last_name}}</a></p>
                            <p class="deal-price"><span class="reglr">{{_c($row->regular_price)}}</span> - <span class="sales">{{_c($row->sale_price)}}</span></p>
                            @if($row->review_count > 0)
                            <div class="star-rating">
                                <span>{{$row->review}}.0</span> <i class="material-icons ic-sm">&#xE8D0;</i> {{$row->review_count}} Rating
                            </div>
                            @endif
                            <p class="add-mile">Ikeja, Lagos | <span>3.1 mi</span></p>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
        </ul>

    </div>
</div>
@endif