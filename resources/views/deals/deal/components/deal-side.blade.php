<div class="deal-side-detail" id="dealSide">

    <form method="post" action="{{url('add-to-cart')}}" v-on:submit.prevent="submit">
        <div class="all-deal-details">

            <div class="detail-sec">

                @if($product->reivew_count > 0)
                <div class="star-rating">
                    <span>{{$product->review}}.0</span> 
                    <i class="material-icons ic-sm">&#xE8D0;</i> {{$product->review_count}} Rating | 
                    <a href="#comments">See reviews</a>
                </div>
                @endif

            </div>

            <div class="detail-sec time-views">
                <div class="row">
                    <div class="col-md-6">
                        <h4><span id="countdown"></span></h4>
                    </div>

                    <div class="col-md-6">
                        <div class="viewed-today">
                            <p>80+ viewed today</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="detail-sec">

            </div>

            <div class="detail-sec">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="deals-listed">
                            @foreach($product->options as $row)

                            @if($row->name != '')
                            <li>
                                <div class="col-md-8">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="option_id" id="radio-{{$row->id}}" value="{{$row->id}}" v-model="form.option_id" v-bind:disabled="disabled">
                                            <h4>{{$row->name}}</h4>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4 text-right">
                                    <p><span class="badge">30% off</span></p>
                                    <h4>{{_c($row->price)}}</h4>
                                </div>
                            </li>
                            @endif

                            @endforeach

                        </ul>

                    </div>
                </div>
            </div>

            <div class="buy-deal-btn">
                <button type="submit" class="btn btn-success" v-html="submitText" v-bind:disabled="disabled" v-if="cart == 0"></button>
                <a href="{{url('cart')}}" class="btn btn-success" v-else>Proceed to Checkout</a>
            </div>

        </div>
    </form>

    <div class="deal-tools">
        <ul>
            <li><a href="javascript;"><i class="material-icons">&#xE80D;</i> Share</a></li>
            <li><a href="javascript:;"><i class="material-icons">&#xE8E7;</i> Save</a></li>
        </ul>
    </div>
</div>


@push('script')

<script type="text/javascript">

    var dealSide = new Vue({

        el : '#dealSide',

        data : {
            form : {
                option_id   : 0,
                product_id  : {{$product->id}},
                _token      : '{{csrf_token()}}'
            },
            defaultSubmitText : '<i class="material-icons">&#xE8CC;</i> Buy Now',
            submitText        : '',
            disabled          : false,
            cart              : 0
        },
        created : function(){

            this.submitText = this.defaultSubmitText;
        },
        methods : {

            submit : function(e){

                this.submitText = '<i class="fa fa-spin fa-spinner"></i> Requesting...';
                this.disabled   = true;

                if(parseInt(this.form.option_id) < 1){

                    toastr.warning('Please select one of the options')
                    this.submitText = this.defaultSubmitText;
                    this.disabled = false;

                    return;
                }

                $.post(e.target.action, this.form, response => {

                    if(response.status == false){

                        if(response.data === 'auth'){

                            $('#login-modal').modal();

                        }else{

                            toastr.error(response.data);
                            this.submitText = this.defaultSubmitText;
                        }

                    }else{

                        toastr.success('Deal has been added to your cart');
                        this.submitText = 'Deal has been added to your cart';
                        this.cart       = 1;

                    }

                    this.disabled = false;
                });

            }
        }
    })
</script>
@endpush