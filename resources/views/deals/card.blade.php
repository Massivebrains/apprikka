<div class="featured-cards deals">
    <div class="row">
        <div class="col-md-5 feat-img-side">
            <a href="{{url('deal/'.$deal->slug)}}">
                <div class="feat-img">

                    {{_featImg($deal)}}
                    
                </div>
            </a>
        </div>

        <div class="col-md-7">
            <div class="card-details-cover">
                <div class="card-details">
                    <h5 style="width:90%">
                        <a href="{{url('deal/'.$deal->slug)}}">{{str_limit($deal->name, 90, '...')}}</a>
                    </h5>
                    <p class="seller">
                        by 
                        <a href="{{url('by/'.$deal->user->slug).'?type=deals'}}">
                            {{$deal->user->name}}
                        </a>
                    </p>
                    
                    <div class="star-rating">
                        @if($deal->review_count > 0)
                        <span>{{$deal->rating}}.0</span> 
                        <i class="material-icons ic-sm">&#xE8D0;</i> 
                        {{$deal->review_count}} {{str_plural('Review', $deal->review_count)}}
                        @else
                        &nbsp;
                        @endif
                    </div>
                    

                    <ul class="share-save deal-share">
                        <li><a href="javascript:;" onclick="save({{$deal->id}})"><i class="material-icons">bookmark</i></a></li>
                        <li><a href="{{url('deal/'.$deal->slug)}}"><i class="material-icons">visibility</i></a></li>
                        <li><a href="{{url('deal/'.$deal->slug)}}"><i class="material-icons">&#xE80D;</i></a></li>
                    </ul>

                    <p class="card-desc">{!! str_limit($deal->description, 200, '...') !!}</p>
                    <p class="add-mile">Ikeja, Lagos | <span>{{$deal->distance}}</span></p>
                </div>


                <div class="card-meta">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tags">
                                <div class="spots-counter" style="font-size:12px;">
                                    <i class="material-icons ic-sm">&#xE192;</i> <span class="countdown" data-date="{{$deal->end_date}}"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="sales-regular-price">
                                <li>{{_c($deal->regular_price)}}</li>
                                <li>{{_c($deal->sale_price)}}</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>