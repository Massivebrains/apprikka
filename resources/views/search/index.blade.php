@extends('layouts.frontend')

@section('content')

<div class="main-search-results sections regular grey">
	<div class="container">
		
		@include('search.components.sidebar')

		<div class="col-md-8">


			<div class="titles-all">
				<div class="col-md-8">
					<h3>Search Results for "{{ucfirst($query)}}"</h3>
					<hr>
				</div>
				<div class="col-md-4">
					<select name="" id="input" class="form-control text-right">
						<option value="">Sort by:</option>
					</select>
					<hr>
				</div>

			</div>

			<div class="spotlight-carousel">

				@foreach($results as $row)

				@if($row->type == 'business')

					@component('businesses.card-mini', ['business' => $row]) @endcomponent

				@elseif($row->type == 'deal')

					@component('deals.card-mini', ['deal' => $row]) @endcomponent

				@elseif($row->event == 'event')

					@component('events.card-mini', ['event' => $row]) @endcomponent

				@endif
				
				@endforeach

				@if($results->count() < 1)
				<div class="alert alert-info">
					We couldn't find anything for your search term.
				</div>
				@endif

			</div>

		</div>
	</div>
</div>

@endsection