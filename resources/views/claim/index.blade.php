@extends('layouts.frontend')

@section('content')

<div class="page-searches" style="background-color:#12ad2c;">

    <div class="col-md-12">

        <div class="container">

            <form action="{{url('claim-verify')}}" method="POST" class="form-inline" role="form">

                {{csrf_field()}}
                <div class="form-group">
                    <label class="sr-only">search</label>
                    <input type="text" name="name" class="form-control search-business" placeholder="Business Name" required>
                </div>

                <div class="form-group">
                    <label class="sr-only" for="">location</label>
                    <input type="text" name="location" class="form-control search-location" placeholder="Business Address or Location">
                </div>

                <button type="submit" class="btn btn-success" style="background:#fff; color : #12ad2c">Search</button>

            </form>
        </div>

    </div>
</div>

<div class="page-filers" style="background:#fff;">
    <div class="container" style="padding:20px; margin-top:30px">
        <div class="row">
            <div class="col-md-12">
                <h1 style="font-size:35px; color:#12ad2c; font-weight:bold">Claim your Business!</h1>

                <p style="color:#333; font-size:16px;">
                    The best way to manage your business on Apprikaa is by claiming your Apprikka Business Page
                    and creating a business login. its a great, free way to engage the Apprikaa community.
                </p>
                <hr>
            </div>
        </div>

        <div class="row">

            <div class="col-md-4">
                <img src="{{asset('/images/claim.png')}}" class="img img-responsive">
            </div>
            <div class="col-md-8">
                <h3 style="font-size:20px; margin-bottom: 15px; color:#12ad2c; font-weight:bold">After claiming your business page, you'll be able to:</h3>
                <ul style="line-height: 30px; list-style: circle; margin-bottom:30px;">
                    <li>Respond to reviews with a direct message or a public comment</li>
                    <li>Track the User Views and Customer Leads Apprikka is generating for your business</li>
                    <li>Add photos and a link to your website</li>
                    <li>Update important information such as your business hours and phone number</li>
                </ul>

                <h3 style="font-size:20px; margin-bottom: 15px; color:#12ad2c; font-weight:bold">More on how to claim</h3>
                <p style="color:#333; font-size:16px;">
                    To begin the claim process, Search your business by address and name below. 
                    To verify that you are the owner of your business, Apprikaa may send a text message to
                    the number on your business page and prompt you to enter a verification code. 
                    Please ensure you are at your place of business to answer this phone call. <a href="#">Learn more.</a>
                </p>
            </div>

        </div>
    </div>


</div>


<section class="sections medium grey">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <p style="text-align:center">Need Help? +234817 5020 329</p>

            </div>

        </div>

    </div>
</section>

@endsection


@push('script')

<script type="text/javascript">

    $.get('{{url('businesses-ajax')}}', response => {

        $('.search-business').typeahead({ source: response });
    });

    $.get('{{url('places/all')}}', response => {

        $('.search-location').typeahead({ source: response });
    });

</script>

@endpush