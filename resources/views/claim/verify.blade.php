@extends('layouts.frontend')

@section('content')

<div class="page-searches" style="background-color:transparent;">

    <div class="col-md-12">

        <div class="container">

            <div class="alert alert-info">
                You're claiming this business using your account associated with <strong style="font-weight: bold">{{Auth::user()->email}}</strong>. To use another account to claim, <a href="{{url('auth/logout')}}">click here.</a>
            </div>
        </div>

    </div>
</div>

<div class="page-filers" style="background:#fff;">

    <div class="container" style="padding:20px; margin-top:30px">
        <div class="row">
            <div class="col-md-8">

                <h3 style="font-size:20px; margin-bottom: 15px; color:#12ad2c; font-weight:bold">Choose How You Would Like to Verify</h3>
                <p style="color:#333; font-size:16px;">By verifying, you represent that you have the authroity to claim this account on behalf of this business.</p>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Phone Number verification</h3>
                    </div>
                    <div class="panel-body">

                        <form action="{{url('claim-business')}}" method="POST" role="form" id="form">

                            {{csrf_field()}}

                            @if(session('error'))
                                <div class="alert alert-danger" style="font-weight: bold">
                                    {{session('error')}}
                                </div>
                            @endif

                            <div class="alert alert-warning">
                                A verification code has been sent to <strong>{{$product->phone}}</strong> and also <strong>{{$product->email}}</strong>. Enter the Code below.
                            </div>

                            <input type="hidden" name="hash" value="{{md5($code)}}">
                            <input type="hidden" name="slug" value="{{$product->slug}}">

                            <div class="input-group">
                                <input type="number" name="code" class="form-control" aria-label="Verification Code" placeholder="Enter Verification code sent to {{$product->phone}}" required>
                                <span class="input-group-addon" style="cursor:pointer" onclick="$('#form').submit()"><strong>Verify Code</strong></span>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            @component('businesses.card-mini', ['business' => $product, 'col' => 4]) @endcomponent
        </div>
        
    </div>


</div>


<section class="sections medium grey">

    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <p style="text-align:center">Need Help? +234817 5020 329</p>

            </div>

        </div>
    </section>
    @endsection
