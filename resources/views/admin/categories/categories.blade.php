@extends('layouts.merchant')

@section('content')

<div class="row">
	<div class="col-md-12">
		<form action="{{url('admin/save-category/'.$category->id)}}" method="POST" role="form">

			{{csrf_field()}}

			<div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
				<label for="">Category Name</label>
				<input type="text" class="form-control" name="name" value="{{old('name', $category->name)}}" required>
				{!! _formError($errors, 'name') !!}
			</div>

			<div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
				<label for="">Category Description</label>
				<textarea name="description" class="form-control textarea" rows="3" required>{{old('description', $category->description)}}</textarea>
				{!! _formError($errors, 'description') !!}
			</div>

			<div class="form-group">
				<label for="">Status</label>
				<select name="status" class="form-control" required>
					@if(old('status', $category->status) == 'active')
						<option value="active" selected>Active</option>
						<option value="inactive">Inactive</option>
					@else
						<option value="active">Active</option>
						<option value="inactive">Inactive</option>
					@endif
				</select>
			</div>

			<input type="hidden" name="type" value="{{$type}}">
			<button type="submit" class="btn btn-success">Submit</button>
		</form>
	</div>
</div>

<hr>

<div class="row">
	
	<div class="col-md-12">
		<table id="datatable" class="table table-bordered" style="width:100%">
			<thead>
				<tr>
					<th>Name</th>
					<th>{{str_plural(ucfirst($type), 2)}}</th>
					<th>Date Created</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				@foreach($categories as $row)
				<tr>
					<td>{{$row->name}}</td>
					<td>0</td>
					<td>{{_d($row->created_at)}}</td>
					<td>{{_badge($row->status)}}</td>
					<td>
						<div class="btn-group">
							<a class="btn btn-xs dropdown-toggle" data-toggle="dropdown" href="#">Action <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href='{{url('admin/categories/'.$row->type.'/'.$row->id)}}'>Edit</a></li>
								<li class="divider"></li>
								<li><a href='{{url('admin/delete-category/'.$row->id)}}' class="confirm">Delete</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@endforeach
			</tfoot>
		</table>
	</div>
</div>

@endsection