@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Merchant Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $row)
        <tr>
            <td>{{$row->first_name.' '.$row->last_name}}</td>
            <td><a href="mailto:{{$row->email}}">{{$row->email}}</a></td>
            <td>{{$row->phone}}</td>
            <td>{{_badge($row->status)}}</td>
            <td>
                <div class="dropdown">
                    <button class="btn btn-success btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Actions
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li><a href="{{url('admin/merchants/basic-info/'.$row->id)}}">View Details</a></li>
                        @if($row->status == 'inactive')
                        <li><a href="{{url('admin/merchants/status/'.$row->id)}}" class="confirm">Suspend</a></li>
                        @endif
                    </ul>
                </div>
            </td>
        </tr>
        @endforeach
    </tfoot>
</table>

@endsection