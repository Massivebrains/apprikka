@extends('layouts.merchant')

@section('content')

<div role="tabpanel">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">BASIC INFO</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">USER DETAILS</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">CONTACT</a>
        </li>
    </ul>

    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="basic-info">

            <form action="{{url('admin/merchants/basic-info/'.$user->id)}}" method="POST" role="form">

                {{csrf_field()}}

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('first_name') ? 'has-error' : ''}}">
                            <label>First Name:</label>
                            <input type="text" class="form-control" name="first_name" value="{{old('first_name', $user->first_name)}}" required>
                            {!! _formError($errors, 'first_name') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('last_name') ? 'has-error' : ''}}">
                            <label>Last Name:</label>
                            <input type="text" class="form-control" name="last_name" value="{{old('last_name', $user->last_name)}}" required>
                            {!! _formError($errors, 'last_name') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                            <label>Email:</label>
                            <input type="text" class="form-control" name="email" value="{{old('email', $user->email)}}" required>
                            {!! _formError($errors, 'email') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {{$errors->has('phone') ? 'has-error' : ''}}">
                            <label>Phone:</label>
                            <input type="number" class="form-control" name="phone" value="{{old('phone', $user->phone)}}" required>
                            {!! _formError($errors, 'phone') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('place_id') ? 'has-error' : ''}}">
                            <label>Land Mark:</label>
                            <select name="place_id" class="form-control place" required>
                                @if($user->place_id != '')
                                <option value="{{$user->place->id}}" selected>{{$user->place->address}}</option>
                                @endif
                            </select>
                            {!! _formError($errors, 'place_id') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group {{$errors->has('address') ? 'has-error' : ''}}">
                            <label>Full Address:</label>
                            <input type="text" name="address" class="form-control" value="{{old('address', $user->address)}}">
                            {!! _formError($errors, 'address') !!}
                        </div>
                    </div>
                </div>
                

                <div class="row">
                   <div class="col-md-6">
                    <div class="form-group {{$errors->has('country_id') ? 'has-error' : ''}}">
                        <label> Country:</label>
                        <select name="country_id" class="form-control select2 country_id">
                            @foreach(\App\Country::get() as $row)
                            @if(old('country_id', $user->country_id) == $row->id)
                            <option value="{{$row->id}}" selected>{{$row->name}}</option>
                            @else
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endif
                            @endforeach
                        </select>
                        {!! _formError($errors, 'country_id') !!}
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group {{$errors->has('state_id') ? 'has-error' : ''}}">
                        <label> State:</label>
                        <select name="state_id"state_id class="form-control select2 state_id">
                            @php 
                            $state = \App\State::where(['id' => old('state_id', $user->state_id)])->first();
                            @endphp 
                            @if($state)
                            <option value="{{$state->id}}" selected>{{$state->name}}</option>
                            @else
                            <option>--select country--</option>
                            @endif
                        </select>
                        {!! _formError($errors, 'state_id') !!}
                    </div>
                </div>
            </div>


            <input type="hidden" name="latitude" value="{{old('latitude', $user->latitude)}}">
            <input type="hidden" name="longitude" value="{{old('longitude', $user->longitude)}}">
            <input type="hidden" name="location_source" value="{{old('location_source', $user->location_source)}}">

            <button type="submit" class="btn btn-success" id="submit">Save and Continue</button>
        </form>

    </div>
</div>
</div>

@endsection


@section('scripts')

