@extends('layouts.merchant')

@section('content')

<div role="tabpanel">

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation">
            <a href="javascript:;">BASIC INFO</a>
        </li>
        <li role="presentation" class="active">
            <a href="#user-details" aria-controls="basic-info" role="tab" data-toggle="tab">USER DETAILS</a>
        </li>
        <li role="presentation">
            <a href="javascript:;">CONTACT</a>
        </li>
    </ul>

    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="user-details">

         <form action="{{url('admin/merchants/details/'.$merchant->id)}}" method="POST" role="form" enctype="multipart/form-data">

            {{csrf_field()}}

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Merchant Website URL:</label>
                        <input type="url" class="form-control" name="website" value="{{old('website', $merchant->website)}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>About Merchant:</label>
                        <textarea name="info" class="form-control textarea" rows="3">{{old('info', $merchant->info)}}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Tags:</label>
                        <input name="tags" type="text" class="form-control tags">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Profile Picture</label>
                        <input type="file" name="image" class="dropify" data-max-file-size="1M" data-allowed-file-extensions="png jpg jpeg gif">
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-success">Submit and Continue</button>
        </form>

    </div>
</div>
</div>

@endsection

