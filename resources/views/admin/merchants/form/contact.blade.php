@extends('layouts.merchant')

@section('content')

<div role="tabpanel">

	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation">
			<a href="javascript:;" >BASIC INFO</a>
		</li>
		<li role="presentation">
			<a href="javascript:;">USER DETAILS</a>
		</li>
		<li role="presentation" class="active">
			<a href="#basic-info" aria-controls="basic-info" role="tab" data-toggle="tab">CONTACT</a>
		</li>
	</ul>


	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="contact">

			<form action="{{url('admin/merchants/contact/'.$merchant->id)}}" method="POST" role="form">

				{{csrf_field()}}

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Office Address:</label>
							<input type="text" name="office_address" class="form-control" value="{{old('office_address', $merchant->office_address)}}">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="">Office Email:</label>
							<input type="text" class="form-control" name="office_email" value="{{old('office_email', $merchant->office_email)}}">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="">Office Phone:</label>
							<input type="text" class="form-control" name="office_phone" value="{{old('office_phone', $merchant->office_phone)}}">
						</div>
					</div>

				</div>

				<button type="submit" class="btn btn-success">Submit and Save</button>
			</form>
		</div>
	</div>
</div>

@endsection


