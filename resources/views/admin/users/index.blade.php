@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $row)
        <tr>
            <td>{{$row->first_name}}</td>
            <td>{{$row->last_name}}</td>
            <td><a href="mailto:{{$row->email}}">{{$row->email}}</a></td>
            <td>{{$row->phone}}</td>
            <td>{{_badge($row->status)}}</td>
            <td>
                <a href='#' class="btn btn-xs btn-{{$row->status == 'active' ? 'success' : 'warning'}}">
                View Details
            </a>
            </td>
        </tr>
        @endforeach
    </tfoot>
</table>

@endsection