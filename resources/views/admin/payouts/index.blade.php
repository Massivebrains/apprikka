@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Merchant</th>
            <th>Reference</th>
            <th>Date</th>
            <th>Payment Date</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>

        @foreach($payouts as $row)

        <tr>
            <td><a href="{{url('admin/merchants/basic-info/'.$row->user->id)}}">{{$row->user->name}}</a></td>
            <td>{{$row->reference}}</td>
            <td>{{_d($row->created_at)}}</td>
            <td>{{_d($row->payment_date)}}</td>
            <td>{{_c($row->amount)}}</td>
            <td>{{_badge($row->status)}}</td>

            <td>
                <div class="dropdown">
                    <button class="btn btn-success btn-xs dropdown-toggle" type="button" data-toggle="dropdown">Actions
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('admin/payouts/payout/'.$row->reference)}}">View Details</a></li>
                        </ul>
                    </div>
                </td>
            </tr>

            @endforeach
        </tbody>
    </table>

    @endsection