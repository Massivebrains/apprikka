@extends('layouts.merchant')

@section('content')

<div class="col-md-8 col-md-offset-2">
	<form action="{{url('admin/admins/'.$admin->id.'/save')}}" method="POST" role="form">

		{{csrf_field()}}
		<div class="row">

			<div class="col-md-6">
				<div class="form-group {{$errors->has('first_name') ? 'has-error' : ''}}">
					<label for="">First Name:</label>
					<input type="text" class="form-control" name="first_name" value="{{old('first_name', $admin->first_name)}}" required>
					{!! _formError($errors, 'first_name') !!}
				</div>
			</div>

			<div class="col-md-6">
				<div class="form-group {{$errors->has('last_name') ? 'has-error' : ''}}">
					<label for="">Last Name:</label>
					<input type="text" class="form-control" name="last_name" value="{{old('last_name', $admin->last_name)}}" required>
					{!! _formError($errors, 'last_name') !!}
				</div>
			</div>

		</div>
		

		<div class="row">
			<div class="col-md-6">
				<div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
					<label for="">Email Address:</label>
					<input type="email" class="form-control" name="email" value="{{old('email', $admin->email)}}" required>
					{!! _formError($errors, 'email') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group {{$errors->has('phone') ? 'has-error' : ''}}">
					<label for="">Phone Number</label>
					<input type="text" class="form-control" name="phone" value="{{old('phone', $admin->phone)}}" required>
					{!! _formError($errors, 'phone') !!}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="form-group {{$errors->has('role') ? 'has-error' : ''}}">
					<label for="">Priviledge:</label>
					<select name="" id="input" class="form-control" required>
						<option value="">Subadmin</option>
						<option value="">Admin</option>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
					<label for="">Create Password:</label>
					<input type="password" class="form-control" name="password" required>
					{!! _formError($errors, 'password') !!}
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
					<label for="">Confirm Password:</label>
					<input type="password" class="form-control" name="password_confirmation">
					{!! _formError($errors, 'password_confirmation') !!}
				</div>
			</div>
		</div>

		<button type="submit" class="btn btn-success">Submit</button>
	</form>
</div>

@endsection