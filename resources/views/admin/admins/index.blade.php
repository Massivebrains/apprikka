@extends('layouts.merchant')

@section('content')

<div class="profile-basic-info">
	<h4>BASIC INFO</h4>
	<table class="table table-bordered table-hover">
		<tbody>
			<tr>
				<td>First Name:</td>
				<td>Last Name: </td>
			</tr>
			<tr>
				<td>Email Address:</td>
				<td>Phone Number</td>
			</tr>
		</tbody>
	</table>
</div>

@endsection