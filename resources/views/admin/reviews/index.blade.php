@extends('layouts.merchant')

@section('content')

<table id="datatable" class="table table-bordered" style="width:100%">
    <thead>
        <tr>
            <th>Date</th>
            <th>Name</th>
            <th>Review</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($reviews as $row)
        <tr>
            <td>{{_d($row->created_at)}}</td>
            <td>{{$row->name}}</td>
            <td>{{$row->name}}</td>
            <td>{{str_limit($row->comment, 50, '...')}}</td>
            <td>{{_badge($row->status)}}</td>
            <td style="width:17%">
                <div class="btn-group">
                    <a href='{{url('admin/review/'.$row->id)}}' class="btn btn-xs btn-{{$row->status == 'visible' ? 'success' : 'warning'}}">View</a>
                    <a href="{{url('admin/review/status/'.$row->id)}}" class="btn btn-xs btn-{{$row->status == 'visible' ? 'success' : 'info'}} confirm">
                        {{$row->status == 'visible' ? 'Hide' : 'Make Visible'}}
                    </a>
                </div>
            </td>
        </tr>
        @endforeach
    </tfoot>
</table>


@endsection