@extends('layouts.merchant')

@section('content')

<div class="account-overview">

  <div class="dash-title">

    <div class="row">

      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Review
            </h3>
          </div>
          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
                <table class="table">
                  <tr>
                    <th>DATE</th>
                    <td>{{_d($review->created_at)}}</td>
                  </tr>
                  <tr>
                    <th>{{strtoupper($review->product->type)}}</th>
                    <td>
                      <a href="{{url($review->product->type.'/'.$review->product->slug)}}" target="_blank">
                        {{$review->product->name}}
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <th>REVIEWER NAME</th> 
                    <td>{{$review->name}}</td>
                  </tr>
                  <tr>
                    <th>REVIEWER EMAIL</th> 
                    <td>{{$review->email}}</td>
                  </tr>
                  <tr>
                    <th>REVIEWER PHONE</th> 
                    <td>{{$review->phone}}</td>
                  </tr>
                  <tr>
                    <th>RATING</th> 
                    <td>{{$review->review}}</td>
                  </tr>
                  <tr>
                    <th>STATUS</th> 
                    <td>{{_badge($review->status)}}</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
          <table class="table table-striped table-bordered">

            <thead>
              <tr>
                <th>COMMENT</th>
              </tr>
            </thead>
            <tbody>

              <tr>
                <td>{{$review->comment}}</td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection