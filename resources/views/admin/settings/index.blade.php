@extends('layouts.merchant')

@section('content')

<div class="profile-basic-info">
	<h4>EXTERNAL SERVICES INFO</h4><br>
	<table class="table table-bordered table-striped">
		<tbody>
			<tr>
				<th>Service</th>
				<th>Home URL</th>
				<th>Login URL</th>
				<th>Username</th>
				<th>Password</th>
			</tr>
			<tr>
				<td>Mail Chimp</td>
				<td><a href="https://mailchimp.com">https://mailchimp.com</a></td>
				<td><a href="https://login.mailchimp.com/" target="_blank">https://login.mailchimp.com</a></td>
				<td>mailchimp@apprikaa.com</td>
				<td>Passme@123</td>
			</tr>
			<tr>
				<td>Send Grid</td>
				<td><a href="https://sendgrid.com">https://sendgrid.com</a></td>
				<td><a href="https://app.sendgrid.com/login" target="_blank">https://app.sendgrid.com/login</a></td>
				<td>support@apprikaa.com</td>
				<td>Passme@123</td>
			</tr>
			<tr>
				<td>Drift Live Chat</td>
				<td><a href="https://drift.com">https://drift.com</a></td>
				<td><a href="https://drift.com" target="_blank">https://drift.com</a></td>
				<td>support@apprikaa.com</td>
				<td>Passme@123</td>
			</tr>
			<tr>
				<td>Blog</td>
				<td><a href="http://apprikka.com/blog">http://apprikka.com/blog</a></td>
				<td><a href="http://apprikka.com/blog" target="_blank">http://apprikka.com/blog</a></td>
				<td>support@apprikaa.com</td>
				<td>Passme@123</td>
			</tr>
		</tbody>
	</table>
</div>

@endsection