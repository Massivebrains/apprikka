@extends('layouts.merchant')

@section('content')

<div class="row">
	<div class="col-md-4">
		<div class="admin-stats new">
			<a href="{{url('merchant/products/business')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$active_businesses}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ACTIVE B. <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>



	<div class="col-md-4">
		<div class="admin-stats new">
			<a href="{{url('merchant/products/deal')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$active_deals}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ACTIVE DEALS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>



	<div class="col-md-4">
		<div class="admin-stats new">
			<a href="{{url('merchant/products/event')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$active_events}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ACTIVE EVENTS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>

<div class="row">


	<div class="col-md-4">
		<div class="admin-stats new">
			<a href="#">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$coupons}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ACTIVE COUPONS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>





	<div class="col-md-4">
		<div class="admin-stats new">
			<a href="{{url('admin/dashboard')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$payouts}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>PAYOUT REQUEST <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>



	<div class="col-md-4">
		<div class="admin-stats">
			<a href="{{url('merchant/products/business')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$businesses}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ALL businesses <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-4">
		<div class="admin-stats">
			<a href="{{url('merchant/products/deal')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$deals}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ALL DEALS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>



	<div class="col-md-4">
		<div class="admin-stats">
			<a href="{{url('merchant/product/event')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$events}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ALL EVENTS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>



	<div class="col-md-4">
		<div class="admin-stats">
			<a href="{{url('admin/reviews')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$reviews}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ALL REVIEWS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-4">
		<div class="admin-stats">
			<a href="{{url('merchant/orders')}}">
				<div class="col-md-4 no-pad">
					<div class="stats-l">
						<h3>{{$orders}}</h3>
					</div>
				</div>
				<div class="col-md-8">
					<div class="stats-r">
						<h5>ORDERS <span><i class="material-icons">arrow_right_alt</i></span></h5>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>
@endsection