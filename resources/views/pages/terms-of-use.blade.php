@extends('layouts.frontend')

@section('content')

<section class="sections medium grey">

    <div class="container">

        <ol class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li class="active">Terms of Use</li>
        </ol>

        <div class="row">

            <div class="col-md-12">

                <div class="page-headlines">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-8 col-sm-8">
                                <h2><strong>Terms of Use</strong></h2>
                            </div>
                          
                        </div>
                    </div>
                </div>

                <div class="category-results">
                    <div class="col-md-12">


                        @for($i = 0; $i<=10; $i++)

                       <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                       </p>

                        @endfor
                    </div>

                </div>

            </div>

        </div>
    </section>

    @include('components.feedback')
    @endsection