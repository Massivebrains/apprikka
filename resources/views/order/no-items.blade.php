@extends('layouts.frontend')

@section('content')

<section class="deal-view regular">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Your Cart</h1>
                    </div>
                    <div class="panel-body">
                        <p class="text-center" style="font-size:1.5em;">
                            You currently have no items in your cart. <a href="{{url('/')}}">Click here</a> to continue browsing Apprikaa.
                        </p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
</section>
@endsection