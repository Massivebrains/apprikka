@extends('layouts.frontend')

@section('content')

<section class="deal-view regular">

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Your Items</h1>
                    </div>
                    <div class="panel-body">
                        @foreach($order->order_details as $row)

                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{$row->product->banner}}" class="img img-responsive img-rounded">
                            </div>
                            <div class="col-md-4">
                                <h3>{{$row->product->name}}</h3>
                                <code>Sold by {{$row->product->user->first_name. ' '.$row->product->user->last_name}}</code>
                            </div>
                            <div class="col-md-4">
                                <h1>
                                    @if($row->price < 1)
                                    FREE
                                    @else
                                    {{_c($row->price)}}
                                    @endif
                                    
                                </h1>
                                <p>{{$row->option_name}}</p>
                                <a href="javascript:;" onclick="removeProduct({{$row->product->id}})" style="color:#b8c2cc;; text-decoration: none;">Remove</a>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                    </div>
                </div>

                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title">Billing Details</h1>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="checkout">

                            {{csrf_field()}}

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>First Name</label>
                                    <input type="text" name="first_name" class="form-control" placeholder="First Name" value="{{Auth::user()->first_name}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="{{Auth::user()->last_name}}" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Email Address</label>
                                    <input type="email" name="email" class="form-control" placeholder="Email Address" value="{{Auth::user()->email}}" required>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Phone</label>
                                    <input type="number" name="phone" class="form-control" placeholder="Phone" value="{{Auth::user()->phone}}" required>
                                </div>

                                <div class="form-group col-md-6">

                                    <label>Country</label>

                                    <select name="country_id" class="form-control country_id select2" required>
                                       @foreach(\App\Country::get() as $row)
                                       @if($row->id == Auth::user()->country_id)
                                       <option value="{{$row->id}}" selected>{{$row->name}}</option>
                                       @else
                                       <option value="{{$row->id}}">{{$row->name}}</option>
                                       @endif
                                       @endforeach
                                   </select>

                               </div>
                               <div class="form-group col-md-6">
                                <label>State</label>
                                <select name="state_id" class="form-control state_id select2" required>
                                    <option value="0">--select country--</option>
                                </select>
                            </div>

                            @if($order->total != 0)
                            <div class="form-group col-sm-12">
                                <label>Address</label>
                                <input type="text" name="address" class="form-control">
                            </div>
                            @endif
                        </div>

                        <input type="hidden" name="order_reference" value="{{$order->order_reference}}">

                        <button type="submit" class="btn btn-success pay" style="display:inline-block">
                            {{$order->total > 0 ? 'Place Order' : 'Complete Order'}}
                        </button>


                    </form>

                    <div class="row payments text-center" style="display:none">
                        <div class="col-md-12">
                            <h1>Payment Options</h1>
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="text-center paystack" style="cursor:pointer">
                                <img src="{{asset('/images/paystack.png')}}" style="width:30%; height:30%">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <h3>OR </h3>
                        </div>
                        <div class="col-sm-12">
                            <div id="paypal"></div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Order Summary</h1>
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h3>
                                <strong>Subtotal</strong>
                                <p class="pull-right">{{_c($order->sub_total)}}</p>
                            </h3>
                        </li>
                        <li class="list-group-item">
                            <h4>
                                <strong class="text-success">
                                    @if($order->coupon != null)
                                    Coupon <span style="font-weight: bold">{{$order->coupon->code}}</span>
                                    @else
                                    Discount / Coupon Code
                                    @endif
                                </strong>
                                <p class="pull-right">{{_c($order->discount, false)}}</p>
                            </h4>
                        </li>
                        <li class="list-group-item">
                            <h4>
                                <strong style="font-weight:bold">Order Total</strong>
                                <p class="pull-right" style="font-weight: bold">{{_c($order->total)}}</p>
                            </h4>
                        </li>
                    </ul>

                </div>
            </div>

            <div class="panel panel-{{session('error') ? 'danger' : 'default'}}" id="coupon">

                <div class="panel-heading">
                    <h1 class="panel-title">Coupon / Discount Code</h1>
                </div>

                <div class="panel-body">

                    <form role="form" action="{{url('coupon')}}" method="post">

                        {{csrf_field()}}

                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Have a coupon Code?</label>
                                <input type="text" name="coupon" class="form-control" placeholder="Enter Coupon/Discount code here" required>
                                @if(session('error'))
                                <label class="text-danger">{{session('error')}}</label>
                                @endif
                                @if(session('message'))
                                <label class="text-success">{{session('message')}}</label>
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="order_reference" value="{{$order->order_reference}}">

                        <button type="submit" class="btn btn-success" style="display:inline-block">
                           Apply
                       </button>

                   </form>

               </div>

           </div>
       </div>
   </div>
</div>
</section>

<script type="text/javascript">

    $checkout = $('#checkout');

    let data        = {};
    let redirectUrl = '{{url('transaction/successful')}}';

    $checkout.submit(e => {

        e.preventDefault();

        data = $checkout.serializeObject();

        $.post('{{url('address')}}', data, response => {

            toastr.success('Your billing details has been saved. Loading payment...');
            
            data = response.data;
            
            if({{$order->total}} == 0){

                window.location = redirectUrl;

            }else{

                $checkout.hide();
                $('.payments').show();
                
            }
            
        })
    })

    removeProduct = product_id => {

        console.log(product_id);
    }

    $('.paystack').click(() => {

        paystack(data.email, data.total, data.order_reference, redirectUrl);
    });

</script>

@include('components.paypal')
@include('components.paystack')

@endsection