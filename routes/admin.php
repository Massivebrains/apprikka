<?php 

Route::get('/', 'DashboardController@index');
Route::get('admins', 'AdminController@index');
Route::get('admins/{id}', 'AdminController@form');
Route::post('admins/{id}/save', 'AdminController@save');

Route::get('categories/{type?}/{id?}', 'CategoriesController@index');
Route::post('save-category/{id?}', 'CategoriesController@save');
Route::get('delete-category/{id?}', 'CategoriesController@delete');

Route::get('sub-categories/{type?}/{id?}', 'CategoriesController@subCategories');

Route::group(['prefix' => 'merchants'], function(){

    Route::any('/', 'MerchantsController@index');
    Route::any('basic-info/{id?}', 'MerchantsController@basicInfo');
    Route::any('details/{id?}', 'MerchantsController@userDetails');
    Route::any('contact/{id?}', 'MerchantsController@contact');
    Route::any('status/{id?}', 'MerchantsController@status');

});

Route::get('users', 'UsersController@index');

Route::group(['prefix' => 'payouts'], function(){

	Route::get('/', 'PayoutsController@index');
	Route::any('payout/{reference?}', 'PayoutsController@payout');
	Route::any('payout/complete/{id?}', 'PayoutsController@complete');

});

Route::get('reviews', 'ReviewsController@index');
Route::get('review/status/{id?}', 'ReviewsController@status');
Route::get('review/{id?}', 'ReviewsController@review');

Route::group(['prefix' => 'settings'], function(){

	Route::get('/', 'SettingsController@index');
});