<?php 

Route::get('/', 'DashboardController@index');


Route::get('products/{type?}', 'ProductsController@index');

Route::group(['prefix' => 'product'], function(){

	Route::any('basic-info/{id?}/{type?}', 'ProductsController@basicInfo');
	Route::any('business-details/{id?}', 'ProductsController@businessDetails');
	Route::any('group-details/{id?}', 'ProductsController@groupDetails');
	Route::any('options/{id?}', 'ProductsController@options');
	Route::any('gallery/{id?}', 'ProductsController@gallery');
	Route::post('upload/{id?}', 'ProductsController@upload');
	Route::any('contact/{id?}', 'ProductsController@contact');
	Route::any('status/{id?}', 'ProductsController@status');
	Route::any('featured/{id?}', 'ProductsController@featured');
});


Route::group(['prefix' => 'quotes'], function(){

	Route::get('/{id?}', 'QuotesController@index');
	Route::get('/{id?}/quote', 'QuotesController@quote');
	Route::get('/{id?}/status', 'QuotesController@status');
});

Route::get('orders', 'OrdersController@index');
Route::get('order/{order_reference}', 'OrdersController@order');

Route::group(['prefix' => 'ajax'], function(){

	Route::group(['prefix' => 'option'], function(){

		Route::get('add/{id?}', 'OptionsController@add');
		Route::post('update/name/{id?}', 'OptionsController@updateName');
		Route::post('update/price/{id?}', 'OptionsController@updatePrice');
		Route::post('update/quantity/{id?}', 'OptionsController@updateQuantity');
		Route::post('remove', 'OptionsController@remove');

	});
	
});

Route::group(['prefix' => 'profile'], function(){

	Route::get('/', 'ProfileController@index');
	Route::post('/basic-info', 'ProfileController@basicInfo');
	Route::post('/billing', 'ProfileController@billing');
	Route::post('/password', 'ProfileController@password');
	Route::post('/upload', 'ProfileController@upload');

});

Route::group(['prefix' => 'payouts'], function(){

	Route::get('/', 'PayoutController@index');
	Route::any('request', 'PayoutController@request');
	Route::any('payout/{reference?}', 'PayoutController@payout');

});

Route::group(['prefix' => 'verify'], function(){

    Route::get('step/{step?}', 'VerificationController@index');
    Route::get('terms', 'VerificationController@terms');
    Route::get('orientation', 'VerificationController@orientation');
    Route::any('upload', 'VerificationController@upload');
    Route::any('bank', 'VerificationController@bank');
    Route::get('complete', 'VerificationController@complete');
});

Route::group(['prefix' => 'coupons'], function(){

	Route::get('/', 'CouponsController@index');
	Route::get('/coupon/{id?}', 'CouponsController@coupon');
	Route::post('/save-coupon/{id?}', 'CouponsController@saveCoupon');
});