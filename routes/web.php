<?php

Route::get('how-it-works', 'PagesController@howItWorks');
Route::get('terms-of-use', 'PagesController@termsOfUse');
Route::get('privacy-policy', 'PagesController@privacyPolicy');
Route::get('faq', 'PagesController@faq');
Route::get('contact-us', 'PagesController@contactUs');
Route::any('location', 'HomeController@location');

Route::group(['prefix' => 'auth'], function(){

    Route::get('/', 'AuthController@index')->name('login');
    Route::post('login', 'AuthController@login');
    Route::get('logout', 'AuthController@logout');

    Route::get('/merchant', 'AuthController@merchant');
    Route::get('/merchant-register', 'AuthController@merchantRegister');
    Route::get('/user-register', 'AuthController@userRegister');
    Route::any('/register/{type?}', 'AuthController@register');
    Route::any('/activate/{id?}', 'AuthController@activate');
    Route::any('forgot-password/{token?}', 'AuthController@forgotPassword');
    Route::any('reset-password/{token?}', 'AuthController@resetPassword');
});

Route::get('/', 'HomeController@index');

Route::get('businesses/{slug?}', 'BusinessesController@index');
Route::get('businesses/sort/{type?}', 'BusinessesController@sort');
Route::post('business/request-quote', 'QuotesController@requestQuote');
Route::get('business/{slug?}', 'BusinessesController@business');
Route::get('businesses-ajax', 'BusinessesController@businessesAjax');


Route::get('deals/{slug?}', 'DealsController@index');
Route::get('deal/{slug?}', 'DealsController@deal');
Route::get('deals-by/{slug}', 'ProductsController@merchant');

Route::get('events/{slug?}', 'EventsController@index');
Route::get('event/{slug?}', 'EventsController@event');

Route::get('groups/{slug?}', 'GroupsController@index');
Route::get('groups/sort/{type?}', 'GroupsController@sort');
Route::get('group/{slug?}', 'GroupsController@group');

Route::get('products/save/{id?}', 'ProductsController@save');
Route::get('by/{slug}', 'ProductsController@merchant');
Route::get('reviews-by/{slug?}', 'ReviewsController@merchant');

Route::get('tag/{tag?}', 'TagsController@index');

Route::post('add-to-cart', 'CartController@addToCart');
Route::get('cart/count', 'CartController@count');
Route::get('cart/{order_reference?}', 'CartController@cart');
Route::post('address', 'CartController@address');
Route::post('coupon', 'CartController@coupon');

Route::group(['prefix' => 'utils'], function(){

    Route::get('states/{id?}', 'UtilityController@states');
    Route::get('sub-categories/{id?}', 'UtilityController@subCategories');
    Route::any('mailchimp', 'UtilityController@mailchimp');
    
});

Route::group(['prefix' => 'transaction'], function(){

    Route::get('requery/{ref?}/{orderNumber?}', 'TransactionController@requery');
    Route::get('successful/{orderNumber?}', 'TransactionController@successful');
});

Route::group(['prefix' => 'places'], function(){

    Route::get('search-by-address/{address?}', 'PlacesController@searchByAddress');
    Route::get('all', 'PlacesController@places');
    Route::any('locations', 'PlacesController@locations');
});

Route::any('search', 'SearchController@index');
Route::post('review', 'ReviewsController@save');
Route::any('claim-business', 'ClaimController@index');
Route::any('claim-verify/{slug?}', 'ClaimController@verify');

