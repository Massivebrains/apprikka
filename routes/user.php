<?php 

Route::group(['middleware' => ['auth', 'user']], function(){

	Route::get('/', 'HomeController@index');
	Route::get('saved', 'HomeController@saved');	
	Route::get('orders', 'HomeController@orders');	
	Route::get('order/{ref?}', 'HomeController@order');	
	Route::get('quotes', 'HomeController@quotes');

});

Route::group(['middleware' => ['auth', 'user'], 'prefix' => 'profile'], function(){

	Route::get('/', 'ProfileController@index');
	Route::post('/basic-info', 'ProfileController@basicInfo');
	Route::post('/billing', 'ProfileController@billing');
	Route::post('/password', 'ProfileController@password');

});



